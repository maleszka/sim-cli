# Contributing guide

These are clearly defined rules that matter the most in smooth teamwork. The
following document is a short guideline on conventions that each contribution
must meet.

## Naming conventions

| Name                      | Used format    | Example               | Notes                                                                 |
|:--------------------------|:---------------|:----------------------|:----------------------------------------------------------------------|
| local variables           | snake\_case    | `http_header`         |                                                                       |
| functions                 | camelCase      | `colorEscapeCode`     |                                                                       |
| function arguments        | snake\_case    | `std::string sim_url` |                                                                       |
|                           |                |                       |                                                                       |
| classes                   | UpperCamelCase | `FetcherSession`      |                                                                       |
| class function members    | camelCase      | `performApiCall`      |                                                                       |
| class data members        | snake\_case\_  | `sim_url_`            | trailing `_` is used to distinguish data members from local variables |
|                           |                |                       |                                                                       |
| typedefs                  | snake\_case\_t | `logger_t`            |                                                                       |
| protected structs         | snake\_case\_t | `score_t`             |                                                                       |
| structs used like classes | UpperCamelCase | `Score`               |                                                                       |
|                           |                |                       |                                                                       |
| enum, enum classes        | UpperCamelCase | `SeverityLevel`       |                                                                       |
| enum values               | camelCase      | `logCritical`         | proper hungarian notation recommended (e.g. `cBlack`)                 |
| other constants           | camelCase      | `roundUserSubmits`    |                                                                       |
|                           |                |                       |                                                                       |
| namespaces                | snake\_case    | `simc::term`          | one-word names recommended (without underscores)                      |
|                           |                |                       |                                                                       |
| preprocessor macros       | SNAKE\_CASE    | `LOG`                 |                                                                       |

[Proper hungarian
notation](https://www.joelonsoftware.com/2005/05/11/making-wrong-code-look-wrong/)
is highly recommended when suitable. For example:

1. Narrow multibyte strings should be prefixed with `ns`, strings for ucs2
   encoding with `us`, and encoding-agnostic strings --- without any prefix.
2. Data related strictly to the simc interactive mode should be prefixed with
   `i` and to the non-interactive mode --- with `ni`.

Acronyms should be treated as normal words, so we have `performApiCall` instead
of `performAPICall`.

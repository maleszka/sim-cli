# sim-cli

A command line front-end to the [SIM][SIM] platform.

<h1 align="center">
  <img src="https://gitlab.com/maleszka/sim-cli/-/raw/e654256dface88578e344decbdbf5d500390f68e/docs/img/round_page.png" alt="Round page screenshot">
</h1>

![Coded 100% by humans](https://img.shields.io/badge/Coded%20by%20human-100%25-success)

SIM is a platform that does its best to handle competitive programming contests
conveniently, providing good tasks management and efficient judgement system.
However, the web UI does not always make it easy to navigate through all the
contests and rounds for the contestant during usual actions, e.g. submitting,
viewing judgement reports.

The sim-cli project is meant to optimize those common use cases and bring SIM
usage closer to the terminal, whilst providing a visually neat user interface.

**Disclaimer**: the project is at an early alpha stage. Many features may not be
implemented yet and many breaking changes are to be expected.

## Installation

You can install sim-cli from the [latest release builds][releases] or download
it from the [Arch User Repository][AUR].

### Building from source

The sim-cli project builds under the [Meson][meson] build system generator
(meson >=0.60) and utilizes CMake module, so [CMake][cmake] also should be
installed (cmake >=3.17).

Other compilation dependencies include:

- `boost`
- `libcurl`

First, configure the build system, invoking the following command in the root of
the repository:

```
meson setup <builddir>
```

You can optionally specify build type with `--buildtype=` argument set to one of
the following values:

- `debug`
- `debugoptimized` (default)
- `release`
- `minsize`

See [meson documentation on `buildtype`][meson_buildtype] for more information.

Then, compile the project:

```
meson compile -C <builddir>
```

Now, you can invoke `cli/simc` directly or tell meson to install it to the
system:

```
meson install -C <builddir>
```

NOTE: the command above might require root privileges.

Custom output directory can be specified using `DESTDIR` environment variable,
`--destdir`, or `--prefix` arguments.

By default, meson will install all the components. If you would like only to
install binary or libraries, specify `--tags` argument. Possible tags are:

- `runtime` --- simc binary and default config
- `devel` --- simc-fetcher library and headers

## Usage

The sim-cli can run in two different modes: interactive (occupying the whole
terminal) and non-interactive (requiring all information to be provided in the
form of command line arguments).

For a complete list of arguments, invoke `simc --help`.

## Configuration

After installing the project, a default configuration file will be placed in
`/usr/local/share/sim-cli/default_config.ini`.

Copy it to the local config directory with the following command:

```sh
cp /usr/local/share/sim-cli/default_config.ini ~/.config/simc.ini
```

That file would have all the options explained and documented.

## Specifying SIM login credentials

Simc is meant to be run frequently, e.g. each time the contestant submits a
solution, therefore it's preferable and recommended to have credentials
specified in the configuration file.

SIM username can be given with an `username` option and password --- `password`
option. Password is, however, not recommended to be specified in plain-text
format, what when the user wants to share their config across devices using a
git repository. That's when `password-cmd` comes in handy.

`password-cmd` lets the user specify a shell command (even a huge one-liner)
that will be executed by simc during its startup to obtain a password string
from stdout of that command. These are some ideas how the user can utilise that:

**1. storing the password in a plain-text file outside the repository**

Supposing the user's password sits in `~/.simc-pass` file:

```conf
password_cmd=cat ~/.simc-pass
```

That's, however, the least recommended way of doing so, because that exposes the
password to be stolen.

**2. storing the password inside an encrypted file**

Supposing the user has already created a [GnuPG key-pair][gpg_guide] assigned to
`foo@bar.com` email address, `~/.simc-pass` file can be encrypted with the
following command:

```sh
gpg --encrypt --armor -r foo@bar.com ~/.simc-pass
```

Now, `password_cmd` can be set this way:

```conf
password_cmd=gpg --decrypt ~/.simc-pass.asc 2>/dev/null
```

**3. typing in password interactively using `pinentry`**

`pinentry` uses its own [protocol][pinentry_protocol], here's an example use:

```conf
password_cmd=echo -e "SETTITLE simc login\nSETDESC Enter your SIM password\nGETPIN" | pinentry 2>/dev/null | grep D | cut -c 3-
```

If the user wants to avoid using a configuration file, all credentials can be
overwritten with `--user` and `--pass` command line options.

## License

This project is licensed under the GNU General Public License v3. Its full
contents can be found in the [LICENSE](LICENSE) file.

[comment]: # (All links should go here:)

[SIM]: https://sim.13lo.pl
[releases]: https://gitlab.com/maleszka/sim-cli/-/releases
[AUR]: https://aur.archlinux.org/packages/sim-cli
[meson]: https://mesonbuild.com/
[cmake]: https://cmake.org/
[meson_buildtype]: https://mesonbuild.com/Builtin-options.html#details-for-buildtype
[gpg_guide]: https://www.howtogeek.com/427982/how-to-encrypt-and-decrypt-files-with-gpg-on-linux/
[pinentry_protocol]: https://info2html.sourceforge.net/cgi-bin/info2html-demo/info2html?%28pinentry%29Protocol

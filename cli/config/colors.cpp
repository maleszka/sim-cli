// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/config/colors.hpp>

namespace simc::config
{

term::TextProps
propsFromStatus(const Palette& colors, SubmissionStatus status)
{
    using namespace term;
    switch (status) {
        case SubmissionStatus::empty:
        case SubmissionStatus::pending:
            return TextProps();
        case SubmissionStatus::ok:
            return TextProps{colors.statusOk, cDefault, aNone};
        case SubmissionStatus::timeLimitExceeded:
            return TextProps{colors.statusTle, cDefault, aNone};
        case SubmissionStatus::memoryLimitExceeded:
            return TextProps{colors.statusMle, cDefault, aNone};
        case SubmissionStatus::wrongAnswer:
            return TextProps{colors.statusWa, cDefault, aNone};
        case SubmissionStatus::compilationFailed:
            return TextProps{colors.statusCf, cDefault, aNone};
        case SubmissionStatus::runtimeError:
            return TextProps{colors.statusRe, cDefault, aNone};
        case SubmissionStatus::checkerError:
            return TextProps{colors.statusCe, cDefault, aNone};
    }
    return TextProps();
}

}  // namespace simc::config

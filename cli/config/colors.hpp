// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

/**
 * \file    cli/config/colors.hpp
 * \brief   Color configuration struct
 * \author  Mikołaj Krzeszowiak
 */

#ifndef CLI_CONFIG_COLORS_HPP_
#define CLI_CONFIG_COLORS_HPP_

#include <cli/term/colors.hpp>

#include <fetcher/data.hpp>

namespace simc::config
{

struct Palette
{
    int statusOk;
    int statusWa;
    int statusTle;
    int statusMle;
    int statusRe;
    int statusCf;
    int statusCe;
    int unobtrusive;
    int primaryAccent;
    int secondaryAccent;
    int selection;
};

term::TextProps propsFromStatus(const Palette& colors, SubmissionStatus status);

}  // namespace simc::config

#endif  // CLI_CONFIG_COLORS_HPP_

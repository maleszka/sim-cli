// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <array>
#include <boost/filesystem.hpp>
#include <cli/config/config.hpp>
#include <cli/term/colors.hpp>
#include <cstdio>
#include <fetcher/utils/logger.hpp>
#include <sstream>
#include <string>

namespace simc::config
{

const int defaultedVerbosity = -1;

Config::Config(int argc, char* argv[]) noexcept
  : od_cli_("Options")
  , od_file_("Configuration file options")
{
    makeOdCli();
    makeOdFile();
    makeOdKeybinds();
    makeOdColors();

    // Save the simc_command_
    if (argc >= 1) simc_command_ = argv[0];

    parseOpts(argc, argv);

    if (err_msg_.empty()) {
        if_interactive_ = command_.empty();
        if_pipe_        = !isatty(fileno(stdout));
        if (if_pipe_) if_colors_ = false;
    } else {
        if_interactive_ = false;
        if_pipe_        = false;
    }

    processVerbosity();
}

// clang-format off
std::string Config::getSimUrl() const { return sim_url_; }
std::string Config::getUsername() const{ return username_; }
std::string Config::getPassword() const { return password_; }
int Config::getVerbosity() const { return verbosity_; }
int Config::getLogVerbosity() const { return log_verbosity_; }
std::string Config::getLogPath() const { return log_path_; }
std::string Config::getSimcCommand() const { return simc_command_; }
std::string Config::getCommand() const { return command_; }
const std::vector<std::string>& Config::getCommandArguments() const { return arguments_; }
bool Config::getIfHelp() const { return if_help_; }
bool Config::getIfVersion() const { return if_version_; }
bool Config::getIfInteractive() const { return if_interactive_; }
bool Config::getIfColors() const { return if_colors_; }
bool Config::getIfPipe() const { return if_pipe_; }
const Keybinds& Config::getKeybinds() const { return keybinds_; }
const Palette& Config::getColors() const { return colors_; }
// clang-format on

std::string
Config::getHelpMessage() const
{
    std::stringstream ss;
    ss << "Usage: " << simc_command_ << " [OPTIONS] [COMMAND] [ARGUMENTS]...\n"
       << "Run Simc, a command line interface to the SIM platform.\n"
       << "By default, it runs interactively; specify COMMAND for the\n"
          "non-interactive mode.\n\n";
    ss << od_cli_;
    return ss.str();
}

std::string
Config::getErrorMessage() const
{
    return err_msg_;
}

void
Config::makeOdCli()
{
    const std::string default_config_path
      = (std::string)getenv("HOME") + "/.config/simc.ini";

    od_cli_.add_options()  //
      ("sim,s",
       po::value<std::string>(&sim_url_)->default_value("https://sim.13lo.pl"),
       "custom SIM instance URL")  //

      ("user,u",
       po::value<std::string>(&username_)->default_value(""),
       "SIM username")  //

      ("pass,p",
       po::value<std::string>(&password_)->default_value(""),
       "SIM password")  //

      ("config,c",
       po::value<std::string>()->default_value(default_config_path),
       "custom configuration file path")  //

      ("verbosity,v",
       po::value<int>(&verbosity_)->default_value(defaultedVerbosity),
       "verbose mode/level")  //

      ("log",
       po::value<std::string>(&log_path_)->default_value(""),
       "log file path")  //

      ("help,h", po::bool_switch(&if_help_), "produce help message")  //

      ("version", po::bool_switch(&if_version_), "show version info")  //
      ;

    // note: '//' are added to the end to make clang-format work properly
}

void
Config::makeOdFile()
{
    od_file_.add_options()  //
      ("General.sim_url",
       po::value<std::string>(&sim_url_),
       "custom SIM instance URL")  //

      ("Credentials.username",
       po::value<std::string>(&username_),
       "SIM username")  //

      ("Credentials.password",
       po::value<std::string>(&password_),
       "SIM password")  //

      ("Credentials.password_cmd",
       po::value<std::string>(&password_cmd_),
       "Command outputting the SIM password")  //
      ;

    file_to_cli_["General.sim-url"]      = "sim";
    file_to_cli_["Credentials.username"] = "user";
    file_to_cli_["Credentials.password"] = "pass";
}

void
Config::makeOdKeybinds()
{
    od_file_.add_options()  //
      ("Keybinds.session_exit",
       po::value<Keybind>(&keybinds_.sessionExit)
         ->default_value(Keybind{{"q", "<C-c>"}}, "q,<C-c>"),
       "Exit session")  //

      ("Keybinds.session_redraw",
       po::value<Keybind>(&keybinds_.sessionRedraw)
         ->default_value(Keybind{{"<C-l>"}}, "<C-l>"),
       "Center cursor and redraw screen")  //

      ("Keybinds.session_reload",
       po::value<Keybind>(&keybinds_.sessionReload)
         ->default_value(Keybind{{"r", "<C-r>"}}, "r,<C-r>"),
       "Reload data and redraw screen")  //

      ("Keybinds.navigation_back",
       po::value<Keybind>(&keybinds_.navigationBack)
         ->default_value(Keybind{{"H", "<Left>", "<Backspace>"}},
                         "H,<Left>,<Backspace>"),
       "Go back in the navigation hierarchy")  //

      ("Keybinds.navigation_down",
       po::value<Keybind>(&keybinds_.navigationDown)
         ->default_value(Keybind{{"j", "<Down>"}}, "j,<Down>"),
       "Focus on the next entry or scroll down")  //

      ("Keybinds.navigation_up",
       po::value<Keybind>(&keybinds_.navigationUp)
         ->default_value(Keybind{{"k", "<Up>"}}, "k,<Up>"),
       "Focus on the previous entry or scroll up")  //

      ("Keybinds.navigation_enter",
       po::value<Keybind>(&keybinds_.navigationEnter)
         ->default_value(Keybind{{"<Enter>"}}, "<Enter>"),
       "Enter focused entry")  //

      ("Keybinds.navigation_exit",
       po::value<Keybind>(&keybinds_.navigationExit)
         ->default_value(Keybind{{"<Enter>", "q"}}, "<Enter>,q"),
       "Exit currently opened page or dialog box")  //

      ("Keybinds.tasks_toggle",
       po::value<Keybind>(&keybinds_.tasksToggle)
         ->default_value(Keybind{{"p"}}, "p"),
       "Toggle tasks (problems) list of the current contest page")  //

      ("Keybinds.search_open",
       po::value<Keybind>(&keybinds_.searchOpen)
         ->default_value(Keybind{{"o"}}, "o"),
       "Open fuzzy search")  //

      ("Keybinds.search_exit",
       po::value<Keybind>(&keybinds_.searchExit)
         ->default_value(Keybind{{"<Escape>", "<C-g>"}}, "<Escape>,<C-g>"),
       "Exit fuzzy search")  //

      ("Keybinds.search_entry_down",
       po::value<Keybind>(&keybinds_.searchEntryDown)
         ->default_value(Keybind{{"<C-j>", "<Down>"}}, "<C-j>,<Down>"),
       "Fuzzy search: move to the next entry")  //

      ("Keybinds.search_entry_up",
       po::value<Keybind>(&keybinds_.searchEntryUp)
         ->default_value(Keybind{{"<C-k>", "<Up>"}}, "<C-k>,<Up>"),
       "Fuzzy search: move to the previous entry")  //

      ("Keybinds.search_entry_select",
       po::value<Keybind>(&keybinds_.searchEntrySelect)
         ->default_value(Keybind{{"<C-l>", "<Tab>", "<Right>"}},
                         "<C-l>,<Tab>,<Right>"),
       "Fuzzy search: select an entry without entering it")  //
      ;
}

void
Config::makeOdColors()
{
    od_file_.add_options()  //
      ("Colors.status_ok",
       po::value<int>(&colors_.statusOk)->default_value(2),
       "The great color of the holy OK")  //

      ("Colors.status_wa",
       po::value<int>(&colors_.statusWa)->default_value(9),
       "Status: wrong answer")  //

      ("Colors.status_tle",
       po::value<int>(&colors_.statusTle)->default_value(3),
       "Status: time limit exceeded")  //

      ("Colors.status_mle",
       po::value<int>(&colors_.statusMle)->default_value(3),
       "Status: memory limit exceeded")  //

      ("Colors.status_re",
       po::value<int>(&colors_.statusRe)->default_value(1),
       "Status: runtime error and friends")  //

      ("Colors.status_cf",
       po::value<int>(&colors_.statusCf)->default_value(5),
       "Status: compilation failed")  //

      ("Colors.status_ce",
       po::value<int>(&colors_.statusCe)->default_value(4),
       "Status: judge error (call the SIM admins pls)")  //

      ("Colors.unobtrusive",
       po::value<int>(&colors_.unobtrusive)->default_value(8),
       "Less imposing, darker color for details")  //

      ("Colors.primary_accent",
       po::value<int>(&colors_.primaryAccent)->default_value(4),
       "TODO: fill me!")  //

      ("Colors.secondary_accent",
       po::value<int>(&colors_.secondaryAccent)->default_value(6),
       "TODO: fill me!")  //

      ("Colors.selection",
       po::value<int>(&colors_.selection)->default_value(8),
       "Background color for selection")  //
      ;
}

void
Config::parseOpts(int argc, char* argv[]) noexcept
{
    po::variables_map vm;

    // Positional arguments
    po::options_description od_cli(od_cli_);
    od_cli.add_options()                              //
      ("command", po::value<std::string>(&command_))  //
      ("arguments", po::value<std::vector<std::string>>(&arguments_));
    po::positional_options_description pod;
    pod.add("command", 1).add("arguments", -1);

    // Parse CLI options
    try {
        auto parsed_cli_opts = po::command_line_parser(argc, argv)
                                 .options(od_cli)
                                 .positional(pod)
                                 .run();
        po::store(parsed_cli_opts, vm);
    } catch (std::exception& e) {
        err_msg_ = e.what();

        // Parse empty options to obtain default values
        char* args[] = {argv[0]};
        po::store(po::command_line_parser(1, args)
                    .options(od_cli)
                    .positional(pod)
                    .run(),
                  vm);
    }

    // Obtain config path
    std::string config_path = "";
    if (err_msg_.empty()
        && boost::filesystem::exists(vm["config"].as<std::string>()))
        config_path = vm["config"].as<std::string>();

    // Parse config file
    if (config_path != "") {
        po::variables_map file_vm;
        try {
            auto parsed_file_opts
              = po::parse_config_file(config_path.c_str(), od_file_);
            po::store(parsed_file_opts, file_vm);
        } catch (std::exception& e) {
            err_msg_ = e.what();
        }

        // Try to insert all file_vm values to vm using our policy
        for (auto& entry : file_vm) {
            std::string name
              = (file_to_cli_.count(entry.first) ? file_to_cli_[entry.first]
                                                 : "");
            auto it = vm.find(name);
            if (it == vm.end()) {
                vm.emplace(entry);
            } else if (it->second.defaulted()) {
                vm.erase(it);
                vm.emplace(entry);
            }
        }
    }

    // Write vm data to Config's members
    po::notify(vm);

    // Fall back to password-cmd if password is unset
    // See https://stackoverflow.com/a/478960/15608830 for more
    if (password_.empty()) {
        std::array<char, 128> buffer;
        std::string result;
        std::unique_ptr<FILE, decltype(&pclose)> pipe(
          popen(password_cmd_.c_str(), "r"), pclose);
        if (!pipe) {
            err_msg_ = "Failed to execute password-cmd";
        } else {
            while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
                result += buffer.data();
            }
        }
        if (*(result.end() - 1) == '\n') {
            result.pop_back();
        }
        password_ = result;
    }

    // Set error message if crucial credentials are unset
    // Ignore check if getting version or help
    if (!(if_help_ || if_version_) && err_msg_.empty()
        && (sim_url_.empty() || username_.empty() || password_.empty()))
        err_msg_ = "required user credentials are not set";
}

void
Config::processVerbosity()
{
    // Initial setup:
    // - log_verbosity_ is unset
    // - if verbosity_ equals defaultedVerbosity, then it was not set by the
    // user
    // - verbosity_ might be overwritten with the `-v` flag

    // When no `-v` flag was specified, set default values
    if (verbosity_ == defaultedVerbosity) {
        verbosity_ = (int)log::SeverityLevel::error;
        log_verbosity_
          = (log_path_.empty() ? (int)log::SeverityLevel::critical + 1
                               : (int)log::SeverityLevel::info);
    }

    // `-v` should adjust log_verbosity_
    else if (!log_path_.empty())
    {
        log_verbosity_ = verbosity_;
    }

    // Can't use clog much in the interactive mode
    if (if_interactive_) {
        verbosity_ = (int)log::SeverityLevel::critical;
    }
}

}  // namespace simc::config

// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak
// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

/**
 * \file    cli/config/config.hpp
 * \brief   Configuration management module
 * \author  Mikołaj Krzeszowiak
 */

#ifndef CLI_CONFIG_CONFIG_HPP_
#define CLI_CONFIG_CONFIG_HPP_

#include <boost/program_options.hpp>
#include <cli/config/colors.hpp>
#include <cli/config/keybinds.hpp>
#include <string>

namespace po = boost::program_options;

namespace simc::config
{

/*******************************************************************************
 * \class     Config
 * \brief     Configuration class
 *
 * This class contains all user-specified configuration options as well as
 * functions used to fetch configuration data.
 *
 * Utilizes Boost's program_options module to parse command-line arguments and
 * a configuration file, no matter whether explicitly specified or defaulted.
 ******************************************************************************/
class Config
{
   protected:
    // Keybinds list
    Keybinds keybinds_;

    // Color configuration
    Palette colors_;

    // Login options
    std::string sim_url_;   //!< url pointing to a SIM instance
    std::string username_;  //!< user's SIM username
    std::string password_;  //!< user's SIM password for the account specified
                            //!< in \ref _username

    // Logging options
    int verbosity_;         //!< stderr verbosity
    int log_verbosity_;     //!< file log verbosity
    std::string log_path_;  //!< log file path

    // Actions
    std::string simc_command_;  //!< command used to run simc
    std::string command_;  //!< command to be used by the non-interactive ui
    std::vector<std::string> arguments_;
    bool if_help_;     //!< whether to print help message
    bool if_version_;  //!< whether to print version info

    // CLI setup
    bool if_interactive_;  //!< whether simc should run in the interactive mode
    bool if_colors_;       //!< whether colors should be enabled
    bool if_pipe_;         //!< whether stdout is redirected

    // Not exposed options
    std::string password_cmd_;  //!< command outputting the user's password

    // Options descriptions
    po::options_description od_cli_;   //!< available options in the cli
    po::options_description od_file_;  //!< available config file options
    std::map<std::string, std::string>
      file_to_cli_;  //!< map config to generic option names

    // Utility
    std::string err_msg_;  //!< eventual parsing error message

   public:
    /**
     * \brief Create configuration object and parse options
     *
     * Tries to parse CLI args and configuration file, and saves parsed values.
     * Fall-backs to defaults and saves error message if parsing fails.
     *
     * @param argc argument count, pass on from \c main()
     * @param argv array of CLI arguments, pass on from \c main()
     *
     * @see getErrorMessage()
     */
    Config(int argc, char* argv[]) noexcept;

    /**
     * \brief Get SIM instance URL from config
     */
    std::string getSimUrl() const;

    /**
     * \brief Get specified user account username
     */
    std::string getUsername() const;

    /**
     * \brief Get SIM account password (in plaintext)
     */
    std::string getPassword() const;

    /**
     * \brief Get verbosity level
     */
    int getVerbosity() const;

    /**
     * \brief Get logger verbosity level
     */
    int getLogVerbosity() const;

    /**
     * \brief Get log file path
     */
    std::string getLogPath() const;

    /**
     * \brief Return the exact string used to run simc
     */
    std::string getSimcCommand() const;

    /**
     * \brief Get non-interactive mode command
     */
    std::string getCommand() const;

    /**
     * \brief Get non-interactive mode command arguments
     */
    const std::vector<std::string>& getCommandArguments() const;

    /**
     * \brief Return true if user wants to display help message
     */
    bool getIfHelp() const;

    /**
     * \brief Return true if user wants to display program version
     */
    bool getIfVersion() const;

    /**
     * \brief Return true if the user requested the interactive mode
     */
    bool getIfInteractive() const;

    /**
     * \brief Return true if the user enabled colors
     */
    bool getIfColors() const;

    /**
     * \brief Return true if stdout is redirected
     */
    bool getIfPipe() const;

    /**
     * \brief Get help message text
     */
    std::string getHelpMessage() const;

    /**
     * \brief Get parsing error message
     */
    std::string getErrorMessage() const;

    /**
     * \brief Get keybinds list
     */
    const Keybinds& getKeybinds() const;

    /**
     * \brief Get colors list
     */
    const Palette& getColors() const;

   protected:
    /**
     * \brief Create an \c options_description object with CLI options (see \ref
     * od_cli_)
     */
    void makeOdCli();

    /**
     * \brief Create an \c options_description object with configuration file
     * options (see \ref od_file_)
     */
    void makeOdFile();

    /**
     * \brief Modify \ref od_file_ to include keybind configuration options
     */
    void makeOdKeybinds();

    /**
     * \brief Modify \ref od_file_ to include color configuration options
     */
    void makeOdColors();

    /**
     * \brief Parse both CLI and config file options
     *
     * Firstly, parses CLI options, and if an error occurs the error message is
     * saved into \c err_msg_. Next, if no errors occur, checks if configuration
     * file exists. If it doesn't, no file is parsed. Otherwise, parses given
     * configuration file and replaces defaulted options with ones from the
     * file.
     *
     * @param argc argument count (directly passed from \c simc::Config
     * constructor)
     * @param argv array of CLI arguments (directly passed from \c simc::Config
     * constructor)
     */
    void parseOpts(int argc, char* argv[]) noexcept;

    /**
     * \brief Policy on how one cli verbosity option affects two: clog verbosity
     *        and log verbosity
     *
     * If no verbosity option is specified, clog verbosity is defaulted to error
     * and log verbosity (if enabled) to info. If log path option is specified,
     * verbosity option can be used to adjust file log verbosity. In all
     * scenarios, clog verbosity is set to critical if simc is about to be run
     * in an interactive mode.
     */
    void processVerbosity();
};

}  // namespace simc::config

#endif  // CLI_CONFIG_CONFIG_HPP_

// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak
// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

/**
 * \file    cli/config/keybinds.hpp
 * \brief   Keybindings definitions
 * \author  Adam Maleszka
 */

#ifndef CLI_CONFIG_KEYBINDS_HPP_
#define CLI_CONFIG_KEYBINDS_HPP_

#include <cli/term/key.hpp>
#include <string>
#include <vector>

namespace simc::term
{
typedef struct Key Key;

}

namespace simc::config
{

struct Keybind
{
    std::vector<std::string> keys;
    bool operator==(const term::Key& key) const;
};
std::istream& operator>>(std::istream& is, Keybind& kbd);

struct Keybinds
{
    // Session
    Keybind sessionExit;
    Keybind sessionRedraw;
    Keybind sessionReload;

    // Navigation
    Keybind navigationBack;
    Keybind navigationDown;
    Keybind navigationUp;
    Keybind navigationEnter;
    Keybind navigationExit;

    // Pages
    Keybind tasksToggle;

    // Fuzzy Search
    Keybind searchOpen;
    Keybind searchExit;
    Keybind searchEntryDown;
    Keybind searchEntryUp;
    Keybind searchEntrySelect;
};

}  // namespace simc::config

#endif  // CLI_CONFIG_KEYBINDS_HPP_

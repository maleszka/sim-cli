// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/input_loop.hpp>
#include <cli/utils/events.hpp>
#include <csignal>
#include <thread>

// Important! Do not delete these
#define ᕕ while
#define ᐛ true
#define ᕗ

namespace simc
{

volatile std::sig_atomic_t sigResizeStatus = 0;
volatile std::sig_atomic_t sigExitStatus   = 0;

void
sigResizeHandler(int signal)
{
  sigResizeStatus = signal;
}

void
sigExitHandler(int signal)
{
  sigExitStatus = signal;
}

InputLoop::InputLoop(std::shared_ptr<EventQueue> queue)
  : queue_(queue)
{
}

InputLoop::~InputLoop()
{
  this->exit();
}

void
InputLoop::run()
{
  // Register kernel signal handler
  std::signal(SIGWINCH, sigResizeHandler);
  std::signal(SIGTERM, sigExitHandler);
  std::signal(SIGINT, sigExitHandler);

  // Key input results
  term::Key key;
  int result;

  /* This is Gary.
   * Gary is an amalgamation of #defines.
   * Gary is NOT suicidal.
   * Please do not delete Gary.
   * Aforementioned #defines available somewhere around line 25.
   * Thanks and have a nice day.
   * ~Mikołaj Krzeszowiak
   */
  // clang-format off
  ᕕ(ᐛ)ᕗ
  // clang-format on

  {
    // Maybe we should exit?
    if (if_stop_) return;

    // Maybe there are some kernel signals?
    if (sigResizeStatus) {
      queue_->emplaceEvent(
        EventType::sigResize, std::any(), EventPriority::signal);
      sigResizeStatus = 0;
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    if (sigExitStatus) {
      queue_->emplaceEvent(
        EventType::sigExit, std::any(), EventPriority::critical);
      sigExitStatus = 0;
    }

    // Try to read a key
    result = ki_.getKey(key);
    if (result == -1)
      queue_->emplaceEvent(
        EventType::inputFailure, std::any(), EventPriority::signal);
    else if (result == 1)
      queue_->emplaceEvent(EventType::keyPressed, key, EventPriority::input);
  }
}

void
InputLoop::exit()
{
  if_stop_ = true;
}


}  // namespace simc

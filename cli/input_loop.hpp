// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak
// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_INPUT_LOOP_HPP_
#define CLI_INPUT_LOOP_HPP_

#include <cli/term/key.hpp>
#include <cli/utils/event_queue.hpp>

namespace simc
{

class InputLoop
{
 protected:
  std::shared_ptr<EventQueue> queue_;
  bool if_stop_ = false;
  term::KeyInput ki_;

 public:
  InputLoop(std::shared_ptr<EventQueue> queue);
  ~InputLoop();
  void run();
  void exit();
};

}  // namespace simc

#endif  // CLI_INPUT_LOOP_HPP_

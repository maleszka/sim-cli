// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/isimc_session.hpp>

#include <cli/input_loop.hpp>
#include <cli/term/window.hpp>
#include <cli/ui/draw.hpp>
#include <cli/ui/pages/root.hpp>
#include <fetcher/utils/macros.hpp>

#include <boost/format.hpp>
#include <chrono>
#include <stdexcept>
#include <thread>

namespace simc
{

iSimcSession::iSimcSession(std::shared_ptr<config::Config> config,
                           std::shared_ptr<term::TermInfo> term)
  : SimcSession(config, term)
  , keybinds_(config->getKeybinds())
  , events_(new EventQueue())
{
    if (!err_msg_.empty()) return;

    BOOST_LOG_FUNCTION();
    LOG(logger_, debug) << "Initializing simc session for the interactive mode";

    // Create the window and command the first things
    term::termSetup(term_.get());
    makeWindow();
    {  // open the root page
        ui::pgRoot::ConstrArgs args = {{events_, config_, locale_, fetcher_}};
        actPageOpenData data;
        data.name = "pgRoot";
        data.args = args;
        events_->emplaceEvent(
          EventType::actPageOpen, data, EventPriority::action);
    }

    // Create input thread
    LOG(logger_, debug) << "Starting input thread";
    input_loop_.reset(new InputLoop(events_));
    input_thread_.reset(new std::thread(&InputLoop::run, input_loop_.get()));
    is_running_ = true;

    // Run events dispatcher
    LOG(logger_, debug) << "Starting events dispatcher";
    uint32_t processed_events = 0;
    while (is_running_) {
        // Obtain an event
        Event event = events_->getEvent();
        processed_events++;
        LOG(logger_, debug)
          << "Got event " << processed_events << ": " << event;

        // Process it
        if (handleEvent(event)) {
        } else {
            LOG(logger_, debug) << "Unhandled event " << event;
        }

        // Update the window
        write();

        // Write log
        log::flush();
    }
}

iSimcSession::~iSimcSession()
{
    BOOST_LOG_FUNCTION();

    // End the input thread
    if (input_loop_ != nullptr) {
        LOG(logger_, debug) << "Ending input thread";
        input_loop_->exit();
        input_thread_->join();
    }

    // Uninitialize terminal output
    current_page_.reset();
    window_.reset();
    term::termRestoreSetup(term_.get());
}

bool
iSimcSession::handleEvent(const Event& event)
{
    BOOST_LOG_FUNCTION();

    // Maybe the current page can handle the event
    if (!window_too_small_ && current_page_ != nullptr
        && current_page_->handleEvent(event))
        return true;

    switch (event.type) {
        case EventType::keyPressed: {
            term::Key key = std::any_cast<term::Key>(event.data);
            LOG(logger_, debug) << "Pressed key '" << key.nsText << "'";

            // Exit session
            if (keybinds_.sessionExit == key) {
                events_->emplaceEvent(EventType::actSessionExit,
                                      std::string(),
                                      EventPriority::critical);
                return true;
            }

            // Redraw window
            if (keybinds_.sessionRedraw == key) {
                if (!window_too_small_) current_page_->draw();
                window_->write(true);
                return true;
            }

            // Reload everything
            if (keybinds_.sessionReload == key) {
                // It is required because we want to use last_page_data_
                SIMC_ASSERT(current_page_ != nullptr);

                current_page_.reset();  // TODO: is it needed?
                fetcher_->forceUpdate();
                openPage();
                return true;
            }

            return false;
        }

        case EventType::inputFailure: {
            // TODO: write to the console log an appropriate message
            return false;
        }

        case EventType::sigResize: {
            resize();
            return true;
        }

        case EventType::sigExit: {
            LOG(logger_, info) << "Exiting due to a kernel signal";
            events_->emplaceEvent(EventType::actSessionExit,
                                  std::string(),
                                  EventPriority::critical);
            return true;
        }

        case EventType::actSessionExit: {
            is_running_ = false;
            if (err_msg_.empty())
                err_msg_ = std::any_cast<std::string>(event.data);
            return true;
        }

        case EventType::actPageOpen: {
            // Create the new page
            last_page_data_ = std::any_cast<actPageOpenData>(event.data);
            openPage();
            return true;
        }

        case EventType::exception: {
            // TODO: force console log to show up
            events_->emplaceEvent(
              EventType::actSessionExit, event.data, EventPriority::critical);
            return true;
        }
    }
    return false;
}

void
iSimcSession::resize()
{
    makeWindow();

    // Resize UI elements
    if (!window_too_small_) {
        SIMC_ASSERT(window_ != nullptr);
        SIMC_ASSERT(current_page_ != nullptr);
        current_page_->resize(window_.get());
    }
}

void
iSimcSession::write()
{
    // Draw UI elements
    if (!window_too_small_) {
        SIMC_ASSERT(current_page_ != nullptr);
        current_page_->draw();
    }

    // Output to the terminal
    SIMC_ASSERT(window_ != nullptr);
    window_->write();
}

void
iSimcSession::openPage()
{
    BOOST_LOG_FUNCTION();
    LOG(logger_, info) << "Openning page '" << last_page_data_.name << "'";
    current_page_.reset(
      ui::Element::makePage(last_page_data_.name, last_page_data_.args));
    current_page_->resize(window_.get());
}

void
iSimcSession::makeWindow()
{
    BOOST_LOG_FUNCTION();

    // Initialize the new window
    window_.reset(new term::Window(term_));
    term::point_t dims = window_->getDimensions();
    LOG(logger_, debug) << "Created new " << std::to_string(dims.col) << "x"
                        << std::to_string(dims.row) << " window";

    // Maybe the window is too small?
    if (dims.row < term::minWindowHeight || dims.col < term::minWindowWidth) {
        window_too_small_ = true;
        LOG(logger_, warning)
          << "Terminal is too small, please resize it to at least "
          << std::to_string(term::minWindowWidth) << "x"
          << std::to_string(term::minWindowHeight);

        ui::drawWindowTooSmall(window_.get());
        window_->write();
    } else {
        // Unset window_too_small_ if window has correct size
        if (window_too_small_) window_too_small_ = false;
    }
}

}  // namespace simc

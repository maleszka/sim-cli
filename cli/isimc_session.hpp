// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_ISIMC_SESSION_HPP_
#define CLI_ISIMC_SESSION_HPP_

#include <cli/simc_session.hpp>
#include <cli/ui/element.hpp>
#include <cli/utils/event_queue.hpp>
#include <thread>

namespace simc
{
class InputLoop;
}

namespace simc
{

class iSimcSession : public SimcSession
{
 protected:
  // Configuration
  const config::Keybinds& keybinds_;

  // Event loop
  bool is_running_ = false;
  std::shared_ptr<EventQueue> events_;
  std::unique_ptr<InputLoop> input_loop_;
  std::unique_ptr<std::thread> input_thread_;

  // UI
  bool window_too_small_ = false;
  std::unique_ptr<ui::Element> current_page_;
  actPageOpenData last_page_data_;

 public:
  iSimcSession(std::shared_ptr<config::Config> config,
               std::shared_ptr<term::TermInfo> term);
  ~iSimcSession();

 protected:
  bool handleEvent(const Event& event);
  void resize();
  void write();
  void openPage();
  void makeWindow();
};

}  // namespace simc

#endif  // CLI_ISIMC_SESSION_HPP_

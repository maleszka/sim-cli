// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/nisimc_session.hpp>

namespace simc
{

niSimcSession::niSimcSession(std::shared_ptr<config::Config> config,
                             std::shared_ptr<term::TermInfo> term)
  : SimcSession(config, term)
{
  BOOST_LOG_FUNCTION();
  LOG(logger_, debug)
    << "Initializing simc session for the non-interactive mode";
}

niSimcSession::~niSimcSession() {}

}  // namespace simc

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_NISIMC_SESSION_HPP_
#define CLI_NISIMC_SESSION_HPP_

#include <cli/simc_session.hpp>

namespace simc
{

class niSimcSession : public SimcSession
{
 public:
  niSimcSession(std::shared_ptr<config::Config> config,
                std::shared_ptr<term::TermInfo> term);
  ~niSimcSession();
};

}  // namespace simc

#endif  // CLI_NISIMC_SESSION_HPP_

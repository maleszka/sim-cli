// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak
// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/isimc_session.hpp>
#include <cli/nisimc_session.hpp>
#include <cli/simc.hpp>
#include <cli/term/terminfo.hpp>
#include <cli/utils/config.hpp>
#include <cli/utils/logger_sinks.hpp>
#include <fetcher/utils/macros.hpp>

namespace simc
{

Simc::Simc(int argc, char* argv[])
  : config_(new config::Config(argc, argv))
{
    BOOST_LOG_FUNCTION();

    // Configure the logging engine initially
    log::setupCore();
    log::setupBasicFileSink(config_->getLogPath(), config_->getLogVerbosity());
    log::setupBasicClogSink(config_->getVerbosity());
    LOG(logger_, info) << "Logging engine initialised";

    // Log CLI setup decisions
    if (config_->getIfPipe()) {
        LOG(logger_, debug)
          << "stdout is not a tty, maybe a pipe, disabling color support";
    }

    // Maybe `--help` or `--version` specified?
    if (config_->getIfHelp() || config_->getIfVersion()) {
        LOG(logger_, info)
          << "Help or version action specified, ending initialization";
        log::flush();
        return;
    }

    // Handle command argument errors
    if (!config_->getErrorMessage().empty()) {
        err_msg_ = "Failed parsing cli args: " + config_->getErrorMessage();
        LOG(logger_, critical) << err_msg_;
        log::flush();
        return;
    }

    // Try to load the terminfo database
    if (!config_->getIfPipe()) {
        term_.reset(new term::TermInfo());

        if (!term_->isOpen()) {
            if (config_->getIfInteractive()) {
                err_msg_ = "Could not load the terminfo database. "
                           "Make sure that TERM and TERMINFO_DIRS are set";
                LOG(logger_, critical) << err_msg_;
                log::flush();
                return;
            } else {
                LOG(logger_, warning)
                  << "Could not load the terminfo database. "
                  << "Falling back to the legacy mode";
            }
        } else {
            LOG(logger_, info)
              << "Successfully loaded a terminfo database for '"
              << term_->getName() << "'";
        }
    }

    // Pipe forces non-interactive mode that requires commands
    if (config_->getIfPipe()
        && (config_->getCommand().empty() || config_->getIfInteractive()))
    {
        err_msg_
          = "Pipe forces the non-interactive mode but no command is specified";
        LOG(logger_, critical) << err_msg_;
        log::flush();
        return;
    }

    // Finish simc initialization logging
    LOG(logger_, info) << "Simc successfully initialized";
    log::flush();
}

Simc::~Simc() {}

void
Simc::start()
{
    // Help message
    if (config_->getIfHelp()) {
        std::cout << config_->getHelpMessage() << "\n";
        return;
    }

    // Version message
    if (config_->getIfVersion()) {
        std::cout << "simc version " << version << "\n";
        return;
    }

    // Don't start the thing if intialization failed
    if (!err_msg_.empty()) return;

    // Start the sessions
    if (config_->getIfInteractive()) {
        session_.reset(new iSimcSession(config_, term_));
    } else {
        session_.reset(new niSimcSession(config_, term_));
    }
}

std::string
Simc::getErrorMessage() const
{
    if (err_msg_.empty() && session_ != nullptr)
        return session_->getErrorMessage();
    return err_msg_;
}

}  // namespace simc

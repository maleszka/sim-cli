// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/simc_session.hpp>

#include <cli/term/window.hpp>
#include <fetcher/utils/macros.hpp>

namespace simc
{

SimcSession::SimcSession(std::shared_ptr<config::Config> config,
                         std::shared_ptr<term::TermInfo> term)
  : config_(config)
  , term_(term)
{
    BOOST_LOG_FUNCTION();
    LOG(logger_, info) << "Initializing simc session";

    // Initialize fetcher
    LOG(logger_, info) << "Logging in as '" << config_->getUsername() << "' to "
                       << config_->getSimUrl();
    try {
        fetcher_.reset(new Fetcher(config_->getSimUrl(),
                                   config_->getUsername(),
                                   config_->getPassword()));
    } catch (const std::exception& e) {
        err_msg_ = (std::string) "login failure: " + e.what();
        LOG(logger_, critical) << err_msg_;
        log::flush();
        return;
    }

    // Get the locale
    const char* locale_name = std::getenv("LANG");
    if (locale_name == nullptr) locale_name = "en_US.utf8";
    LOG(logger_, info) << "Using locale '" << locale_name << "'";
    locale_.reset(new std::locale(locale_name));
}

SimcSession::~SimcSession()
{
    BOOST_LOG_FUNCTION();
    LOG(logger_, info) << "Logging out of the session";
    fetcher_.reset();
}

std::string
SimcSession::getErrorMessage() const
{
    return err_msg_;
}

}  // namespace simc

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_SIMC_SESSION_HPP_
#define CLI_SIMC_SESSION_HPP_

#include <cli/config/config.hpp>
#include <fetcher/fetcher.hpp>
#include <fetcher/utils/logger.hpp>

namespace simc::term
{
class TermInfo;
class Window;
}  // namespace simc::term

namespace simc
{

class SimcSession
{
   protected:
    // Configuration
    std::shared_ptr<config::Config> config_;
    std::shared_ptr<std::locale> locale_;

    // Output instance
    std::shared_ptr<term::TermInfo> term_;
    std::shared_ptr<term::Window> window_;

    // Data
    std::shared_ptr<Fetcher> fetcher_;

    // Utilities
    log::logger_t logger_;
    std::string err_msg_ = "";

   public:
    SimcSession(std::shared_ptr<config::Config> config,
                std::shared_ptr<term::TermInfo> term);
    virtual ~SimcSession();
    std::string getErrorMessage() const;
};

}  // namespace simc

#endif  // CLI_SIMC_SESSION_HPP_

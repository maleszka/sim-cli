// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_TERM_BUFFER_HPP_
#define CLI_TERM_BUFFER_HPP_

#include <cli/term/frame.hpp>
#include <vector>

namespace simc::term
{

class Buffer : public Frame
{
   protected:
    std::vector<std::vector<Cell>> data_;
    std::vector<bool> touched_;
    Buffer* output_ = nullptr;
    point_t output_base_;

   public:
    Buffer(Frame* output);
    Buffer(point_t dims);
    void setOutput(Frame* output);

    void putCell(point_t pos, const Cell& c);
    const Cell& getCell(point_t pos);

    void setCursor(point_t pos);
    void hideCursor();
    virtual void write(bool force = false);
    void clear();

    virtual void copyLine(point_t start,
                          std::vector<Cell>::const_iterator first,
                          std::vector<Cell>::const_iterator last);
    std::vector<Cell>::const_iterator rowBegin(int row) const;
    std::vector<Cell>::const_iterator rowEnd(int row) const;
};

}  // namespace simc::term

#endif  // CLI_TERM_BUFFER_HPP_

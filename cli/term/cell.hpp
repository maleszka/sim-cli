// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_TERM_CELL_HPP_
#define CLI_TERM_CELL_HPP_

#include <cli/term/colors.hpp>

namespace simc::term
{

class Cell
{
   protected:
    char16_t ch_;
    TextProps props_;

   public:
    Cell(const char16_t& ch = u' ', const TextProps& props = TextProps());
    std::string encode(TermInfo* term) const;
    std::string encode(TermInfo* term, const Cell& last) const;
    bool operator==(const Cell& other) const;
    bool operator!=(const Cell& other) const;
    const char16_t& getCh() const;
    const TextProps& getProps() const;
};

}  // namespace simc::term

#endif  // CLI_TERM_CELL_HPP_

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/term/buffer.hpp>
#include <cli/term/frame.hpp>
#include <fetcher/utils/macros.hpp>

namespace simc::term
{

Frame::Frame(Frame* parent, point_t offset, point_t dims, bool pad)
{
    // Find the root
    SIMC_ASSERT(parent != nullptr);
    root_ = (parent->root_ == nullptr ? reinterpret_cast<Buffer*>(parent)
                                      : parent->root_);

    // Calculate absolute coordinates
    base_ = parent->base_ + offset;

    // Check if base fits in the parent
    point_t br_corner = parent->base_ + parent->dims_;
    SIMC_ASSERT(base_.row < br_corner.row && base_.col < br_corner.col);

    // Calculate dims
    dims_ = (pad ? parent->dims_ - dims - offset : dims);
    SIMC_ASSERT(dims_.row >= 0 && dims_.col >= 0);
    SIMC_ASSERT(base_.row + dims_.row <= br_corner.row
                && base_.col + dims_.col <= br_corner.col);
}

Frame::Frame()
{
    root_ = nullptr;
    base_ = {0, 0};
}

Frame::~Frame() {}

void
Frame::putCell(point_t pos, const Cell& c)
{
    if (pos.row >= dims_.row || pos.col >= dims_.col) return;
    if (root_ == nullptr) return;
    root_->putCell(base_ + pos, c);
}

void
Frame::putCh(point_t pos, const char16_t& ch, const TextProps& props)
{
    putCell(pos, Cell(ch, props));
}

void
Frame::putStr(point_t pos, const std::u16string& str, const TextProps& props)
{
    for (size_t i = 0; i < str.size(); ++i)
        putCell(point_t(pos.row, pos.col + i), Cell(str[i], props));
}

void
Frame::putStr(point_t pos, const CellString& str)
{
    for (size_t i = 0; i < str.size(); ++i)
        putCell(point_t(pos.row, pos.col + i), str[i]);
}

void
Frame::setCursor(point_t pos)
{
    SIMC_ASSERT(pos.row >= 0 && pos.col >= 0);
    SIMC_ASSERT(pos.row < dims_.row && pos.col < dims_.col);
    root_->setCursor(pos + base_);
}

void
Frame::hideCursor()
{
    root_->hideCursor();
}

const point_t&
Frame::getDimensions() const
{
    return dims_;
}

}  // namespace simc::term

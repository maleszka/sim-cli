// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_TERM_FRAME_HPP_
#define CLI_TERM_FRAME_HPP_

#include <cli/term/cell.hpp>
#include <cli/term/cell_string.hpp>
#include <cli/term/colors.hpp>
#include <cli/term/point.hpp>

namespace simc::term
{

class Buffer;

class Frame
{
   protected:
    Buffer* root_;
    point_t dims_;
    point_t base_;

   public:
    Frame(Frame* parent, point_t offset, point_t dims, bool pad = false);
    virtual ~Frame();

    virtual void putCell(point_t pos, const Cell& c);
    void putCh(point_t pos,
               const char16_t& ch,
               const TextProps& props = TextProps());
    void putStr(point_t pos,
                const std::u16string& str,
                const TextProps& props = TextProps());
    void putStr(point_t pos, const CellString& str);

    virtual void setCursor(point_t pos);
    virtual void hideCursor();

    const point_t& getDimensions() const;

   protected:
    Frame();
    friend class Buffer;
};

}  // namespace simc::term

#endif  // CLI_TERM_FRAME_HPP_

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

/**
 * \file    cli/term/key.hpp
 * \brief   Wrapper around termkey library
 * \author  Adam Maleszka
 */

#ifndef CLI_TERM_KEY_HPP_
#define CLI_TERM_KEY_HPP_

#include <memory>
#include <string>

typedef struct TermKey TermKey;
struct pollfd;

namespace simc::term
{

typedef struct Key
{
    bool single;
    std::string nsText;
} Key;

class KeyInput
{
   protected:
    std::unique_ptr<TermKey, void (*)(TermKey*)> tk_;
    std::unique_ptr<pollfd> poll_;

    const int short_wait_ = 50;
    const int long_wait_  = 200;

    void* key_            = nullptr;
    int current_wait_     = long_wait_;

   public:
    KeyInput();
    ~KeyInput();

    int getKey(Key& key);

   protected:
    void writeKey(Key& key);
};

}  // namespace simc::term

#endif  // CLI_TERM_KEY_HPP_

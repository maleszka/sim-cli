// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_TERM_TERMINFO_HPP_
#define CLI_TERM_TERMINFO_HPP_

#include <any>
#include <cli/term/point.hpp>
#include <memory>
#include <string>
#include <vector>

typedef struct unibi_term unibi_term;

namespace simc::term
{

class TermInfo
{
   protected:
    const char* term_name_ = nullptr;
    std::unique_ptr<unibi_term, void (*)(unibi_term*)> term_;

   public:
    TermInfo();
    ~TermInfo();

    const char* getName() const;
    bool isOpen() const;

    std::string queryEscapeCode(std::any command,
                                std::vector<std::any> params = {});
    int queryCapability(std::any cap);
    point_t getDimensions() const;
};

}  // namespace simc::term

#endif  // CLI_TERM_TERMINFO_HPP_

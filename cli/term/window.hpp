// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_TERM_WINDOW_HPP_
#define CLI_TERM_WINDOW_HPP_

#include <cli/term/buffer.hpp>
#include <vector>

namespace simc::term
{

const int minWindowHeight = 8;
const int minWindowWidth  = 60;

void termSetup(TermInfo* term);
void termRestoreSetup(TermInfo* term);


class Window : public Buffer
{
   protected:
    std::shared_ptr<TermInfo> term_;
    point_t cursor_;
    bool cursor_visible_ = false;

   public:
    Window(std::shared_ptr<TermInfo> term);
    ~Window();

    void write(bool force = false);
    void setCursor(point_t pos);
    void hideCursor();
};

}  // namespace simc::term

#endif  // CLI_TERM_WINDOW_HPP_

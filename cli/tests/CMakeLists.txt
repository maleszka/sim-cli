# Remove warnings
get_directory_property(dir_options COMPILE_OPTIONS)
list(REMOVE_ITEM dir_options "-Wpedantic" "-Wextra")
set_directory_properties(PROPERTIES COMPILE_OPTIONS "${dir_options}")

# Tests list
create_test_sourcelist(CliTestSources cli_tests.cpp
  logging.cpp
  config_defaults.cpp
)

# Enable asserts, even in Release build type
string(REPLACE "/DNDEBUG" "" CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE}")
string(REPLACE "-DNDEBUG" "" CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE}")
string(REPLACE "/DNDEBUG" "" CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
string(REPLACE "-DNDEBUG" "" CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")

# Compilation settings
add_executable(cli_tests ${CliTestSources})
set_target_properties(cli_tests PROPERTIES
  CXX_STANDARD 17
  CXX_STANDARD_REQUIRED OFF
  CXX_EXTENSIONS OFF
)
target_link_libraries(cli_tests PRIVATE simc-lib)

# Register all tests
set(CliTests ${CliTestSources})
remove(CliTests cli_tests.cpp)
foreach(test_file ${CliTests})
  get_filename_component(test ${test_file} NAME_WE)
  add_test(NAME ${test} COMMAND cli_tests ${test})
endforeach()

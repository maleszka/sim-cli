#include <cli/config/config.hpp>
#include <cli/utils/logger.hpp>

int
config_defaults(int argc, char* argv[])
{
    using namespace simc;
    using namespace simc::config;

    // Initialise config with some trash
    char* args[]
      = {argv[0], (char*)"-08asd", (char*)"980", (char*)"----heeeellp"};
    Config config(4, args);

    // It should throw an error message
    assert(config.getErrorMessage() == "unrecognised option '-08asd'");

    // But it should still work
    assert(config.getSimUrl() == "https://sim.13lo.pl");
    assert(config.getVerbosity() == (int)log::SeverityLevel::error);
    assert(config.getLogVerbosity() == (int)log::SeverityLevel::critical + 1);

    return 0;
}

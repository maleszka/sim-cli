#include <cli/config/config.hpp>
#include <cli/utils/logger.hpp>
#include <fstream>

int
logging(int argc, char* argv[])
{
    using namespace simc;
    using namespace simc::config;

    // Setup configuration options
    char* args[] = {argv[0], (char*)"-v", (char*)"1", (char*)"--log=simc.log"};
    Config* config = new Config(4, args);
    assert(config->getVerbosity() == (int)log::SeverityLevel::error);
    assert(config->getLogVerbosity() == (int)log::SeverityLevel::info);
    assert(config->getLogPath() == "simc.log");

    // Setup logging module
    log::setupCore();
    log::setupBasicFileSink(config->getLogPath(), config->getLogVerbosity());
    log::logger_t lg;

    // Output sample messages
    BOOST_LOG_FUNCTION();
    LOG(lg, debug) << "This should not be sent";
    LOG(lg, info) << "But this indeed";
    log::flush();

    // Read log from file
    std::vector<std::string> lines;
    std::string line;
    std::ifstream log_file(config->getLogPath().c_str());
    assert(log_file.is_open());
    while (std::getline(log_file, line)) lines.emplace_back(line);
    log_file.close();

    // Check log output
    assert(lines.size() == 1);
    line = lines[0];
    assert(line.find("[logging:22]") != line.size());
    assert(line.find("Info") != line.size());
    assert(line.find("But this indeed") != line.size());

    // Tidy up
    delete config;
    return 0;
}

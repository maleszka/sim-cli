// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka
// Copyright (C) 2023 Mikołaj Krzeszowiak

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/term/colors.hpp>
#include <cli/term/frame.hpp>
#include <cli/term/window.hpp>
#include <cli/ui/draw.hpp>
#include <cli/utils/string.hpp>
#include <cli/utils/time.hpp>
#include <fetcher/utils/macros.hpp>

namespace simc::ui
{

void
drawSeparator(term::Frame* frame,
              int row,
              const term::CellString& text,
              int sep_color)
{
    SIMC_ASSERT(frame != nullptr);
    SIMC_ASSERT(frame->getDimensions().row > row && row >= 0);
    SIMC_ASSERT((int)text.size() < frame->getDimensions().col);

    // Generate three parts
    std::u16string left, right;
    if (text.empty()) {
        while ((int)left.size() + 2 <= frame->getDimensions().col)
            left += u". ";
        if ((int)left.size() < frame->getDimensions().col) left += u".";
    } else {
        int non_text_space = frame->getDimensions().col - text.size();
        int dots_before    = non_text_space / 11;
        SIMC_ASSERT(non_text_space >= 0);

        for (int i = 0; i < dots_before; ++i) left += u". ";
        int remaining_space = non_text_space - dots_before * 2;
        while ((int)right.size() + 2 <= remaining_space) right += u" .";
        if ((int)right.size() < remaining_space) right += u" ";
    }

    // Output
    term::TextProps sep_props{sep_color, term::cDefault, term::aNone};
    frame->putStr({row, 0}, left, sep_props);
    frame->putStr({row, (int)left.size()}, text);
    frame->putStr({row, (int)left.size() + (int)text.size()}, right, sep_props);
}

void
drawTitle(term::Frame* frame, term::CellString cs_text)
{
    SIMC_ASSERT(frame != nullptr);
    SIMC_ASSERT(frame->getDimensions().row >= 1);
    SIMC_ASSERT(frame->getDimensions().col >= 8);

    // Shorten the text
    cs_text = csShorten(cs_text, frame->getDimensions().col - 4);

    // Calculate text size and separator size
    int tl                   = cs_text.size();
    int sepLength            = (frame->getDimensions().col - tl - 4) / 2;
    std::u16string sepString = u"";

    // Generate separator string with appropriate length
    for (int i = 0; i < sepLength; ++i) sepString += u"─";

    // Output
    frame->putStr({0, 0}, sepString);
    frame->putStr({0, sepLength}, u"┐ ");
    frame->putStr({0, sepLength + 2}, cs_text);
    frame->putStr({0, sepLength + tl + 2}, u" ┌");
    frame->putStr({0, sepLength + tl + 4}, sepString);
    frame->putCh({0, frame->getDimensions().col - 1}, u'─');
}

void
drawWindowTooSmall(term::Frame* frame)
{
    SIMC_ASSERT(frame != nullptr);

    // Check for regular size notification
    if (frame->getDimensions().row >= 3 && frame->getDimensions().col >= 25) {
        // Messages
        std::u16string msg_toosmall = u"Terminal size too small";
        std::u16string msg_current
          = u"Current: " + to_u16string(frame->getDimensions().col) + u"x"
            + to_u16string(frame->getDimensions().row);
        std::u16string msg_required
          = u"Required: " + to_u16string(term::minWindowWidth) + u"x"
            + to_u16string(term::minWindowHeight);

        // Calculate position
        int startRow    = (frame->getDimensions().row - 3) / 2;
        int startColumn = (frame->getDimensions().col - 23) / 2;

        // Output
        frame->putStr({startRow, startColumn}, msg_toosmall, {-1, -1, 32});
        frame->putStr({startRow + 1, startColumn}, msg_current, {1, -1, 0});
        frame->putStr({startRow + 2, startColumn}, msg_required, {2, -1, 0});

        // Check for small size notification
    } else if (frame->getDimensions().row >= 1
               && frame->getDimensions().col >= 9)
    {
        // One constant message
        frame->putStr({0, 0}, u"Too small", {1, -1, 0});

        // Too small? Don't generate anything.
    } else {
    }
}

term::CellString
csShorten(const term::CellString& str, int length, const term::TextProps& props)
{
    if (length >= (int)str.size()) return str;
    if (length < 5) return term::CellString();
    term::CellString results = str.substr(0, length);
    results[length - 4]      = term::Cell(u' ', props);
    results[length - 3]      = term::Cell(u'.', props);
    results[length - 2]      = term::Cell(u'.', props);
    results[length - 1]      = term::Cell(u'.', props);
    return results;
}

term::CellString
csPadLeft(const term::CellString& str, int length, const term::TextProps& props)
{
    term::CellString results;
    while ((int)results.size() + (int)str.size() < length)
        results += term::CellString(u" ", props);
    results += str;
    return results;
}

term::CellString
csShortStatus(const config::Palette& colors, SubmissionStatus status)
{
    std::u16string text;
    switch (status) {
        case SubmissionStatus::empty:
        case SubmissionStatus::pending:
            text = u"   ";
            break;
        case SubmissionStatus::ok:
            text = u" OK";
            break;
        case SubmissionStatus::timeLimitExceeded:
            text = u"TLE";
            break;
        case SubmissionStatus::memoryLimitExceeded:
            text = u"MLE";
            break;
        case SubmissionStatus::wrongAnswer:
            text = u" WA";
            break;
        case SubmissionStatus::compilationFailed:
            text = u" CF";
            break;
        case SubmissionStatus::runtimeError:
            text = u" RE";
            break;
        case SubmissionStatus::checkerError:
            text = u" CE";
            break;
    }
    return term::CellString(text, config::propsFromStatus(colors, status));
}

term::CellString
csTimeRange(boost::posix_time::ptime utc_start,
            boost::posix_time::ptime utc_end,
            int color)
{
    const auto& start = localFromUtc(utc_start);
    const auto& end   = localFromUtc(utc_end);

    if ((start.is_neg_infinity() ^ end.is_pos_infinity())
        || (!start.is_special() && !end.is_special()))
    {
        // Main text
        std::u16string text;
        if (start.is_neg_infinity()) {
            text = u"to " + ptimeToString(end);
        } else if (end.is_pos_infinity()) {
            text = u"from " + ptimeToString(start);
        } else {
            text = ptimeToString(start) + u" -- " + ptimeToString(end);
        }
        return term::CellString(
          text, term::TextProps{color, term::cDefault, term::aNone});
    }
    return term::CellString();
}

term::CellString
csTimeRangeActive(boost::posix_time::ptime utc_start,
                  boost::posix_time::ptime utc_end,
                  const config::Palette& colors)
{
    const auto& utc_now = boost::posix_time::second_clock::universal_time();
    bool active         = utc_start <= utc_now && utc_now <= utc_end;
    if (active
        && ((utc_start.is_neg_infinity() ^ utc_end.is_pos_infinity())
            || (!utc_start.is_special() && !utc_end.is_special())))
    {
        return term::CellString(u" (")
               + term::CellString(u"now",
                                  term::TextProps{colors.secondaryAccent,
                                                  term::cDefault,
                                                  term::aNone})
               + term::CellString(u")");
    }
    return term::CellString();
}

}  // namespace simc::ui

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UI_DRAW_HPP_
#define CLI_UI_DRAW_HPP_

#include <cli/config/colors.hpp>
#include <cli/term/cell.hpp>
#include <cli/term/cell_string.hpp>
#include <string>

namespace simc::term
{
class Frame;
}

namespace simc::ui
{

void drawSeparator(term::Frame* frame,
                   int row,
                   const term::CellString& text = term::CellString(),
                   int sep_color                = term::cDefault);

void drawTitle(term::Frame* frame, term::CellString cs_text);

void drawWindowTooSmall(term::Frame* frame);

term::CellString csShorten(const term::CellString& str,
                           int length,
                           const term::TextProps& default_props
                           = term::TextProps());

term::CellString csPadLeft(const term::CellString& str,
                           int length,
                           const term::TextProps& default_props
                           = term::TextProps());

term::CellString csShortStatus(const config::Palette& colors,
                               SubmissionStatus status);

term::CellString csTimeRange(boost::posix_time::ptime utc_start,
                             boost::posix_time::ptime utc_end,
                             int color = term::cDefault);

term::CellString csTimeRangeActive(boost::posix_time::ptime utc_start,
                                   boost::posix_time::ptime utc_end,
                                   const config::Palette& colors);

}  // namespace simc::ui


#endif  // CLI_UI_DRAW_HPP_

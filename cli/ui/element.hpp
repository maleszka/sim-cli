// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UI_ELEMENT_HPP_
#define CLI_UI_ELEMENT_HPP_

#include <cli/config/config.hpp>
#include <cli/utils/event_queue.hpp>

#include <any>
#include <functional>
#include <map>
#include <string>

namespace simc
{
class Fetcher;
}

namespace simc::term
{
class Frame;
class Buffer;
}  // namespace simc::term

namespace simc::ui
{

struct BasicConstrArgs
{
    std::shared_ptr<EventQueue> events;
    std::shared_ptr<const config::Config> config;
    std::shared_ptr<const std::locale> locale;
    std::shared_ptr<Fetcher> fetcher;
};

class Element
{
   protected:
    term::Frame* main_frame_ = nullptr;

   public:
    Element();
    virtual ~Element();

    virtual void resize(term::Frame* parent)     = 0;
    virtual void draw()                          = 0;
    virtual bool handleEvent(const Event& event) = 0;
    virtual std::string getName() const          = 0;

    static Element* makePage(std::string name, const std::any& args);

   protected:
    inline static std::map<std::string,
                           std::function<Element*(const std::any& args)>>
      page_factories_;

    template<class>
    friend class PageFactoryMaker;
};

template<class T>
class PageFactoryMaker
{
   private:
    std::string name_;

   public:
    PageFactoryMaker(const char* name)
      : name_(name)
    {
        if (!Element::page_factories_.count(name))
            Element::page_factories_[name]
              = [](const std::any& args) -> Element*
            { return new T(std::any_cast<typename T::ConstrArgs>(args)); };
    }
    ~PageFactoryMaker() { Element::page_factories_.erase(name_); }
};

}  // namespace simc::ui

#endif  // CLI_UI_ELEMENT_HPP_

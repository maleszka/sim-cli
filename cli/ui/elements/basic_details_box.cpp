// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/term/buffer.hpp>
#include <cli/term/frame.hpp>
#include <cli/ui/draw.hpp>
#include <fetcher/utils/macros.hpp>

#include <cli/ui/elements/basic_details_box.hpp>

namespace simc::ui
{

elBasicDetailsBox::elBasicDetailsBox(const ConstrArgs& args)
  : field_names_(args.field_names)
  , cs_field_values_(args.field_values)
  , margin_color_(args.margin_color)
{
    SIMC_ASSERT(args.field_names.size() == args.field_values.size());

    // Calculate max field name length
    left_length_ = 0;
    for (const auto& name : field_names_) {
        left_length_ = std::max(left_length_, (int)name.length());
    }
}

elBasicDetailsBox::~elBasicDetailsBox() {}

void
elBasicDetailsBox::resize(term::Frame* parent)
{
    SIMC_ASSERT(parent != nullptr);

    // Construct frames & buffers
    main_frame_ = parent;
    main_buffer_.reset(new term::Buffer(main_frame_));

    // Calculate max field value length
    int max_right_length = main_frame_->getDimensions().col - left_length_
                           - separatorMargin - leftRightGap;

    // Shorten field values and calculate right side length
    cs_short_field_values_.clear();
    right_length_ = 0;
    for (const auto& cs_value : cs_field_values_) {
        term::CellString cs_short = csShorten(cs_value, max_right_length);
        cs_short_field_values_.push_back(cs_short);
        right_length_ = std::max(right_length_, (int)cs_short.size());
    }

    // Create centered subframe
    int cframe_height = cs_short_field_values_.size();
    int cframe_width
      = left_length_ + separatorMargin + leftRightGap + right_length_;
    int starting_column = (main_frame_->getDimensions().col - cframe_width) / 2;
    centered_frame_.reset(new term::Frame(
      main_buffer_.get(), {0, starting_column}, {cframe_height, cframe_width}));
}

void
elBasicDetailsBox::draw()
{
    // Output fields to centered subframe
    for (int i = 0; i < (int)field_names_.size(); ++i) {
        centered_frame_->putCh({i, 0}, u'│', margin_color_);
        centered_frame_->putStr({i, separatorMargin}, field_names_[i]);
        centered_frame_->putStr(
          {i, left_length_ + separatorMargin + leftRightGap},
          csPadLeft(cs_short_field_values_[i], right_length_));
    }

    // Write to buffer
    main_buffer_->write();
    main_buffer_->clear();
}

bool
elBasicDetailsBox::handleEvent(const Event& event)
{
    (void)event;
    return false;
}

std::string
elBasicDetailsBox::getName() const
{
    return "elBasicDetailsBox";
}

int
elBasicDetailsBox::getHeight() const
{
    return field_names_.size();
}

}  // namespace simc::ui

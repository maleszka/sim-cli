// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UI_ELEMENTS_BASIC_DETAILS_BOX_HPP_
#define CLI_UI_ELEMENTS_BASIC_DETAILS_BOX_HPP_

#include <cli/term/cell_string.hpp>
#include <cli/ui/element.hpp>
#include <vector>

namespace simc::ui
{

class elBasicDetailsBox : public Element
{
   public:
    struct ConstrArgs : BasicConstrArgs
    {
        std::vector<std::u16string> field_names = std::vector<std::u16string>();
        std::vector<term::CellString> field_values
          = std::vector<term::CellString>();
        term::TextProps margin_color = term::TextProps();
    };

   protected:
    std::vector<std::u16string> field_names_;
    std::vector<term::CellString> cs_field_values_;
    std::vector<term::CellString> cs_short_field_values_;

    std::unique_ptr<term::Buffer> main_buffer_;
    std::unique_ptr<term::Frame> centered_frame_;

    term::TextProps margin_color_;
    const int separatorMargin = 2;
    const int leftRightGap    = 2;
    int left_length_;
    int right_length_;

   public:
    elBasicDetailsBox(const ConstrArgs& args);
    ~elBasicDetailsBox();

    void resize(term::Frame* parent);
    void draw();
    bool handleEvent(const Event& event);
    std::string getName() const;

    int getHeight() const;
};

}  // namespace simc::ui

#endif  // CLI_UI_ELEMENTS_BASIC_DETAILS_BOX_HPP_

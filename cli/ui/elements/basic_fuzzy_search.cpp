// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/elements/basic_fuzzy_search.hpp>

#include <cli/term/buffer.hpp>
#include <cli/term/cell_string.hpp>
#include <cli/term/frame.hpp>
#include <cli/ui/draw.hpp>
#include <cli/ui/elements/one_line_input.hpp>
#include <cli/utils/fuzzy_search.hpp>
#include <cli/utils/string.hpp>
#include <fetcher/utils/macros.hpp>

namespace simc::ui
{

elBasicFuzzySearch::elBasicFuzzySearch(const ConstrArgs& args)
  : events_(args.events)
  , config_(args.config)
  , locale_(args.locale)
  , fetcher_(args.fetcher)
  , ids_(args.ids)
  , us_names_(args.us_names)
  , cs_margin_(args.cs_margin)
{
    nentries_         = args.ids.size();
    nmatched_entries_ = nentries_;
    SIMC_ASSERT(args.us_names.size() == nentries_);
    SIMC_ASSERT(args.cs_margin.size() == nentries_);

    // Construct ids
    us_ids_.resize(nentries_);
    for (size_t i = 0; i < nentries_; ++i)
        us_ids_[i] = to_u16string(args.ids[i]);

    // Calculate max_id_length and max_margin_length
    for (size_t i = 0; i < nentries_; ++i) {
        max_id_length_ = std::max(max_id_length_, (int)us_ids_[i].size());
        max_margin_length_
          = std::max(max_margin_length_, (int)cs_margin_[i].size());
    }

    // Construct backend instance
    std::vector<FuzzySearch::entry_t> backend_data(nentries_);
    for (size_t i = 0; i < nentries_; ++i)
        backend_data[i] = {usToLowercase(us_names_[i], *locale_), us_ids_[i]};
    backend_.reset(new FuzzySearch(backend_data, locale_));

    // Construct input box
    input_.reset(new elOneLineInput());
}

elBasicFuzzySearch::~elBasicFuzzySearch()
{
    // Make sure that input hides cursor before frames deletion
    input_.reset();
}

void
elBasicFuzzySearch::resize(term::Frame* parent)
{
    SIMC_ASSERT(parent != nullptr);
    SIMC_ASSERT(parent->getDimensions().row >= 5);

    // Allocate new buffer
    main_frame_ = parent;
    main_buffer_.reset(new term::Buffer(main_frame_));

    // Allocate new frames
    prompt_frame_.reset(
      new term::Frame(main_buffer_.get(),  //
                      {0, 0},              //
                      {1, main_buffer_->getDimensions().col}));
    input_frame_.reset(new term::Frame(prompt_frame_.get(),      //
                                       {0, max_id_length_ + 2},  //
                                       {0, 0},                   //
                                       true));
    entries_frame_.reset(new term::Frame(main_buffer_.get(),  //
                                         {1, 0},              //
                                         {1, 0},              //
                                         true));
    separator_frame_.reset(new term::Frame(
      main_buffer_.get(),                                         //
      term::point_t(entries_frame_->getDimensions().row + 1, 0),  //
      {0, 0},                                                     //
      true));

    // Resize input element
    input_->resize(input_frame_.get());

    // Resizing may affect scrolling
    adjustScrollStart();
}

void
elBasicFuzzySearch::draw()
{
    SIMC_ASSERT(main_frame_ != nullptr);

    // Get colors palette
    const config::Palette& colors = config_->getColors();

    // Draw prompt
    prompt_frame_->putCh({0, 0}, u'>');
    input_->draw();

    // Draw entries
    {
        // Request sorted indices
        const std::vector<size_t>& sorted = backend_->getSorted();
        nmatched_entries_                 = sorted.size();

        // Draw line by line
        for (int row = 0; row < entries_frame_->getDimensions().row
                          && row + scroll_start_entry_ < (int)nmatched_entries_;
             ++row)
        {
            int entry          = sorted[row + scroll_start_entry_];
            const auto& id     = us_ids_[entry];
            const auto& name   = us_names_[entry];
            const auto& margin = cs_margin_[entry];
            const FuzzySearch::matched_t& matched = backend_->getMatched(entry);
            bool selected = row + scroll_start_entry_ == selected_entry_;

            // Draw id
            entries_frame_->putStr(
              {row, max_id_length_ - (int)id.size()},
              id,
              term::TextProps{colors.unobtrusive, term::cDefault, term::aNone});

            // Draw margin
            entries_frame_->putStr(
              {row, entries_frame_->getDimensions().col - (int)margin.size()},
              margin);

            // Draw name
            int space_for_name = entries_frame_->getDimensions().col
                                 - max_id_length_ - max_margin_length_ - 3;
            int name_start = max_id_length_ + 2;
            term::CellString cs_name(name.size());
            for (size_t i = 0; i < name.size(); ++i)
                cs_name[i] = term::Cell(
                  name[i],
                  term::TextProps{
                    term::cDefault,
                    (selected ? colors.selection : term::cDefault),
                    (uint16_t)(matched[i] ? term::aBold : term::aNone)});
            cs_name = csShorten(cs_name, space_for_name);
            entries_frame_->putStr({row, name_start}, cs_name);
        }
    }

    // Draw separator
    drawSeparator(separator_frame_.get(), 0);

    main_buffer_->write();
    main_buffer_->clear();
}

bool
elBasicFuzzySearch::handleEvent(const Event& event)
{
    // Maybe we have got some input update
    if (input_->handleEvent(event)) {
        us_search_text_ = input_->usGetText();
        std::vector<std::u16string> new_search(usExtractWords(us_search_text_));

        // Find the first search that differs
        size_t differs = 0;
        while (differs < last_search_.size() && differs < new_search.size()
               && last_search_[differs] == new_search[differs])
            differs++;

        // Pop searches
        for (size_t i = differs; i < last_search_.size(); ++i)
            backend_->popSearch();

        // Push new searches
        for (size_t i = differs; i < new_search.size(); ++i)
            backend_->pushSearch(new_search[i]);

        // Update last_search_
        last_search_ = new_search;

        // Reset selection
        selected_entry_ = 0;
        adjustScrollStart();
        return true;
    }

    ////////////////
    // Key events //
    ////////////////

    if (event.type == EventType::keyPressed) {
        const term::Key& key             = std::any_cast<term::Key>(event.data);
        const config::Keybinds& keybinds = config_->getKeybinds();

        // Move selection up
        if (keybinds.searchEntryUp == key) {
            if (selected_entry_ == 0) return false;
            selected_entry_--;
            adjustScrollStart();
            return true;
        }

        // Move selection down
        if (keybinds.searchEntryDown == key) {
            if (selected_entry_ + 1 >= (int)nmatched_entries_) return false;
            selected_entry_++;
            adjustScrollStart();
            return true;
        }
    }

    return false;
}

std::string
elBasicFuzzySearch::getName() const
{
    return "elBasicFuzzySearch";
}

int
elBasicFuzzySearch::getSelected()
{
    // Maybe nothing to select
    if (nmatched_entries_ == 0) return -1;

    // Obtain sorted results
    const std::vector<size_t>& sorted = backend_->getSorted();
    return ids_[sorted[selected_entry_]];
}

void
elBasicFuzzySearch::adjustScrollStart()
{
    const int margins = 1;

    if (selected_entry_ < margins)
        scroll_start_entry_ = 0;
    else {
        SIMC_ASSERT(entries_frame_ != nullptr);
        int rows = entries_frame_->getDimensions().row;

        // Make sure that there's always a margin above the selected entry
        scroll_start_entry_
          = std::min(scroll_start_entry_, selected_entry_ - margins);

        // And a margin below
        scroll_start_entry_
          = std::max(scroll_start_entry_, selected_entry_ + margins + 1 - rows);
    }
}

int
fuzzySearchRecommendedHeight(int max_height)
{
    const int min_height = 5;
    float nice_exponent  = 0.75;
    int nice_height      = std::round(std::pow(max_height, nice_exponent)) + 5;
    return std::min(std::max(min_height, nice_height), max_height);
}

}  // namespace simc::ui

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UI_ELEMENTS_BASIC_FUZZY_SEARCH_HPP_
#define CLI_UI_ELEMENTS_BASIC_FUZZY_SEARCH_HPP_

#include <cli/term/cell_string.hpp>
#include <cli/ui/element.hpp>

namespace simc
{
class FuzzySearch;
}

namespace simc::ui
{
class elOneLineInput;
}

namespace simc::ui
{
class elBasicFuzzySearch : public Element
{
   public:
    struct ConstrArgs : BasicConstrArgs
    {
        std::vector<int> ids                 = std::vector<int>();
        std::vector<std::u16string> us_names = std::vector<std::u16string>();
        std::vector<term::CellString> cs_margin
          = std::vector<term::CellString>();
    };

   protected:
    std::shared_ptr<EventQueue> events_;
    std::shared_ptr<const config::Config> config_;
    std::shared_ptr<const std::locale> locale_;
    std::shared_ptr<Fetcher> fetcher_;

    // Fuzzy search backends
    std::unique_ptr<FuzzySearch> backend_;
    std::unique_ptr<elOneLineInput> input_;

    // Search info
    std::u16string us_search_text_;
    std::vector<std::u16string> last_search_;

    // Entries list
    size_t nentries_;
    size_t nmatched_entries_;
    std::vector<int> ids_;
    std::vector<std::u16string> us_ids_;
    std::vector<std::u16string> us_names_;
    std::vector<term::CellString> cs_margin_;
    int selected_entry_     = 0;
    int scroll_start_entry_ = 0;
    int max_id_length_      = 0;
    int max_margin_length_  = 0;

    // Frames
    std::unique_ptr<term::Buffer> main_buffer_;
    std::unique_ptr<term::Frame> prompt_frame_;
    std::shared_ptr<term::Frame> input_frame_;
    std::unique_ptr<term::Frame> entries_frame_;
    std::unique_ptr<term::Frame> separator_frame_;

   public:
    elBasicFuzzySearch(const ConstrArgs& args);
    ~elBasicFuzzySearch();

    void resize(term::Frame* parent);
    void draw();
    bool handleEvent(const Event& event);
    std::string getName() const;

    int getSelected();

   protected:
    void adjustScrollStart();
};

int fuzzySearchRecommendedHeight(int max_height);

}  // namespace simc::ui

#endif  // CLI_UI_ELEMENTS_BASIC_FUZZY_SEARCH_HPP_

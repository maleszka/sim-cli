// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/elements/contest_body.hpp>

#include <cli/term/buffer.hpp>
#include <cli/term/frame.hpp>
#include <cli/ui/draw.hpp>
#include <cli/ui/pages/round.hpp>
#include <cli/utils/string.hpp>
#include <fetcher/fetcher.hpp>
#include <fetcher/utils/macros.hpp>

namespace simc::ui
{

elContestBody::elContestBody(const ConstrArgs& args)
  : ScrollableElement(args.config)
  , events_(args.events)
  , locale_(args.locale)
  , fetcher_(args.fetcher)
{
    const auto& contest = fetcher_->getContest(args.contest_id);
    const auto& colors  = config_->getColors();

    // Generate rounds section
    {
        // Download data from the fetcher
        const auto& rounds  = contest->getChildren();
        const auto& submits = fetcher_->getSubmissions()->query(
          keywords::_only_final  = true,
          keywords::_contest_ids = std::vector<int>{contest->getId()});

        // Allocate section
        Section section;
        section.type = SectionType::rounds;
        section.title
          = term::CellString(u"Rounds",
                             term::TextProps{colors.secondaryAccent,
                                             term::cDefault,
                                             term::aNone})
            + term::CellString(u" (" + to_u16string(rounds.size()) + u")");
        section.entries.resize(rounds.size());

        // Assign rounds
        for (size_t entry_id = 0; entry_id < rounds.size(); ++entry_id) {
            RoundsEntry rentry;
            rentry.round = std::reinterpret_pointer_cast<FetcherDataRound>(
              rounds[rounds.size() - 1 - entry_id]);
            section.entries[entry_id] = rentry;
        }

        // Get max_id_length, and max_total_tasks_length
        int max_id_length           = 0;
        int max_total_tasks_length  = 0;
        int max_solved_tasks_length = 0;
        for (auto& entry : section.entries) {
            const auto& rentry = std::any_cast<RoundsEntry>(entry);
            max_id_length      = std::max(
              max_id_length, (int)to_u16string(rentry.round->getId()).size());
            max_total_tasks_length = std::max(
              max_total_tasks_length,
              (int)to_u16string(rentry.round->getTotalTasks()).size());
            max_solved_tasks_length = std::max(
              max_solved_tasks_length,
              (int)to_u16string(rentry.round->getTotalSolvedTasks()).size());
        }

        // Generate id, name, and right_margin strings
        int max_right_margin_length = 0;
        int max_schedule_length     = 0;
        for (auto& entry : section.entries) {
            RoundsEntry rentry = std::any_cast<RoundsEntry>(entry);

            // Generate id
            rentry.id = term::CellString(
              to_u16string(rentry.round->getId()),
              term::TextProps{colors.unobtrusive, term::cDefault, term::aNone});
            rentry.id = csPadLeft(rentry.id, max_id_length);

            // Generate name
            rentry.name = term::CellString(rentry.round->usGetName());

            // Generate schedule information
            {
                const auto& utc_start = rentry.round->getStartDate();
                const auto& utc_end   = rentry.round->getEndDate();
                rentry.schedule
                  = csTimeRange(utc_start, utc_end, colors.unobtrusive);
                const auto& active
                  = csTimeRangeActive(utc_start, utc_end, colors);
                if (active.empty())
                    rentry.schedule += term::CellString(u"       ");
                else
                    rentry.schedule += term::CellString(u" ") + active;
                max_schedule_length
                  = std::max(max_schedule_length, (int)rentry.schedule.size());
            }

            // Generate right_margin
            {
                const auto& solved
                  = csPadLeft(term::CellString(to_u16string(
                                rentry.round->getTotalSolvedTasks())),
                              max_solved_tasks_length);
                const auto& total = csPadLeft(
                  term::CellString(to_u16string(rentry.round->getTotalTasks())),
                  max_total_tasks_length);

                rentry.right_margin = solved + term::CellString(u" / ") + total;
                max_right_margin_length = std::max(
                  max_right_margin_length, (int)rentry.right_margin.size());
            }

            entry = rentry;
        }

        // Pad information on the right
        for (auto& entry : section.entries) {
            RoundsEntry rentry = std::any_cast<RoundsEntry>(entry);
            rentry.schedule = csPadLeft(rentry.schedule, max_schedule_length);
            rentry.right_margin
              = csPadLeft(rentry.right_margin, max_right_margin_length);
            entry = rentry;
        }

        sections_.emplace_back(std::move(section));
    }
}

elContestBody::~elContestBody() {}

bool
elContestBody::handleEvent(const Event& event)
{
    if (ScrollableElement::handleEvent(event)) return true;

    if (event.type == EventType::keyPressed) {
        const auto& key      = std::any_cast<term::Key>(event.data);
        const auto& keybinds = config_->getKeybinds();

        // Enter
        if (keybinds.navigationEnter == key) {
            if (selected_segment_ == -1) return false;
            const auto& scroll_id
              = std::any_cast<ScrollId>(segments_[selected_segment_]->id_);
            const auto& section = sections_[scroll_id.section];

            switch (section.type) {
                case SectionType::rounds: {
                    const auto& rentry = std::any_cast<RoundsEntry>(
                      section.entries[scroll_id.entry]);
                    actPageOpenData data;
                    data.name = "pgRound";
                    data.args = pgRound::ConstrArgs{
                      {events_, config_, locale_, fetcher_},
                      rentry.round->getId()};
                    events_->emplaceEvent(
                      EventType::actPageOpen, data, EventPriority::action);
                    return true;
                }
            }
            return false;
        }
    }

    return false;
}

void
elContestBody::allocateSegments()
{
    int width = main_buffer_->getDimensions().col;

    // Delete old segments
    segments_.clear();

    // Create segments section by section
    for (size_t section_id = 0; section_id < sections_.size(); ++section_id) {
        const auto& section = sections_[section_id];

        for (size_t entry_id = 0; entry_id < section.entries.size(); ++entry_id)
        {
            bool first        = entry_id == 0,
                 last         = entry_id + 1 == section.entries.size();
            const auto& entry = section.entries[entry_id];

            switch (section.type) {
                case SectionType::rounds: {
                    auto rentry = std::any_cast<RoundsEntry>(entry);

                    int height  = (first ? (last ? 4 : 3) : (last ? 2 : 1));
                    int scrollbox_begin = (first ? 0 : -1);
                    int scrollbox_end   = height + (last ? 0 : 1);

                    // allocate ScrollSegment
                    segments_.emplace_back(new ScrollSegment({height, width},
                                                             config_,
                                                             scrollbox_begin,
                                                             scrollbox_end));

                    // set selection
                    // TODO: what if name gets shortened?
                    int selection_start_col = 2 + rentry.id.size() + 1;
                    segments_.back()->setSelection(
                      {(first ? 2 : 0), selection_start_col},
                      rentry.name.size());

                    // assign id
                    segments_.back()->id_ = ScrollId{section_id, entry_id};
                    break;
                }
            }
        }
    }
}

void
elContestBody::renderSegment(int segment_id)
{
    const auto& segment   = segments_[segment_id];
    const auto& colors    = config_->getColors();
    const auto& scroll_id = std::any_cast<ScrollId>(segment->id_);
    const auto& section   = sections_[scroll_id.section];
    const auto& entry     = section.entries[scroll_id.entry];
    bool first            = scroll_id.entry == 0;
    int width             = segment->getDimensions().col;

    // Allocate final buffer
    std::unique_ptr<term::Buffer> buffer(
      new term::Buffer(segment->getDimensions()));

    // Render the segment
    switch (section.type) {
        case SectionType::rounds: {
            const auto& rentry = std::any_cast<RoundsEntry>(entry);

            // Write section separator
            if (first)
                drawSeparator(
                  buffer.get(), 0, section.title, colors.unobtrusive);

            // Write the entry
            {
                term::Frame frame(
                  buffer.get(), {(first ? 2 : 0), 0}, {1, width});

                // left margin
                {
                    term::TextProps margin_props;
                    if (rentry.round->getTotalSolvedTasks()
                          == rentry.round->getTotalTasks()
                        && rentry.round->getTotalTasks() > 0)
                        margin_props.fg = colors.statusOk;
                    frame.putCh({0, 0}, u'┃', margin_props);
                }

                // id
                frame.putStr({0, 2}, rentry.id);

                // name
                bool show_schedule = false;
                {
                    int space_for_name
                      = width               //
                        - 2                 // left margin
                        - rentry.id.size()  //
                        - 1                 // space between id and name
                        - 1  // space between name and right margin
                        - rentry.right_margin.size();
                    SIMC_ASSERT(space_for_name >= 0);

                    // Maybe we will also show the schedule
                    if (space_for_name - rentry.schedule.size() - 3 > 6) {
                        show_schedule = true;
                        space_for_name -= rentry.schedule.size() + 3;
                    }

                    term::CellString final_name
                      = csShorten(rentry.name, space_for_name);
                    frame.putStr({0, 2 + (int)rentry.id.size() + 1},
                                 final_name);
                }

                // schedule
                if (show_schedule) {
                    frame.putStr({0,
                                  width - (int)rentry.right_margin.size() - 3
                                    - (int)rentry.schedule.size()},
                                 rentry.schedule);
                }

                // right margin
                frame.putStr({0, width - (int)rentry.right_margin.size()},
                             rentry.right_margin);
            }
            break;
        }
    }

    // Pass the buffer to the segment
    segment->setBuffer(std::move(buffer));
}

std::string
elContestBody::getName() const
{
    return "elContestBody";
}

}  // namespace simc::ui

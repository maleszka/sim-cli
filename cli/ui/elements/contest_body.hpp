// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UI_CONTEST_BODY_HPP_
#define CLI_UI_CONTEST_BODY_HPP_

#include <cli/term/cell_string.hpp>
#include <cli/ui/scroll.hpp>

namespace simc::ui
{

const int minContestBodyHeight = 5;

class elContestBody : public ScrollableElement
{
   public:
    struct ConstrArgs : BasicConstrArgs
    {
        int contest_id;
    };

   protected:
    enum class SectionType
    {
        rounds,
    };

    struct RoundsEntry
    {
        std::shared_ptr<FetcherDataRound> round;
        term::CellString id;
        term::CellString name;
        term::CellString schedule;
        term::CellString right_margin;
    };

    struct Section
    {
        SectionType type;
        term::CellString title;
        std::vector<std::any> entries;
    };

    struct ScrollId
    {
        size_t section;
        size_t entry;
    };

    std::shared_ptr<EventQueue> events_;
    std::shared_ptr<const std::locale> locale_;
    std::shared_ptr<Fetcher> fetcher_;

    std::vector<Section> sections_;

   public:
    elContestBody(const ConstrArgs& args);
    ~elContestBody();

    bool handleEvent(const Event& event);
    std::string getName() const;

   protected:
    void allocateSegments();
    void renderSegment(int segment_id);
};

}  // namespace simc::ui

#endif  // CLI_UI_CONTEST_BODY_HPP_

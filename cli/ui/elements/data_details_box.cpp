// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/elements/data_details_box.hpp>

#include <cli/ui/draw.hpp>
#include <cli/utils/string.hpp>
#include <cli/utils/time.hpp>
#include <fetcher/fetcher.hpp>
#include <fetcher/utils/macros.hpp>

namespace simc::ui
{

elBasicDetailsBox::ConstrArgs
makeBasicArgs(const elDataDetailsBox::ConstrArgs& args)
{
    elBasicDetailsBox::ConstrArgs results
      = {{args.events, args.config, args.locale, args.fetcher}};

    const auto& colors = args.config->getColors();

    // Type specific attributes
    std::shared_ptr<FetcherData> object;
    std::vector<std::shared_ptr<Submission>> submits;
    switch (args.type) {
        case elDataDetailsBox::DetailsType::contest: {
            // Download data from the fetcher
            const auto& contest = args.fetcher->getContest(args.object_id);
            object              = contest;
            submits             = args.fetcher->getSubmissions()->query(
              keywords::_contest_ids = std::vector<int>{args.object_id});

            // Contest name
            {
                term::CellString name(contest->usGetName(),
                                      term::TextProps{colors.primaryAccent,
                                                      term::cDefault,
                                                      term::aNone});
                term::CellString id(
                  u"(" + to_u16string(contest->getId()) + u")",
                  term::TextProps{
                    colors.unobtrusive, term::cDefault, term::aNone});
                results.field_names.emplace_back(u"Contest:");
                results.field_values.emplace_back(name + term::CellString(u" ")
                                                  + id);
            }
            break;
        }
        case elDataDetailsBox::DetailsType::round: {
            // Download data from the fetcher
            const auto& round = args.fetcher->getRound(args.object_id);
            object            = round;
            submits           = args.fetcher->getSubmissions()->query(
              keywords::_round_ids = std::vector<int>{args.object_id});

            // Round name
            {
                term::CellString name(round->usGetName(),
                                      term::TextProps{colors.primaryAccent,
                                                      term::cDefault,
                                                      term::aNone});
                term::CellString id(u"(" + to_u16string(round->getId()) + u")",
                                    term::TextProps{colors.unobtrusive,
                                                    term::cDefault,
                                                    term::aNone});
                results.field_names.emplace_back(u"Round:");
                results.field_values.emplace_back(name + term::CellString(u" ")
                                                  + id);
            }

            // Time range (schedule)
            {
                const auto& start = round->getStartDate();
                const auto& end   = round->getEndDate();
                const auto& value = csTimeRange(start, end, term::cDefault)
                                    + csTimeRangeActive(start, end, colors);
                if (!value.empty()) {
                    results.field_names.emplace_back(u"Scheduled:");
                    results.field_values.emplace_back(value);
                }
            }

            // Results since
            {
                const auto& date = localFromUtc(round->getResultsDate());
                std::u16string us_date;
                if (date.is_neg_infinity()) {
                    us_date = u"immediately";
                } else {
                    if (date.is_special())
                        throw std::runtime_error(
                          "failed processing round details: bad date");
                    us_date = ptimeToString(date);
                }
                results.field_names.emplace_back(u"Results:");
                results.field_values.emplace_back(us_date);
            }

            // Ranking since
            {
                const auto& date = localFromUtc(round->getRankingDate());
                std::u16string us_date;
                if (date.is_neg_infinity()) {
                    us_date = u"immediately";
                } else {
                    if (date.is_special())
                        throw std::runtime_error(
                          "failed processing round details: bad date");
                    us_date = ptimeToString(date);
                }
                results.field_names.emplace_back(u"Ranking:");
                results.field_values.emplace_back(us_date);
            }
            break;
        }
        case elDataDetailsBox::DetailsType::task: {
            // Download data from the fetcher
            const auto& task = args.fetcher->getRoundTask(args.object_id);
            object           = task;
            submits          = args.fetcher->getSubmissions()->query(
              keywords::_round_task_ids = std::vector<int>{args.object_id});
            std::shared_ptr<Submission> final_submit;
            for (const auto& submit : submits)
                if (submit->getIsFinal()) final_submit = submit;
            if (final_submit == nullptr && !submits.empty())
                final_submit = submits[0];  // assume latest submission is final
                                            // if none are marked final

            // Round task name
            {
                term::CellString name(task->usGetName(),
                                      term::TextProps{colors.primaryAccent,
                                                      term::cDefault,
                                                      term::aNone});
                term::CellString id(u"(" + to_u16string(task->getId()) + u")",
                                    term::TextProps{colors.unobtrusive,
                                                    term::cDefault,
                                                    term::aNone});
                results.field_names.emplace_back(u"Task:");
                results.field_values.emplace_back(name + term::CellString(u" ")
                                                  + id);
            }

            // Final status
            if (final_submit != nullptr) {
                results.field_names.emplace_back(u"Status");
                results.field_values.emplace_back(
                  csShortStatus(colors, final_submit->getStatus())
                  + term::CellString(
                    u" ("
                    + (final_submit->getScore() >= 0
                         ? to_u16string(final_submit->getScore())
                         : u"-")
                    + u")"));
            }

            // Final rule
            {
                std::u16string rule_text
                  = (task->getFinalSubmitRule() == FinalSubmitRule::highestScore
                       ? u"highest score"
                       : u"latest compiling");
                results.field_names.emplace_back(u"Final rule:");
                results.field_values.emplace_back(rule_text);
            }
            break;
        }
    }

    // Attributes common for contest and round
    if (args.type == elDataDetailsBox::DetailsType::contest
        || args.type == elDataDetailsBox::DetailsType::round)
    {
        // Total problems
        results.field_names.emplace_back(u"Total problems:");
        results.field_values.emplace_back(
          to_u16string(object->getTotalTasks()));

        // Solved problems
        {
            term::CellString count(to_u16string(object->getTotalSolvedTasks()));
            term::CellString score(to_u16string(object->getTotalScore()),
                                   term::TextProps{colors.secondaryAccent,
                                                   term::cDefault,
                                                   term::aNone});
            results.field_names.emplace_back(u"Solved:");
            results.field_values.emplace_back(count + term::CellString(u" (")
                                              + score + term::CellString(u")"));
        }
    }

    // Last submission
    {
        results.field_names.emplace_back(u"Last submission:");
        if (submits.empty()) {
            results.field_values.emplace_back(
              u"none",
              term::TextProps{colors.unobtrusive, term::cDefault, term::aNone});
        } else {
            const auto& last_submit = submits.front();
            term::CellString date(
              ptimeToString(localFromUtc(last_submit->getDate())));
            term::CellString addition;
            addition
              = term::CellString(u" (")
                + term::CellString(
                  last_submit->getScore() >= 0
                    ? to_u16string(last_submit->getScore())
                    : u"scoring failed",
                  config::propsFromStatus(colors, last_submit->getStatus()))
                + term::CellString(u")");
            results.field_values.emplace_back(date + addition);
        }
    }

    // Colorize margin if everything is completed
    if (object->getTotalSolvedTasks() == object->getTotalTasks()
        && object->getTotalTasks() > 0)
    {
        results.margin_color
          = term::TextProps{colors.statusOk, term::cDefault, term::aBold};
    }

    return results;
}

elDataDetailsBox::elDataDetailsBox(const ConstrArgs& args)
  : elBasicDetailsBox(makeBasicArgs(args))
{
}

}  // namespace simc::ui

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/elements/data_fuzzy_search.hpp>

#include <cli/term/cell.hpp>
#include <cli/ui/draw.hpp>
#include <cli/utils/string.hpp>
#include <fetcher/fetcher.hpp>

namespace simc::ui
{

elBasicFuzzySearch::ConstrArgs
makeBasicFuzzySearchArgs(const elDataFuzzySearch::ConstrArgs& args)
{
    elBasicFuzzySearch::ConstrArgs results
      = {{args.events, args.config, args.locale, args.fetcher}};

    // Make sure that submissions have been downloaded
    std::shared_ptr<SubmissionsList> submissions
      = args.fetcher->getSubmissions();

    // Download fetcher data objects
    std::vector<std::shared_ptr<FetcherData>> objects;
    switch (args.type) {
        case elDataFuzzySearch::SearchType::contestsList:
            objects = args.fetcher->getRoot()->getChildren();
            break;
        case elDataFuzzySearch::SearchType::contestRoundsList:
            objects = args.fetcher->getContest(args.object_id)->getChildren();
            break;
        case elDataFuzzySearch::SearchType::roundTasksList:
            objects = args.fetcher->getRound(args.object_id)->getChildren();
            break;
        case elDataFuzzySearch::SearchType::contestTasksList: {
            const auto& rounds
              = args.fetcher->getContest(args.object_id)->getChildren();
            for (const auto& round : rounds) {
                const auto& tasks = round->getChildren();
                objects.insert(objects.end(), tasks.begin(), tasks.end());
            }
            break;
        }
    }

    // Get colors palette
    const config::Palette& colors = args.config->getColors();

    // Allocate data
    results.ids.resize(objects.size());
    results.us_names.resize(objects.size());
    results.cs_margin.resize(objects.size());

    // Save IDs
    for (size_t i = 0; i < objects.size(); ++i)
        results.ids[i] = objects[i]->getId();

    // Save names
    for (size_t i = 0; i < objects.size(); ++i)
        results.us_names[i] = objects[i]->usGetName();

    //////////////////
    // Right margin //
    //////////////////

    // Standard margin: <solved tasks> / <total tasks>

    if (args.type == elDataFuzzySearch::SearchType::contestsList
        || args.type == elDataFuzzySearch::SearchType::contestRoundsList)
    {
        // Obtain max_margin_length
        int max_margin_length = 0;
        for (auto obj : objects)
            max_margin_length
              = std::max(max_margin_length,
                         (int)to_u16string(obj->getTotalTasks()).size());

        // Save right margin
        for (size_t i = 0; i < objects.size(); ++i) {
            // Don't add margin for unloaded contests
            if (args.type == elDataFuzzySearch::SearchType::contestsList
                && objects[i]->getTotalTasks() == 0)
                continue;

            term::CellString left(
              to_u16string(objects[i]->getTotalSolvedTasks()));
            term::CellString mid(u" / ");
            term::CellString right(to_u16string(objects[i]->getTotalTasks()));
            right                = csPadLeft(right, max_margin_length);
            results.cs_margin[i] = left + mid + right;
        }
    }

    // Problems list: <status> (<score>)

    else if (args.type == elDataFuzzySearch::SearchType::roundTasksList
             || args.type == elDataFuzzySearch::SearchType::contestTasksList)
    {
        // Query submissions database for final submissions
        const auto& final_submits
          = (args.type == elDataFuzzySearch::SearchType::roundTasksList
               ? submissions->query(keywords::_round_ids
                                    = std::vector{args.object_id},
                                    keywords::_only_final = true)
               : submissions->query(keywords::_contest_ids
                                    = std::vector{args.object_id},
                                    keywords::_only_final = true));

        // Map round task ids to indices in results
        std::unordered_map<int, size_t> id_to_index;
        for (size_t index = 0; index < objects.size(); ++index)
            id_to_index[objects[index]->getId()] = index;

        // Browse final submissions and generate right margin
        for (const auto& submit : final_submits) {
            term::CellString status
              = csShortStatus(colors, submit->getStatus());
            term::CellString score = submit->getScore() >= 0
                                       ? to_u16string(submit->getScore())
                                       : u"-";
            score = term::CellString(u"(") + score + term::CellString(u")");
            score = csPadLeft(score, 6);

            auto& res
              = results.cs_margin[id_to_index[submit->getRoundTaskId()]];
            res = status + score;
        }

        // Try to set CF/JE status final-ish submissions where no final
        // submission was found
        for (const auto& [round_task_id, index] : id_to_index) {
            auto& res = results.cs_margin[index];
            if (res != term::CellString()) continue;

            const auto& cfje_submissions = submissions->query(
              keywords::_round_task_ids = std::vector{round_task_id},
              keywords::_statuses
              = std::vector{SubmissionStatus::compilationFailed,
                            SubmissionStatus::checkerError});
            if (cfje_submissions.empty()) continue;
            const auto& submit = cfje_submissions[0];

            term::CellString status
              = csShortStatus(colors, submit->getStatus());
            term::CellString score = submit->getScore() >= 0
                                       ? to_u16string(submit->getScore())
                                       : u"-";
            score = term::CellString(u"(") + score + term::CellString(u")");
            score = csPadLeft(score, 6);
            res   = status + score;
        }
    }

    return results;
}

elDataFuzzySearch::elDataFuzzySearch(const ConstrArgs& args)
  : elBasicFuzzySearch(makeBasicFuzzySearchArgs(args))
  , type_(args.type)
  , object_id_(args.object_id)
{
}

const elDataFuzzySearch::SearchType&
elDataFuzzySearch::getType() const
{
    return type_;
}

const int&
elDataFuzzySearch::getObjectId() const
{
    return object_id_;
}

std::string
elDataFuzzySearch::getName() const
{
    return "elDataFuzzySearch";
}

}  // namespace simc::ui

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UI_ELEMENTS_DATA_FUZZY_SEARCH_HPP_
#define CLI_UI_ELEMENTS_DATA_FUZZY_SEARCH_HPP_

#include <cli/ui/elements/basic_fuzzy_search.hpp>

namespace simc::ui
{

class elDataFuzzySearch : public elBasicFuzzySearch
{
   public:
    enum class SearchType
    {
        contestsList      = 0,
        contestRoundsList = 1,
        roundTasksList    = 2,
        contestTasksList,
    };

    struct ConstrArgs : BasicConstrArgs
    {
        SearchType type;
        int object_id;
    };

   protected:
    SearchType type_;
    int object_id_;

   public:
    elDataFuzzySearch(const ConstrArgs& args);

    const SearchType& getType() const;
    const int& getObjectId() const;

    std::string getName() const;
};

}  // namespace simc::ui

#endif  // CLI_UI_ELEMENTS_DATA_FUZZY_SEARCH_HPP_

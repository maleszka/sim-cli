// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/elements/nav_fuzzy_search.hpp>

#include <cli/ui/pages/contest.hpp>
#include <cli/ui/pages/round.hpp>
#include <cli/ui/pages/round_task.hpp>
#include <cli/ui/pages/statistics.hpp>
#include <fetcher/fetcher.hpp>
#include <fetcher/utils/macros.hpp>

namespace simc::ui
{

elNavFuzzySearch::elNavFuzzySearch(const ConstrArgs& args)
  : events_(args.events)
  , config_(args.config)
  , locale_(args.locale)
  , fetcher_(args.fetcher)
  , search_(new elDataFuzzySearch(args))
{
}

elNavFuzzySearch::~elNavFuzzySearch() {}

void
elNavFuzzySearch::resize(term::Frame* parent)
{
    main_frame_ = parent;
    search_->resize(main_frame_);
}

void
elNavFuzzySearch::draw()
{
    search_->draw();
}

bool
elNavFuzzySearch::handleEvent(const Event& event)
{
    // Maybe search_ can handle that event
    if (search_->handleEvent(event)) return true;

    // Handle keybinds
    if (event.type == EventType::keyPressed) {
        const term::Key& key             = std::any_cast<term::Key>(event.data);
        const config::Keybinds& keybinds = config_->getKeybinds();

        // Proceed with selection, but don't enter
        if (keybinds.searchEntrySelect == key) {
            int id = search_->getSelected();
            if (id == -1) return false;
            select(id);
            return true;
        }

        // Enter selected entry
        if (keybinds.navigationEnter == key) {
            int id = search_->getSelected();
            if (id == -1) return false;
            enter(id);
            return true;
        }

        // Go back in the hierarchy
        if (keybinds.navigationBack == key) {
            if (search_->getType()
                == elDataFuzzySearch::SearchType::contestsList)
                return false;
            goBack();
            return true;
        }
    }

    return false;
}

void
elNavFuzzySearch::select(int id)
{
    using stype = elDataFuzzySearch::SearchType;
    SIMC_ASSERT(id != -1);

    // Maybe we can't select and thus enter?
    stype curr_type = search_->getType();
    if (curr_type == stype::roundTasksList) {
        enter(id);
        return;
    }

    // Make a new search
    stype new_type = (stype)((int)curr_type + 1);
    elDataFuzzySearch::ConstrArgs args
      = {{events_, config_, locale_, fetcher_}, new_type, id};
    search_.reset(new elDataFuzzySearch(args));
    search_->resize(main_frame_);
}

void
elNavFuzzySearch::enter(int id)
{
    using stype       = elDataFuzzySearch::SearchType;
    stype search_type = search_->getType();
    SIMC_ASSERT(id != -1);

    // Create event data
    actPageOpenData data;
    BasicConstrArgs base_args{events_, config_, locale_, fetcher_};
    switch (search_type) {
        case stype::contestsList:
            data.name = "pgContest";
            data.args = pgContest::ConstrArgs{base_args, id};
            break;
        case stype::contestRoundsList:
            data.name = "pgRound";
            data.args = pgRound::ConstrArgs{base_args, id};
            break;
        case stype::roundTasksList:
            data.name = "pgRoundTask";
            data.args = pgRoundTask::ConstrArgs{base_args, id};
            break;
        default:
            return;
    }

    // Push event
    events_->emplaceEvent(EventType::actPageOpen, data, EventPriority::action);
}

void
elNavFuzzySearch::goBack()
{
    using stype     = elDataFuzzySearch::SearchType;
    stype curr_type = search_->getType();
    int curr_id     = search_->getObjectId();

    // Make sure that we are not in the root
    SIMC_ASSERT(curr_type != stype::contestsList);

    // Get parent id
    int parent_id = -1;
    switch (curr_type) {
        case stype::roundTasksList:
            parent_id = fetcher_->getRound(curr_id)->getParent()->getId();
            break;
        case stype::contestRoundsList:
            parent_id = fetcher_->getContest(curr_id)->getParent()->getId();
            break;
        default:
            break;
    }

    // Make a new search
    stype new_type = (stype)((int)curr_type - 1);
    elDataFuzzySearch::ConstrArgs args
      = {{events_, config_, locale_, fetcher_}, new_type, parent_id};
    search_.reset(new elDataFuzzySearch(args));
    search_->resize(main_frame_);
}

std::string
elNavFuzzySearch::getName() const
{
    return "elNavFuzzySearch";
}

}  // namespace simc::ui

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/elements/one_line_input.hpp>

#include <cli/term/buffer.hpp>
#include <cli/term/frame.hpp>
#include <cli/utils/string.hpp>
#include <fetcher/utils/macros.hpp>
#include <utf8.h>

namespace simc::ui
{

elOneLineInput::~elOneLineInput()
{
    if (main_frame_ != nullptr) main_frame_->hideCursor();
}

void
elOneLineInput::resize(term::Frame* parent)
{
    main_frame_ = parent;
    main_buffer_.reset(new term::Buffer(main_frame_));
    adjustScrollStart();
}

void
elOneLineInput::draw()
{
    // Output the text
    for (int col = 0; col < main_buffer_->getDimensions().col; ++col) {
        int text_col = col + hscroll_start_;
        if (text_col >= (int)text_.size()) break;
        main_buffer_->putCh({0, col}, text_[text_col]);
    }

    // Set cursor
    main_frame_->setCursor({0, cursor_ - hscroll_start_});

    // Write buffers
    main_buffer_->write();
    main_buffer_->clear();
}

bool
elOneLineInput::handleEvent(const Event& event)
{
    if (event.type != EventType::keyPressed) return false;
    term::Key key = std::any_cast<term::Key>(event.data);

    // Remove one char before the cursor
    if (key.nsText == "<Backspace>") {
        if (cursor_ == 0) return false;  // nothing to do
        text_.erase(cursor_ - 1, 1);
        cursor_--;
        adjustScrollStart();
        return true;
    }

    // Remove one char under the cursor
    if (key.nsText == "<Delete>") {
        if (cursor_ == (int)text_.size()) return false;  // nothing to do
        text_.erase(cursor_, 1);
        return true;
    }

    // Go cursor left one character
    if (key.nsText == "<Left>") {
        if (cursor_ == 0) return false;  // can't go
        cursor_--;
        adjustScrollStart();
        return true;
    }

    // Go cursor right one character
    if (key.nsText == "<Right>") {
        if (cursor_ == (int)text_.size()) return false;  // can't go
        cursor_++;
        adjustScrollStart();
        return true;
    }

    // Go cursor left one word
    if (key.nsText == "<C-Left>") {
        if (cursor_ == 0) return false;  // nothing to do
        while (cursor_ > 0 && isspace(text_[cursor_ - 1])) cursor_--;
        while (cursor_ > 0 && !isspace(text_[cursor_ - 1])) cursor_--;
        adjustScrollStart();
        return true;
    }

    // Go cursor right one word
    if (key.nsText == "<C-Right>") {
        if (cursor_ == (int)text_.size()) return false;  // nothing to do
        while (cursor_ < (int)text_.size() && isspace(text_[cursor_]))
            cursor_++;
        while (cursor_ < (int)text_.size() && !isspace(text_[cursor_]))
            cursor_++;
        adjustScrollStart();
        return true;
    }

    // Insert text
    if (key.single) {
        std::u32string ucs4_text = utf8::utf8to32(key.nsText);

        // Requirements:
        // 1. it must fit in char16_t
        // 2. it must be one character
        if (ucs4_text.size() != 1
            || ucs4_text[0] > std::numeric_limits<char16_t>::max())
            return false;

        text_.insert(cursor_, 1, (char16_t)ucs4_text[0]);
        cursor_++;
        adjustScrollStart();
        return true;
    }
    return false;
}

std::string
elOneLineInput::getName() const
{
    return "elOneLineInput";
}

const std::u16string&
elOneLineInput::usGetText() const
{
    return text_;
}

void
elOneLineInput::adjustScrollStart()
{
    SIMC_ASSERT(main_frame_ != nullptr);
    int cols          = main_frame_->getDimensions().col;
    const int margins = 1;

    if (cursor_ < margins)
        hscroll_start_ = 0;
    else {
        // Make sure that there's always one character margin before the cursor
        hscroll_start_ = std::min(hscroll_start_, cursor_ - margins);

        // And a margin below
        hscroll_start_ = std::max(hscroll_start_, cursor_ + margins + 1 - cols);
    }
}

}  // namespace simc::ui

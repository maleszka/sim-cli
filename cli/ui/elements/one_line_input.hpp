// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UI_ELEMENTS_ONE_LINE_INPUT_HPP_
#define CLI_UI_ELEMENTS_ONE_LINE_INPUT_HPP_

#include <cli/ui/element.hpp>

namespace simc::ui
{

class elOneLineInput : public Element
{
   protected:
    std::u16string text_ = u"";
    int cursor_          = 0;
    int hscroll_start_   = 0;
    std::unique_ptr<term::Buffer> main_buffer_;

   public:
    ~elOneLineInput();

    void resize(term::Frame* parent);
    void draw();
    bool handleEvent(const Event& event);
    std::string getName() const;

    const std::u16string& usGetText() const;

   protected:
    void adjustScrollStart();
};

}  // namespace simc::ui

#endif  // CLI_UI_ELEMENTS_ONE_LINE_INPUT_HPP_

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/elements/tasks_list.hpp>

#include <cli/term/buffer.hpp>
#include <cli/ui/draw.hpp>
#include <cli/ui/pages/round_task.hpp>
#include <cli/utils/string.hpp>
#include <cli/utils/time.hpp>
#include <fetcher/fetcher.hpp>
#include <fetcher/utils/macros.hpp>

namespace simc::ui
{

elTasksList::elTasksList(const ConstrArgs& args)
  : ScrollableElement(args.config)
  , events_(args.events)
  , locale_(args.locale)
  , fetcher_(args.fetcher)
  , sections_(3)
{
    // Download round tasks and submissions from the fetcher
    std::vector<std::shared_ptr<FetcherDataRoundTask>> round_tasks;
    std::vector<std::shared_ptr<Submission>> submits;
    {
        std::unordered_map<size_t, size_t> id_to_index;
        auto addRound =
          [&round_tasks, &id_to_index](const std::shared_ptr<FetcherData> round)
        {
            const auto& children = round->getChildren();
            round_tasks.reserve(round_tasks.size() + children.size());
            for (const auto& child : children) {
                id_to_index[child->getId()] = round_tasks.size();
                round_tasks.emplace_back(
                  std::reinterpret_pointer_cast<FetcherDataRoundTask>(child));
            }
        };

        std::vector<std::shared_ptr<Submission>> all_submits;
        if (args.contest_id == -1 && args.round_id != -1) {
            // Download round tasks
            addRound(fetcher_->getRound(args.round_id));

            // Download submissions
            const auto& submits_list = fetcher_->getSubmissions();
            all_submits              = submits_list->query(keywords::_round_ids
                                              = std::vector<int>{args.round_id},
                                              keywords::_only_final = true);
        } else if (args.contest_id != -1 && args.round_id == -1) {
            // Download round tasks
            const auto& contest  = fetcher_->getContest(args.contest_id);
            const auto& children = contest->getChildren();
            for (const auto& round : children) addRound(round);

            // Download submissions
            const auto& submits_list = fetcher_->getSubmissions();
            all_submits              = submits_list->query(
              keywords::_contest_ids = std::vector<int>{args.contest_id},
              keywords::_only_final  = true);
        }

        // Write save all_submits to submits
        submits.resize(round_tasks.size());
        for (const auto& submit : all_submits)
            submits[id_to_index[submit->getRoundTaskId()]] = submit;
    }

    // Create entries
    const auto& colors = config_->getColors();
    {
        bool show_category = args.round_id == -1;
        auto& in_progress  = sections_[0].entries;
        auto& todo         = sections_[1].entries;
        auto& done         = sections_[2].entries;
        for (size_t i = 0; i < round_tasks.size(); ++i) {
            const auto& task = round_tasks[i];

            std::shared_ptr<Submission>
              submit;  // explicitly declared non-reference
                       // type due to delayed declaration
            // First check if there are no final submissions without an empty
            // list. This happens if and only if the only submission statuses
            // are CF and JE.
            if (submits[i] == nullptr) {
                const auto cfje_submits = fetcher_->getSubmissions()->query(
                  keywords::_package_ids
                  = std::vector<int>{task->getProblemId()},
                  keywords::_statuses
                  = std::vector{SubmissionStatus::compilationFailed,
                                SubmissionStatus::checkerError});
                if (!cfje_submits.empty()) {
                    submit = cfje_submits[0];
                }
            } else {
                submit = submits[i];
            }
            Entry entry;
            entry.submit = submit;
            entry.task   = task;

            // id
            entry.id       = to_u16string(task->getId());
            max_id_length_ = std::max(max_id_length_, (int)entry.id.size());

            // name
            entry.name = task->usGetName();
            if (show_category)
                entry.category
                  = u" ("
                    + usToLowercase(task->getParent()->usGetName(), *locale_)
                    + u")";

            // category-depended
            if (submit == nullptr) {  // to be done
                todo.emplace_back(std::move(entry));
            } else if (submit->getScore() == 100
                       && submit->getStatus() == SubmissionStatus::ok)
            {  // done
                // right margin
                entry.right_margin = ptimeToString(submit->getDate());
                max_margin_length_ = std::max(max_margin_length_,
                                              (int)entry.right_margin.size());

                // Add it
                done.emplace_back(std::move(entry));
            } else {  // in progress
                // right margin
                entry.right_margin
                  = term::CellString(ptimeToString(submit->getDate()))
                    + term::CellString(u"  ")
                    + csShortStatus(colors, submit->getStatus())
                    + csPadLeft(
                      term::CellString(u"(")
                        + term::CellString(submit->getScore() >= 0
                                             ? to_u16string(submit->getScore())
                                             : u"-")
                        + term::CellString(u")"),
                      6);
                max_margin_length_ = std::max(max_margin_length_,
                                              (int)entry.right_margin.size());

                // Add it
                in_progress.emplace_back(std::move(entry));
            }
        }
    }

    // Create section titles
    {
        term::TextProps title_props{
          colors.secondaryAccent, term::cDefault, term::aNone};
        sections_[0].title = term::CellString(u"In progress", title_props);
        sections_[1].title = term::CellString(u"To be done", title_props);
        sections_[2].title = term::CellString(u"Done", title_props);
        for (auto& section : sections_) {
            section.title += term::CellString(
              u" (" + to_u16string(section.entries.size()) + u")");
        }
    }

    // Sort `In progress` and `Done` by last submission date
    auto date_cmp = [](const Entry& a, const Entry& b)
    { return a.submit->getDate() > b.submit->getDate(); };
    std::sort(
      sections_[0].entries.begin(), sections_[0].entries.end(), date_cmp);
    std::sort(
      sections_[2].entries.begin(), sections_[2].entries.end(), date_cmp);
}

elTasksList::~elTasksList() {}

bool
elTasksList::handleEvent(const Event& event)
{
    if (ScrollableElement::handleEvent(event)) return true;

    if (event.type == EventType::keyPressed) {
        const auto& key      = std::any_cast<term::Key>(event.data);
        const auto& keybinds = config_->getKeybinds();

        // Enter
        if (keybinds.navigationEnter == key) {
            if (selected_segment_ == -1) return false;
            actPageOpenData data;
            data.name = "pgRoundTask";
            data.args = pgRoundTask::ConstrArgs{
              {events_, config_, locale_, fetcher_},
              std::any_cast<ScrollId>(segments_[selected_segment_]->id_)
                .round_task_id};
            events_->emplaceEvent(
              EventType::actPageOpen, data, EventPriority::action);
            return true;
        }
    }

    return false;
}

void
elTasksList::allocateSegments()
{
    int width               = main_buffer_->getDimensions().col;
    int selection_start_col = 2 + max_id_length_ + 1;

    // Delete old segments
    segments_.clear();

    // Create segments section by section
    for (size_t section_id = 0; section_id < sections_.size(); ++section_id) {
        const auto& section = sections_[section_id];
        if (section.entries.empty()) continue;  // don't display empty sections

        // Create ScrollSegment out of each entry
        for (size_t entry_id = 0; entry_id < section.entries.size(); ++entry_id)
        {
            const Entry& entry  = section.entries[entry_id];
            bool first          = entry_id == 0,
                 last           = entry_id + 1 == section.entries.size();

            int height          = (first ? (last ? 4 : 3) : (last ? 2 : 1));
            int scrollbox_begin = (first ? 0 : -1);
            int scrollbox_end   = height + (last ? 0 : 1);

            // allocate ScrollSegment
            segments_.emplace_back(new ScrollSegment(
              {height, width}, config_, scrollbox_begin, scrollbox_end));

            // set selection
            // TODO: what if name gets shortened?
            segments_.back()->setSelection(
              {(first ? 2 : 0), selection_start_col}, entry.name.size());

            // assign id
            segments_.back()->id_
              = ScrollId{section_id, entry_id, entry.task->getId()};
        }
    }
}

void
elTasksList::renderSegment(int segment_id)
{
    auto const& segment   = segments_[segment_id];
    auto const& colors    = config_->getColors();
    auto const& scroll_id = std::any_cast<ScrollId>(segment->id_);
    auto const& section   = sections_[scroll_id.section];
    auto const& entry     = section.entries[scroll_id.entry];
    bool first            = scroll_id.entry == 0;
    int width             = segment->getDimensions().col;

    // Allocate final buffer
    std::unique_ptr<term::Buffer> buffer(
      new term::Buffer(segment->getDimensions()));

    // Write section separator
    if (first)
        drawSeparator(buffer.get(), 0, section.title, colors.unobtrusive);

    // Write the entry
    {
        term::Frame frame(buffer.get(), {(first ? 2 : 0), 0}, {1, width});

        // left margin
        {
            term::TextProps margin_props;
            if (entry.submit != nullptr)
                margin_props
                  = config::propsFromStatus(colors, entry.submit->getStatus());
            frame.putCh({0, 0}, u'┃', margin_props);
        }

        // id
        frame.putStr(
          {0, 2 + max_id_length_ - (int)entry.id.size()},
          entry.id,
          term::TextProps{colors.unobtrusive, term::cDefault, term::aNone});

        // name
        {
            int space_for_name = width             //
                                 - 2               // left margin
                                 - max_id_length_  //
                                 - 1               // space between id and name
                                 - 1  // space between name and right margin
                                 - max_margin_length_;
            SIMC_ASSERT(space_for_name >= 0);


            // format: <name> (<round_name>)
            term::CellString final_name(entry.name);
            if (entry.name.size() + entry.category.size()
                <= (size_t)space_for_name)
            {
                final_name += term::CellString(
                  entry.category,
                  term::TextProps{
                    colors.unobtrusive, term::cDefault, term::aNone});
            } else {
                final_name = csShorten(final_name, space_for_name);
            }
            frame.putStr({0, 2 + max_id_length_ + 1}, final_name);
        }

        // right margin
        frame.putStr({0, width - (int)entry.right_margin.size()},
                     entry.right_margin);
    }

    // Pass the buffer to the segment
    segment->setBuffer(std::move(buffer));
}

std::string
elTasksList::getName() const
{
    return "elTasksList";
}

}  // namespace simc::ui

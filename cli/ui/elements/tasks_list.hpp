// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UI_ELEMENTS_TASKS_LIST_HPP_
#define CLI_UI_ELEMENTS_TASKS_LIST_HPP_

#include <cli/term/cell_string.hpp>
#include <cli/ui/scroll.hpp>

namespace simc::ui
{

const int minTasksListHeight = 4;

class elTasksList : public ScrollableElement
{
   public:
    struct ConstrArgs : BasicConstrArgs
    {
        int contest_id = -1;
        int round_id   = -1;
    };

   protected:
    struct Entry
    {
        std::shared_ptr<Submission> submit;
        std::shared_ptr<FetcherDataRoundTask> task;
        std::u16string id;
        std::u16string name;
        std::u16string category;
        term::CellString right_margin;
    };

    struct Section
    {
        term::CellString title;
        std::vector<Entry> entries;
    };

    struct ScrollId
    {
        size_t section;
        size_t entry;
        int round_task_id;
    };

    std::shared_ptr<EventQueue> events_;
    std::shared_ptr<const std::locale> locale_;
    std::shared_ptr<Fetcher> fetcher_;

    std::vector<Section> sections_;
    int max_id_length_     = 0;
    int max_margin_length_ = 0;

   public:
    elTasksList(const ConstrArgs& args);
    ~elTasksList();

    bool handleEvent(const Event& event);
    std::string getName() const;

   protected:
    void allocateSegments();
    void renderSegment(int segment_id);
};

}  // namespace simc::ui

#endif  // CLI_UI_ELEMENTS_TASKS_LIST_HPP_

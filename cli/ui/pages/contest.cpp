// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/pages/contest.hpp>

#include <cli/term/buffer.hpp>
#include <cli/term/frame.hpp>
#include <cli/ui/draw.hpp>
#include <cli/ui/elements/contest_body.hpp>
#include <cli/ui/elements/data_details_box.hpp>
#include <cli/ui/elements/nav_fuzzy_search.hpp>
#include <cli/ui/pages/contest_tasks.hpp>
#include <cli/ui/pages/root.hpp>
#include <fetcher/fetcher.hpp>
#include <fetcher/utils/macros.hpp>

namespace simc::ui
{

pgContest::pgContest(const ConstrArgs& args)
  : events_(args.events)
  , config_(args.config)
  , locale_(args.locale)
  , fetcher_(args.fetcher)
  , contest_(fetcher_->getContest(args.contest_id))
{
    // Get name (for title)
    contest_name_ = contest_->usGetName();

    // Initialize details box
    details_.reset(new elDataDetailsBox(
      {{args.events, args.config, args.locale, args.fetcher},
       elDataDetailsBox::DetailsType::contest,
       contest_->getId()}));

    // Initialize body element
    body_.reset(
      new elContestBody({{args.events, args.config, args.locale, args.fetcher},
                         contest_->getId()}));
}

pgContest::~pgContest() {}

void
pgContest::resize(term::Frame* parent)
{
    // Main
    main_frame_ = parent;
    main_buffer_.reset(new term::Buffer(main_frame_));

    // Title
    title_frame_.reset(new term::Frame(
      main_buffer_.get(), {0, 0}, {1, main_buffer_->getDimensions().col}));
    title_buffer_.reset(new term::Buffer(title_frame_.get()));
    drawTitle(title_buffer_.get(), contest_name_);

    // For small windows, show only body, without details
    int space_for_body
      = main_buffer_->getDimensions().row - 2 - details_->getHeight() - 1;
    bool no_details = space_for_body < minContestBodyHeight;

    // Details
    if (no_details) {
        details_frame_.reset();
    } else {
        details_frame_.reset(new term::Frame(
          main_buffer_.get(),
          {2, 0},
          {details_->getHeight(), main_buffer_->getDimensions().col}));
        details_->resize(details_frame_.get());
    }

    // Body
    if (no_details) {
        body_frame_.reset(
          new term::Frame(main_buffer_.get(), {2, 0}, {0, 0}, true));
    } else {
        body_frame_.reset(
          new term::Frame(main_buffer_.get(),
                          {2 + details_frame_->getDimensions().row + 1, 0},
                          {0, 0},
                          true));
    }
    body_->resize(body_frame_.get());

    // Fuzzy search
    search_frame_.reset(new term::Frame(
      main_buffer_.get(),
      {1, 0},
      {fuzzySearchRecommendedHeight(main_buffer_->getDimensions().row - 1),
       main_buffer_->getDimensions().col}));
    if (search_ != nullptr) search_->resize(search_frame_.get());
}

void
pgContest::draw()
{
    title_buffer_->write(true);
    if (details_frame_ != nullptr) details_->draw();
    body_->draw();
    if (search_ != nullptr) search_->draw();
    main_buffer_->write();
    main_buffer_->clear();
}

bool
pgContest::handleEvent(const Event& event)
{
    // Maybe that event can be handled by the fuzzy search
    if (search_ != nullptr && search_->handleEvent(event)) return true;

    // Maybe that event can be handled by the body
    if (search_ == nullptr && body_->handleEvent(event)) return true;

    // Handle keyPressed event
    if (event.type == EventType::keyPressed) {
        const term::Key& key             = std::any_cast<term::Key>(event.data);
        const config::Keybinds& keybinds = config_->getKeybinds();

        // Open fuzzy search
        if (keybinds.searchOpen == key) {
            if (search_ != nullptr) return false;  // can't re-open
            search_.reset(new elNavFuzzySearch(
              {{{events_, config_, locale_, fetcher_},
                elDataFuzzySearch::SearchType::contestRoundsList,
                contest_->getId()}}));
            if (search_frame_ != nullptr) search_->resize(search_frame_.get());
            return true;
        }

        // Close fuzzy search
        if (keybinds.searchExit == key) {
            if (search_ == nullptr) return false;
            search_.reset();
            return true;
        }

        // Open tasks page
        if (keybinds.tasksToggle == key) {
            actPageOpenData data;
            data.name = "pgContestTasks";
            data.args = pgContestTasks::ConstrArgs{
              {events_, config_, locale_, fetcher_}, contest_->getId()};
            events_->emplaceEvent(
              EventType::actPageOpen, data, EventPriority::action);
            return true;
        }

        // Go back in the hierarchy
        if (keybinds.navigationBack == key) {
            actPageOpenData data;
            data.name = "pgRoot";
            data.args
              = pgRoot::ConstrArgs{{events_, config_, locale_, fetcher_}};
            events_->emplaceEvent(
              EventType::actPageOpen, data, EventPriority::action);
        }
    }

    return false;
}

std::string
pgContest::getName() const
{
    return "pgContest";
}

}  // namespace simc::ui

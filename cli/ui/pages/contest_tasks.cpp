// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/pages/contest_tasks.hpp>

#include <cli/term/buffer.hpp>
#include <cli/term/frame.hpp>
#include <cli/ui/draw.hpp>
#include <cli/ui/elements/data_fuzzy_search.hpp>
#include <cli/ui/elements/tasks_list.hpp>
#include <cli/ui/pages/contest.hpp>
#include <cli/ui/pages/round_task.hpp>
#include <fetcher/fetcher.hpp>

namespace simc::ui
{

pgContestTasks::pgContestTasks(const ConstrArgs& args)
  : events_(args.events)
  , config_(args.config)
  , locale_(args.locale)
  , fetcher_(args.fetcher)
  , contest_(fetcher_->getContest(args.contest_id))
{
    // Get name (for title)
    contest_name_ = contest_->usGetName();

    // Download tasks list
    tasks_.reset(new elTasksList(elTasksList::ConstrArgs{
      {args.events, args.config, args.locale, args.fetcher}, args.contest_id}));
}

pgContestTasks::~pgContestTasks() {}

void
pgContestTasks::resize(term::Frame* parent)
{
    // Main
    main_frame_ = parent;
    main_buffer_.reset(new term::Buffer(main_frame_));

    // Title
    title_frame_.reset(new term::Frame(
      main_buffer_.get(), {0, 0}, {1, main_buffer_->getDimensions().col}));
    title_buffer_.reset(new term::Buffer(title_frame_.get()));
    drawTitle(title_buffer_.get(),
              term::CellString(u"Problems: ") + contest_name_);

    // Tasks
    tasks_frame_.reset(
      new term::Frame(main_buffer_.get(), {2, 0}, {0, 0}, true));
    tasks_->resize(tasks_frame_.get());

    // Fuzzy search
    search_frame_.reset(new term::Frame(
      main_buffer_.get(),
      {1, 0},
      {fuzzySearchRecommendedHeight(main_buffer_->getDimensions().row - 1),
       main_buffer_->getDimensions().col}));
    if (search_ != nullptr) search_->resize(search_frame_.get());
}

void
pgContestTasks::draw()
{
    title_buffer_->write(true);
    tasks_->draw();
    if (search_ != nullptr) search_->draw();
    main_buffer_->write();
    main_buffer_->clear();
}

bool
pgContestTasks::handleEvent(const Event& event)
{
    // Maybe that event can be handled by the fuzzy search
    if (search_ != nullptr && search_->handleEvent(event)) return true;

    // Maybe that event can be handled by the tasks list
    if (search_ == nullptr && tasks_->handleEvent(event)) return true;

    // Handle keyPressed event
    if (event.type == EventType::keyPressed) {
        const term::Key& key             = std::any_cast<term::Key>(event.data);
        const config::Keybinds& keybinds = config_->getKeybinds();

        // Open fuzzy search
        if (keybinds.searchOpen == key) {
            if (search_ != nullptr) return false;  // can't re-open
            search_.reset(new elDataFuzzySearch(
              {{{events_, config_, locale_, fetcher_},
                elDataFuzzySearch::SearchType::contestTasksList,
                contest_->getId()}}));
            if (search_frame_ != nullptr) search_->resize(search_frame_.get());
            return true;
        }

        // Fuzzy search: enter selected task
        if (search_ != nullptr
            && (keybinds.navigationEnter == key
                || keybinds.searchEntrySelect == key))
        {
            actPageOpenData data;
            data.name = "pgRoundTask";
            data.args = pgRoundTask::ConstrArgs{
              {events_, config_, locale_, fetcher_}, search_->getSelected()};
            events_->emplaceEvent(
              EventType::actPageOpen, data, EventPriority::action);
            return true;
        }

        // Close fuzzy search
        if (search_ != nullptr
            && (keybinds.searchExit == key || keybinds.navigationBack == key))
        {
            search_.reset();
            return true;
        }

        // Go back to the contest page
        if ((keybinds.navigationBack == key && search_ == nullptr)
            || keybinds.navigationExit == key || keybinds.tasksToggle == key)
        {
            actPageOpenData data;
            data.name = "pgContest";
            data.args = pgContest::ConstrArgs{
              {events_, config_, locale_, fetcher_}, contest_->getId()};
            events_->emplaceEvent(
              EventType::actPageOpen, data, EventPriority::action);
            return true;
        }
    }

    return false;
}

std::string
pgContestTasks::getName() const
{
    return "pgContestTasks";
}

}  // namespace simc::ui

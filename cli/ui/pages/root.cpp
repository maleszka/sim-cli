// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/pages/root.hpp>

#include <cli/term/buffer.hpp>
#include <cli/term/frame.hpp>
#include <cli/ui/elements/nav_fuzzy_search.hpp>
#include <cli/ui/pages/statistics.hpp>
#include <fetcher/utils/macros.hpp>

namespace simc::ui
{

pgRoot::pgRoot(const ConstrArgs& args)
  : events_(args.events)
  , config_(args.config)
  , locale_(args.locale)
  , fetcher_(args.fetcher)
{
    elNavFuzzySearch::ConstrArgs search_args
      = {{{events_, config_, locale_, fetcher_},
          elDataFuzzySearch::SearchType::contestsList,
          -1}};
    search_.reset(new elNavFuzzySearch(search_args));
}

pgRoot::~pgRoot() {}

void
pgRoot::resize(term::Frame* parent)
{
    main_frame_ = parent;
    main_buffer_.reset(new term::Buffer(main_frame_));

    // Reallocate search_frame_
    search_frame_.reset(new term::Frame(
      main_buffer_.get(),
      {0, 0},
      {fuzzySearchRecommendedHeight(main_buffer_->getDimensions().row),
       main_buffer_->getDimensions().col}));
    search_->resize(search_frame_.get());
}

void
pgRoot::draw()
{
    search_->draw();
    main_buffer_->write();
    main_buffer_->clear();
}

bool
pgRoot::handleEvent(const Event& event)
{
    // Maybe this event can be handled by fuzzy search?
    if (search_->handleEvent(event)) return true;

    if (event.type == EventType::keyPressed) {
        const config::Keybinds& keybinds = config_->getKeybinds();
        const term::Key& key             = std::any_cast<term::Key>(event.data);

        // Close fuzzy search
        if (keybinds.searchExit == key) {
            pgStatistics::ConstrArgs args
              = {{events_, config_, locale_, fetcher_}};
            actPageOpenData data = {"pgStatistics", args};
            events_->emplaceEvent(
              EventType::actPageOpen, data, EventPriority::action);
            return true;
        }
    }

    return false;
}

std::string
pgRoot::getName() const
{
    return "pgRoot";
}

}  // namespace simc::ui

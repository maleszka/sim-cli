// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UI_PAGES_ROOT_HPP_
#define CLI_UI_PAGES_ROOT_HPP_

#include <cli/ui/element.hpp>

namespace simc::ui
{
class elNavFuzzySearch;
}

namespace simc::ui
{

class pgRoot : public Element
{
   public:
    struct ConstrArgs : BasicConstrArgs
    {
    };

   protected:
    std::shared_ptr<EventQueue> events_;
    std::shared_ptr<const config::Config> config_;
    std::shared_ptr<const std::locale> locale_;
    std::shared_ptr<Fetcher> fetcher_;

    std::unique_ptr<term::Buffer> main_buffer_;
    std::shared_ptr<term::Frame> search_frame_;
    std::unique_ptr<elNavFuzzySearch> search_;

   public:
    pgRoot(const ConstrArgs& args);
    ~pgRoot();

    void resize(term::Frame* parent);
    void draw();
    bool handleEvent(const Event& event);
    std::string getName() const;

   protected:
    inline static PageFactoryMaker<pgRoot> factory_{"pgRoot"};
};

}  // namespace simc::ui

#endif  // CLI_UI_PAGES_ROOT_HPP_

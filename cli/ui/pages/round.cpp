// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/pages/round.hpp>

#include <cli/term/buffer.hpp>
#include <cli/term/frame.hpp>
#include <cli/ui/draw.hpp>
#include <cli/ui/elements/data_details_box.hpp>
#include <cli/ui/elements/nav_fuzzy_search.hpp>
#include <cli/ui/elements/tasks_list.hpp>
#include <cli/ui/pages/contest.hpp>
#include <fetcher/fetcher.hpp>
#include <fetcher/utils/macros.hpp>

namespace simc::ui
{

pgRound::pgRound(const ConstrArgs& args)
  : events_(args.events)
  , config_(args.config)
  , locale_(args.locale)
  , fetcher_(args.fetcher)
  , round_(fetcher_->getRound(args.round_id))
{
    // Get contest name (for the title)
    contest_name_ = round_->getParent()->usGetName();

    // Initialize details box
    details_.reset(new elDataDetailsBox(
      {{args.events, args.config, args.locale, args.fetcher},
       elDataDetailsBox::DetailsType::round,
       round_->getId()}));

    // Initialize tasks list
    tasks_.reset(
      new elTasksList({{args.events, args.config, args.locale, args.fetcher},
                       -1,
                       round_->getId()}));
}

pgRound::~pgRound() {}

void
pgRound::resize(term::Frame* parent)
{
    // Main
    main_frame_ = parent;
    main_buffer_.reset(new term::Buffer(main_frame_));

    // Title
    title_frame_.reset(new term::Frame(
      main_buffer_.get(), {0, 0}, {1, main_buffer_->getDimensions().col}));
    title_buffer_.reset(new term::Buffer(title_frame_.get()));
    drawTitle(title_buffer_.get(), contest_name_);

    // For small windows, show tasks instead of details
    int space_for_tasks
      = main_buffer_->getDimensions().row - 2 - details_->getHeight() - 1;
    bool no_details = space_for_tasks < minTasksListHeight;

    // Details
    if (no_details) {
        details_frame_.reset();
    } else {
        details_frame_.reset(new term::Frame(
          main_buffer_.get(),
          {2, 0},
          {details_->getHeight(), main_buffer_->getDimensions().col}));
        details_->resize(details_frame_.get());
    }

    // Round tasks list
    if (no_details) {
        tasks_frame_.reset(
          new term::Frame(main_buffer_.get(), {2, 0}, {0, 0}, true));
    } else {
        tasks_frame_.reset(
          new term::Frame(main_buffer_.get(),
                          {2 + details_frame_->getDimensions().row + 1, 0},
                          {0, 0},
                          true));
    }
    tasks_->resize(tasks_frame_.get());

    // Fuzzy search
    search_frame_.reset(new term::Frame(
      main_buffer_.get(),
      {1, 0},
      {fuzzySearchRecommendedHeight(main_buffer_->getDimensions().row - 1),
       main_buffer_->getDimensions().col}));
    if (search_ != nullptr) search_->resize(search_frame_.get());
}

void
pgRound::draw()
{
    title_buffer_->write(true);
    if (details_frame_ != nullptr) details_->draw();
    tasks_->draw();
    if (search_ != nullptr) search_->draw();
    main_buffer_->write();
    main_buffer_->clear();
}

bool
pgRound::handleEvent(const Event& event)
{
    // Maybe that event can be handled by the fuzzy search
    if (search_ != nullptr && search_->handleEvent(event)) return true;

    // Maybe that event can be handled by tasks list
    if (search_ == nullptr && tasks_->handleEvent(event)) return true;

    // Handle keyPressed event
    if (event.type == EventType::keyPressed) {
        const term::Key& key             = std::any_cast<term::Key>(event.data);
        const config::Keybinds& keybinds = config_->getKeybinds();

        // Open fuzzy search
        if (keybinds.searchOpen == key) {
            if (search_ != nullptr) return false;  // can't re-open
            search_.reset(new elNavFuzzySearch(
              {{{events_, config_, locale_, fetcher_},
                elDataFuzzySearch::SearchType::roundTasksList,
                round_->getId()}}));
            if (search_frame_ != nullptr) search_->resize(search_frame_.get());
            return true;
        }

        // Close fuzzy search
        if (keybinds.searchExit == key) {
            if (search_ == nullptr) return false;
            search_.reset();
            return true;
        }

        // Go back in the hierarchy
        if (keybinds.navigationBack == key) {
            actPageOpenData data;
            data.name = "pgContest";
            data.args
              = pgContest::ConstrArgs{{events_, config_, locale_, fetcher_},
                                      round_->getParent()->getId()};
            events_->emplaceEvent(
              EventType::actPageOpen, data, EventPriority::action);
        }
    }

    return false;
}

std::string
pgRound::getName() const
{
    return "pgRound";
}

}  // namespace simc::ui

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/pages/round_task.hpp>

#include <cli/term/buffer.hpp>
#include <cli/term/frame.hpp>
#include <cli/ui/draw.hpp>
#include <cli/ui/elements/data_details_box.hpp>
#include <cli/ui/elements/nav_fuzzy_search.hpp>
#include <cli/ui/pages/round.hpp>
#include <fetcher/fetcher.hpp>

namespace simc::ui
{

pgRoundTask::pgRoundTask(const ConstrArgs& args)
  : events_(args.events)
  , config_(args.config)
  , locale_(args.locale)
  , fetcher_(args.fetcher)
  , task_(fetcher_->getRoundTask(args.round_task_id))
{
    // Get name (for title)
    contest_name_ = task_->getParent()->getParent()->usGetName();

    // Set details box
    details_.reset(new elDataDetailsBox(
      {{args.events, args.config, args.locale, args.fetcher},
       elDataDetailsBox::DetailsType::task,
       task_->getId()}));
}

pgRoundTask::~pgRoundTask() {}

void
pgRoundTask::resize(term::Frame* parent)
{
    // Main
    main_frame_ = parent;
    main_buffer_.reset(new term::Buffer(main_frame_));

    // Title
    title_frame_.reset(new term::Frame(
      main_buffer_.get(), {0, 0}, {1, main_buffer_->getDimensions().col}));
    title_buffer_.reset(new term::Buffer(title_frame_.get()));
    drawTitle(title_buffer_.get(), contest_name_);

    // Details
    details_frame_.reset(new term::Frame(
      main_buffer_.get(),
      {2, 0},
      {details_->getHeight(), main_buffer_->getDimensions().col}));
    details_->resize(details_frame_.get());

    // Fuzzy search
    search_frame_.reset(new term::Frame(
      main_buffer_.get(),
      {1, 0},
      {fuzzySearchRecommendedHeight(main_buffer_->getDimensions().row - 1),
       main_buffer_->getDimensions().col}));
    if (search_ != nullptr) search_->resize(search_frame_.get());
}

void
pgRoundTask::draw()
{
    title_buffer_->write(true);
    details_->draw();
    if (search_ != nullptr) search_->draw();
    main_buffer_->write();
    main_buffer_->clear();
}

bool
pgRoundTask::handleEvent(const Event& event)
{
    // Maybe that event can be handled by the fuzzy search
    if (search_ != nullptr && search_->handleEvent(event)) return true;

    // Handle keyPressed event
    if (event.type == EventType::keyPressed) {
        const term::Key& key             = std::any_cast<term::Key>(event.data);
        const config::Keybinds& keybinds = config_->getKeybinds();

        // Open fuzzy search
        if (keybinds.searchOpen == key) {
            if (search_ != nullptr) return false;  // can't re-open
            search_.reset(new elNavFuzzySearch(
              {{{events_, config_, locale_, fetcher_},
                elDataFuzzySearch::SearchType::roundTasksList,
                task_->getParent()->getId()}}));
            if (search_frame_ != nullptr) search_->resize(search_frame_.get());
            return true;
        }

        // Close fuzzy search
        if (keybinds.searchExit == key) {
            if (search_ == nullptr) return false;
            search_.reset();
            return true;
        }

        // Go back in the hierarchy
        if (keybinds.navigationBack == key) {
            actPageOpenData data;
            data.name = "pgRound";
            data.args
              = pgRound::ConstrArgs{{events_, config_, locale_, fetcher_},
                                    task_->getParent()->getId()};
            events_->emplaceEvent(
              EventType::actPageOpen, data, EventPriority::action);
        }
    }
    return false;
}

std::string
pgRoundTask::getName() const
{
    return "pgRoundTask";
}

}  // namespace simc::ui

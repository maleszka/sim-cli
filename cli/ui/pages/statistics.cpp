// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/pages/statistics.hpp>

namespace simc::ui
{

pgStatistics::pgStatistics(const ConstrArgs& args)
  : events_(args.events)
  , config_(args.config)
  , locale_(args.locale)
  , fetcher_(args.fetcher)
{
    // Statistics page feature is not introduced yet.
    // Simply exit session
    events_->emplaceEvent(
      EventType::actSessionExit, std::string(), EventPriority::action);
}

pgStatistics::~pgStatistics() {}

void
pgStatistics::resize(term::Frame* parent)
{
    (void)parent;
}

void
pgStatistics::draw()
{
}

bool
pgStatistics::handleEvent(const Event& event)
{
    (void)event;
    return false;
}

std::string
pgStatistics::getName() const
{
    return "pgStatistics";
}

}  // namespace simc::ui

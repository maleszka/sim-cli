// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/ui/scroll.hpp>

#include <cli/term/buffer.hpp>
#include <fetcher/utils/macros.hpp>

#include <algorithm>

namespace simc::ui
{

ScrollSegment::ScrollSegment(const term::point_t& dims,
                             std::shared_ptr<const config::Config> config,
                             int scrollbox_begin,
                             int scrollbox_end)
  : dims_(dims)
  , config_(config)
  , scrollbox_begin_(scrollbox_begin == INT_MAX ? 0 : scrollbox_begin)
  , scrollbox_end_(scrollbox_end == INT_MAX ? dims_.row : scrollbox_end)
{
}

void
ScrollSegment::setSelection(term::point_t selection_start, int selection_length)
{
    selection_start_  = selection_start;
    selection_length_ = selection_length;
}

void
ScrollSegment::setBuffer(std::unique_ptr<term::Buffer>&& buffer)
{
    SIMC_ASSERT(buffer->getDimensions().row == dims_.row
                && buffer->getDimensions().col == dims_.col);
    buffer_ = std::move(buffer);
}

void
ScrollSegment::draw(term::Buffer* output,
                    int output_start_row,
                    int start_row,
                    int end_row,
                    bool selected)
{
    SIMC_ASSERT(buffer_ != nullptr);
    SIMC_ASSERT(start_row >= 0);
    SIMC_ASSERT(end_row - start_row + output_start_row
                <= output->getDimensions().row);
    SIMC_ASSERT(dims_.col <= output->getDimensions().col);

    // Add selection
    term::CellString backup(selection_length_);
    if (selected) {
        for (int i = 0; i < selection_length_; ++i) {
            term::point_t pos
              = {selection_start_.row, selection_start_.col + i};
            backup[i]  = buffer_->getCell(pos);
            auto props = backup[i].getProps();
            props.bg   = config_->getColors().selection;
            buffer_->putCh(pos, backup[i].getCh(), props);
        }
    }

    // Write
    for (int row = start_row; row < dims_.row && row < end_row; ++row) {
        output->copyLine({row - start_row + output_start_row, 0},
                         buffer_->rowBegin(row),
                         buffer_->rowEnd(row));
    }

    // Restore selected region to original state
    if (selected) buffer_->putStr(selection_start_, backup);
}

bool
ScrollSegment::isRendered() const
{
    return buffer_ != nullptr;
}

const term::point_t&
ScrollSegment::getDimensions() const
{
    return dims_;
}

ScrollableElement::ScrollableElement(
  std::shared_ptr<const config::Config> config)
  : config_(config)
{
}

void
ScrollableElement::resize(term::Frame* parent)
{
    main_frame_ = parent;
    main_buffer_.reset(new term::Buffer(main_frame_));
    allocateSegments();
    makeSegmentPrefixSums();
    adjustScrollStart();
}

void
ScrollableElement::draw()
{
    // Find the first segment that should be rendered
    size_t first_segment = std::upper_bound(segment_psums_.begin(),
                                            segment_psums_.end(),
                                            scroll_start_row_)
                           - 1 - segment_psums_.begin();
    SIMC_ASSERT(first_segment < segments_.size() || segments_.empty());

    // Render segments and write them to the buffer
    // Naming conventions:
    // * o_ row relatively to the output buffer (main_buffer_)
    // * sc_ row in the scroll buffer (buffer containing all segments)
    // * sg_ row in the segment
    int o_height = main_buffer_->getDimensions().row;
    int sc_start = scroll_start_row_;
    for (size_t segment_id = first_segment;
         segment_id < segments_.size()
         && segment_psums_[segment_id] < scroll_start_row_ + o_height;
         ++segment_id)
    {
        auto& segment                = segments_[segment_id];
        const auto& sc_segment_start = segment_psums_[segment_id];

        // Calculate what range of segment_start we want to output
        int sg_start = sc_start - sc_segment_start;
        int sg_end   = std::min(segment->getDimensions().row,
                              scroll_start_row_ + o_height - sc_segment_start);

        // Make sure that the segment is rendered
        if (!segment->isRendered()) renderSegment(segment_id);

        // Draw
        segment->draw(main_buffer_.get(),
                      sc_start - scroll_start_row_,
                      sg_start,
                      sg_end,
                      (int)segment_id == selected_segment_);
        sc_start += sg_end - sg_start;
    }

    main_buffer_->write();
    main_buffer_->clear();
}

bool
ScrollableElement::handleEvent(const Event& event)
{
    if (event.type == EventType::keyPressed) {
        const auto& key      = std::any_cast<term::Key>(event.data);
        const auto& keybinds = config_->getKeybinds();

        // Select the previous entry
        if (keybinds.navigationUp == key) {
            if (selected_segment_ == -1) return false;
            selected_segment_--;
            adjustScrollStart();
            return true;
        }

        // Select the next entry
        if (keybinds.navigationDown == key) {
            if (selected_segment_ + 1 == (int)segments_.size()) return false;
            selected_segment_++;
            adjustScrollStart();
            return true;
        }
    }
    return false;
}

void
ScrollableElement::makeSegmentPrefixSums()
{
    // segments_psums[i] --- sum of heights for segments < i
    segment_psums_    = std::vector<int>(segments_.size() + 1);
    segment_psums_[0] = 0;
    for (size_t i = 1; i < segment_psums_.size(); ++i) {
        segment_psums_[i]
          = segment_psums_[i - 1] + segments_[i - 1]->getDimensions().row;
    }
}

void
ScrollableElement::adjustScrollStart()
{
    // Edge case: nothing selected
    if (selected_segment_ == -1) {
        scroll_start_row_ = 0;
        return;
    }

    // One element past the last one that we certainly want to have
    int wanted_end = segment_psums_[selected_segment_]
                     + segments_[selected_segment_]->scrollbox_end_;
    SIMC_ASSERT(wanted_end <= segment_psums_.back());
    int at_least      = wanted_end - main_buffer_->getDimensions().row;
    scroll_start_row_ = std::max(scroll_start_row_, at_least);

    // The first line that we certainly want to have
    int wanted_start = segment_psums_[selected_segment_]
                       + segments_[selected_segment_]->scrollbox_begin_;
    SIMC_ASSERT(wanted_start >= 0);
    int at_most       = wanted_start;
    scroll_start_row_ = std::min(scroll_start_row_, at_most);

    // Make sure that such a layout is possible
    SIMC_ASSERT(at_least <= at_most);
}

}  // namespace simc::ui

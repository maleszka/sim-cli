// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UI_SCROLL_HPP_
#define CLI_UI_SCROLL_HPP_

#include <cli/ui/element.hpp>

namespace simc::ui
{

class ScrollSegment
{
   protected:
    term::point_t dims_;
    std::shared_ptr<const config::Config> config_;
    std::unique_ptr<term::Buffer> buffer_;
    term::point_t selection_start_ = {0, 0};
    int selection_length_          = 0;

   public:
    std::any id_;
    int scrollbox_begin_, scrollbox_end_;

    ScrollSegment(const term::point_t& dims,
                  std::shared_ptr<const config::Config> config,
                  int scrollbox_begin = INT_MAX,
                  int scrollbox_end   = INT_MAX);
    void setSelection(term::point_t selection_start, int selection_length);
    void setBuffer(std::unique_ptr<term::Buffer>&& buffer);
    void draw(term::Buffer* output,
              int output_start_row = 0,
              int start_row        = 0,
              int end_row          = INT_MAX,
              bool selected        = false);
    bool isRendered() const;
    const term::point_t& getDimensions() const;
};

class ScrollableElement : public Element
{
   protected:
    std::shared_ptr<const config::Config> config_;

    std::vector<std::unique_ptr<ScrollSegment>> segments_;
    std::vector<int> segment_psums_;
    int selected_segment_ = -1;
    int scroll_start_row_ = 0;

    std::unique_ptr<term::Buffer> main_buffer_;

   public:
    ScrollableElement(std::shared_ptr<const config::Config> config);

    void resize(term::Frame* parent);
    void draw();
    bool handleEvent(const Event& event);

   protected:
    void adjustScrollStart();
    void makeSegmentPrefixSums();
    virtual void allocateSegments()            = 0;
    virtual void renderSegment(int segment_id) = 0;
};

}  // namespace simc::ui

#endif  // CLI_UI_SCROLL_HPP_

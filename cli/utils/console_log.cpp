// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/utils/console_log.hpp>

#include <boost/format.hpp>
#include <iostream>

namespace simc::log
{

// Boost log namespace aliases
namespace expr                    = boost::log::expressions;
namespace attrs                   = boost::log::attributes;

const std::string packStartFormat = "#+BEGIN_%1%;";
const std::string packEndFormat   = "#+END_%1%;";

std::string
extract(const std::string& str, std::string attr_name)
{
    std::string start = (boost::format(packStartFormat) % attr_name).str();
    std::string end   = (boost::format(packEndFormat) % attr_name).str();

    size_t start_pos  = str.find(start);
    size_t end_pos    = str.find(end, start_pos);
    if (start_pos == std::string::npos || end_pos == std::string::npos)
        return "";

    size_t content_start = start_pos + start.size();
    size_t content_end   = end_pos;
    return str.substr(content_start, content_end - content_start);
}

std::string
pack(std::string attr_name, std::string content)
{
    std::string start = (boost::format(packStartFormat) % attr_name).str();
    std::string end   = (boost::format(packEndFormat) % attr_name).str();
    return start + content + end;
}

ConsoleLogBuffer::LogEntry::LogEntry(const std::string& text)
{
    std::cerr << "Constructing log entry: '" << text << "'\n";

    // Severity
    severity = (log::SeverityLevel)std::stoi(extract(text, "severity"));

    // Scope
    scope = extract(text, "scope");

    // Timestamp
    boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
    timestamp = epoch
                + boost::posix_time::milliseconds(
                  std::stoll(extract(text, "timestamp")));

    // Message
    message = extract(text, "message");
}

int
ConsoleLogBuffer::sync()
{
    const std::string& all_text = this->str();
    std::string leftovers;

    // Get entries one by one
    size_t start,  // index of the first character in entry
      end = 0;     // index of character one past newline in entry
    while (true) {
        // Find the next entry string
        start = end;
        end   = all_text.find("\n", start);

        // We have reached end
        if (end == std::string::npos) {
            leftovers = all_text.substr(start);
            break;
        }

        // Push back a new entry
        entries_.emplace_back(all_text.substr(start, end - start));
        end++;
    }

    this->str(leftovers);
    return 0;
}

const std::vector<ConsoleLogBuffer::LogEntry>&
ConsoleLogBuffer::getEntries() const
{
    return entries_;
}

void
consoleLogFormatter(const boost::log::record_view& rec,
                    boost::log::formatting_ostream& strm)
{
    // Severity level
    strm << pack(
      "severity",
      std::to_string(
        (int)boost::log::extract<simc::log::SeverityLevel>("Severity", rec)
          .get()));

    // Scope
    strm << pack(
      "scope",
      namedScopeToStr(
        boost::log::extract<attrs::named_scope::value_type>("Scope", rec)
          .get()));

    // Timestamp
    boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
    boost::posix_time::ptime timestamp
      = boost::log::extract<boost::posix_time::ptime>("TimeStamp", rec).get();
    strm << pack("timestamp",
                 std::to_string((timestamp - epoch).total_milliseconds()));

    // Message
    strm << pack("message", rec[expr::smessage].get());
}

}  // namespace simc::log

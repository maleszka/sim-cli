// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UTILS_CONSOLE_LOG_HPP_
#define CLI_UTILS_CONSOLE_LOG_HPP_

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <cli/utils/logger.hpp>
#include <sstream>
#include <vector>

namespace simc::log
{

class ConsoleLogBuffer : public std::stringbuf
{
   public:
    struct LogEntry
    {
        SeverityLevel severity;
        std::string scope;
        boost::posix_time::ptime timestamp;
        std::string message;
        LogEntry() = default;
        LogEntry(const std::string& text);
    };

   protected:
    std::vector<LogEntry> entries_;

   public:
    int sync();
    const std::vector<LogEntry>& getEntries() const;
};

void consoleLogFormatter(const boost::log::record_view& rec,
                         boost::log::formatting_ostream& strm);

}  // namespace simc::log

#endif  // CLI_UTILS_CONSOLE_LOG_HPP_

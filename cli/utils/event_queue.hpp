// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UTILS_EVENT_QUEUE_HPP_
#define CLI_UTILS_EVENT_QUEUE_HPP_

#include <cli/utils/events.hpp>
#include <condition_variable>
#include <mutex>
#include <queue>

namespace simc
{

class EventQueue
{
   protected:
    std::priority_queue<Event> events_;
    std::mutex mutex_;
    std::condition_variable cv_;

   public:
    void pushEvent(const Event& event);
    void emplaceEvent(const EventType& type,
                      const std::any& data,
                      EventPriority priority);

    Event getEvent();

    size_t size();
    bool empty();
};

}  // namespace simc

#endif  // CLI_UTILS_EVENT_QUEUE_HPP_

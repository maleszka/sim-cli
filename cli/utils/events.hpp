// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UTILS_EVENTS_HPP_
#define CLI_UTILS_EVENTS_HPP_

#include <any>
#include <iostream>

namespace simc
{

struct actPageOpenData
{
    std::string name;
    std::any args;
};

enum class EventType
{
    // Input
    keyPressed,
    inputFailure,

    // Signals
    sigResize,
    sigExit,

    // Actions
    actSessionExit,
    actPageOpen,

    // Critical
    exception,
};

enum class EventPriority
{
    input    = 0,  //!< for user input
    signal   = 1,  //!< for important input
    action   = 2,  //!< for usual actions (e.g. load contest)
    critical = 3,  //!< for errors
};

typedef struct Event
{
    EventType type;
    std::any data;
    EventPriority priority;
} Event;

std::ostream& operator<<(std::ostream& os, const Event& event);
bool operator<(const Event& a, const Event& b);

}  // namespace simc

#endif  // CLI_UTILS_EVENTS_HPP_

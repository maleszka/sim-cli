// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak
// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <algorithm>
#include <cli/utils/fuzzy_search.hpp>
#include <cli/utils/string.hpp>

namespace simc
{

FuzzySearch::FuzzySearch(const std::vector<entry_t>& data,
                         std::shared_ptr<const std::locale> locale)
  : entries_(data)
  , locale_(locale)
{
    // Allocate sorted_
    sorted_.resize(data.size());
    for (size_t i = 0; i < data.size(); ++i) sorted_[i] = i;

    // Create the root search
    search_t root;
    root.prompt         = u"";
    root.prefix_score   = std::vector<int>(data.size(), 0);
    root.prefix_matched = std::vector<matched_t>(data.size());
    searches_.push_back(root);
}

void
FuzzySearch::pushSearch(std::u16string us_search)
{
    searches_.push_back(createSearch(us_search));

    // Apply last's score to the current's (prefix sums)
    auto current = searches_.end() - 1, last = current - 1;
    for (size_t i = 0; i < entries_.size(); ++i) {
        if (current->prefix_score[i] == entryExcluded) continue;
        current->prefix_score[i] += last->prefix_score[i];
        current->prefix_matched[i] |= last->prefix_matched[i];
    }

    if_sorted = false;
}

void
FuzzySearch::popSearch()
{
    if (searches_.size() <= 1) return;
    searches_.pop_back();
    if_sorted = false;
}

std::vector<size_t>
FuzzySearch::getSorted()
{
    if (!if_sorted) sort();
    std::vector<size_t> results;
    const auto& scores = searches_.back().prefix_score;
    for (const auto& x : sorted_) {
        if (scores[x] == entryExcluded) break;
        results.emplace_back(x);
    }
    return results;
}

const FuzzySearch::matched_t&
FuzzySearch::getMatched(size_t index) const
{
    return searches_.back().prefix_matched[index];
}

auto
FuzzySearch::createSearch(std::u16string us_search) -> search_t
{
    search_t search;
    search.prefix_score.resize(entries_.size());
    search.prefix_matched.resize(entries_.size());
    search.prompt = usToLowercase(us_search, *locale_);

    for (size_t i = 0; i < entries_.size(); ++i) {
        // Don't search if already excluded
        if (searches_.back().prefix_score[i] == entryExcluded) {
            search.prefix_score[i] = entryExcluded;
            continue;
        }

        // Perform the search
        auto res                 = fuzzyMatch(entries_[i].us_name,
                              search.prompt,
                              searches_.back().prefix_matched[i]);
        search.prefix_score[i]   = res.first;
        search.prefix_matched[i] = res.second;

        // Apply bonusIdMatch
        if (search.prompt == entries_[i].us_id)
            search.prefix_score[i] += bonusIdMatch;
    }

    return search;
}

auto
FuzzySearch::fuzzyMatch(const std::u16string& us_text,
                        const std::u16string& us_search,
                        const matched_t& prefix_matched,
                        size_t text_pos,
                        size_t search_pos) -> std::pair<int, matched_t>
{
    // Searched word has ended, fully matched
    if (search_pos == us_search.size()) return {0, matched_t()};

    // Text has ended, could not match
    if (text_pos == us_text.size()) return {entryExcluded, matched_t()};

    // Find the best score using backtracking
    int best_score = entryExcluded;
    matched_t best_matched;

    for (size_t pos = text_pos; pos < us_text.size(); ++pos) {
        if (us_text[pos] != us_search[search_pos]) continue;

        auto res = fuzzyMatch(
          us_text, us_search, prefix_matched, pos + 1, search_pos + 1);
        if (res.first == entryExcluded) continue;
        res.second.set(pos);

        // Bonus (penalty) for matching the same position twice
        if (prefix_matched[pos]) res.first += bonusMatchedTwice;

        // Bonus for consecutive matches
        if (pos == text_pos) res.first += bonusConsecutive;

        // Bonus for starting a new word
        if (pos == 0 || isspace(us_text[pos - 1])) res.first += bonusWordStart;

        // Update the best score
        if (best_score < res.first) {
            best_score   = res.first;
            best_matched = res.second;
        }
    }

    return {best_score, best_matched};
}

void
FuzzySearch::sort()
{
    std::sort(sorted_.begin(),
              sorted_.end(),
              [this](int a, int b)
              {
                  auto& scores = this->searches_.back().prefix_score;
                  if (scores[a] == scores[b])
                      return entries_[a].us_name < entries_[b].us_name;
                  return scores[a] > scores[b];
              });
    if_sorted = true;
}

}  // namespace simc

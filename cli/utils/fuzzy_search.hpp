// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak
// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

/**
 * \file    cli/utils/fuzzy_search.hpp
 * \brief   Fuzzy search module
 * \author  Adam Maleszka
 * \author  Mikołaj Krzeszowiak
 */

#ifndef CLI_UTILS_FUZZYSEARCH_HPP_
#define CLI_UTILS_FUZZYSEARCH_HPP_

#include <bitset>
#include <limits>
#include <locale>
#include <memory>
#include <vector>

namespace simc
{


/*******************************************************************************
 * \class     FuzzySearch
 * \brief     Back-end for fuzzy search selection
 *
 * This class implements basic fuzzy search algorithm suited for filtering and
 * searching FetcherData objects. It provides convenient methods for modifying
 * search prompt and returning adequately sorted and filtered list of
 * FetcherData objects.
 ******************************************************************************/
class FuzzySearch
{
   public:
    const static size_t maxNameLength
      = 512;  //!< max size of names and searches
    typedef std::bitset<maxNameLength> matched_t;

    //! Information about given entry (fetcher object)
    struct entry_t
    {
        std::u16string us_name;  //!< object name, lowercase
        std::u16string us_id;    //!< object ID, as string
    };

   protected:
    struct search_t
    {
        std::u16string prompt;
        std::vector<int> prefix_score;
        std::vector<matched_t> prefix_matched;
    };
    constexpr static int entryExcluded = std::numeric_limits<int>::min();

    std::vector<entry_t> entries_;    //!< information about all the entries
    std::vector<search_t> searches_;  //!< searches stack

    bool if_sorted = false;
    std::vector<size_t> sorted_;  //!< entry indexes sorted by score

    // Fuzzy match bonuses
    const static int bonusIdMatch      = 10;
    const static int bonusConsecutive  = 6;
    const static int bonusWordStart    = 2;
    const static int bonusMatchedTwice = -2;

    std::shared_ptr<const std::locale>
      locale_;  //<! locale used for lowercase conversion

   public:
    /**
     * \brief Load objects and prepare data for searches
     *
     * Also converts object names to lowercase according to \p loc to make the
     * search case insensitive.
     */
    FuzzySearch(const std::vector<entry_t>& data,
                std::shared_ptr<const std::locale> locale
                = std::make_shared<std::locale>());

    /**
     * \brief Push new search prompt on to the searches stack
     */
    void pushSearch(std::u16string us_search);

    /**
     * \brief Pop the lastly pushed search from the searches stack
     */
    void popSearch();

    /**
     * \brief Get indices in order that makes the original list sorted
     */
    std::vector<size_t> getSorted();

    /**
     * \brief Get matching data of given entry
     */
    const matched_t& getMatched(size_t index) const;

   protected:
    /**
     * \brief Create new search object to be pushed to the stack
     */
    search_t createSearch(std::u16string us_search);

    /**
     * \brief Calculate score with fuzzy matching
     *
     * Tries to match \p us_search in \p us_text and obtain the best score using
     * backtracking. Score is calculated according to many bonuses that favorize
     * consecutive matches, word starts, and prevent from multiple matches.
     */
    std::pair<int, matched_t> fuzzyMatch(const std::u16string& us_text,
                                         const std::u16string& us_search,
                                         const matched_t& prefix_matched,
                                         size_t text_pos   = 0,
                                         size_t search_pos = 0);
    /**
     * \brief Sorts #sorted_ by score of the whole stack for each entry
     */
    void sort();
};

}  // namespace simc

#endif  // CLI_UTILS_FUZZYSEARCH_HPP_

// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UTILS_LOGGER_SINKS_HPP_
#define CLI_UTILS_LOGGER_SINKS_HPP_

#include <boost/log/sinks.hpp>
#include <boost/log/support/exception.hpp>

namespace simc
{
namespace log
{

/**
 * \brief  Convert named scope to string
 */
std::string namedScopeToStr(
  const boost::log::attributes::named_scope::value_type& scope);

/**
 *  \typedef sink_t
 *  \brief   Sink type used by this module
 */
typedef boost::log::sinks::synchronous_sink<
  boost::log::sinks::text_ostream_backend>
  sink_t;

/**
 * \brief Configures boost.log global attributes
 */
void setupCore();

/*
 * \brief Configures sink with given \p output, \p verbosity, and \p formatter
 */
boost::shared_ptr<sink_t> setupSink(
  boost::shared_ptr<std::ostream> output,
  int verbosity,
  std::function<void(const boost::log::record_view&,
                     boost::log::formatting_ostream& strm)> formatter);

/**
 * \brief Configures file sink and registers it to the boost.log core
 */
boost::shared_ptr<sink_t> setupBasicFileSink(std::string path, int verbosity);

/**
 * \brief Configures and registers sink for use as a console log
 */
boost::shared_ptr<sink_t> setupBasicClogSink(int verbosity);

}  // namespace log
}  // namespace simc
#endif  // CLI_UTILS_LOGGER_SINKS_HPP_

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/utils/string.hpp>

#include <algorithm>
#include <locale>

namespace simc
{

bool
isspace(char16_t ch)
{
    return std::isspace((char)ch);
}

std::vector<std::u16string>
usExtractWords(const std::u16string& str)
{
    std::vector<std::u16string> results;

    size_t start = std::u16string::npos;
    for (size_t i = 0; i < str.size(); ++i) {
        bool ws = isspace(str[i]);

        // Found a start
        if (!ws && start == std::u16string::npos) {
            start = i;
        }

        // Word ended with white-space
        if (ws && start != std::u16string::npos) {
            results.push_back(str.substr(start, i - start));
            start = std::u16string::npos;
            continue;
        }

        // We end without white-space
        if (i + 1 == str.size() && !ws && start != std::u16string::npos) {
            results.push_back(str.substr(start));
            start = std::u16string::npos;
            continue;
        }
    }

    return results;
}

std::u16string
usToLowercase(std::u16string str, const std::locale& loc)
{
    std::u16string result;
    std::transform(str.begin(),
                   str.end(),
                   std::back_inserter(result),
                   [&loc](const char16_t& ch)
                   { return (char16_t)std::tolower((wchar_t)ch, loc); });
    return result;
}

}  // namespace simc

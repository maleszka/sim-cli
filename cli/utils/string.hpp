// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UTILS_STRING_HPP_
#define CLI_UTILS_STRING_HPP_

#include <string>
#include <vector>

namespace simc
{

template<typename T>
std::u16string
to_u16string(T value)
{
    std::string str(std::to_string(value));
    return std::u16string(str.begin(), str.end());
}

bool isspace(char16_t ch);

std::vector<std::u16string> usExtractWords(const std::u16string& str);

std::u16string usToLowercase(std::u16string str, const std::locale& loc);

}  // namespace simc

#endif  // CLI_UTILS_STRING_HPP_

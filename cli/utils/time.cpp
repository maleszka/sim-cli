// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <cli/utils/time.hpp>

#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <sstream>

namespace simc
{

boost::posix_time::time_duration
calculateTimezoneOffset()
{
    return boost::posix_time::second_clock::local_time()
           - boost::posix_time::second_clock::universal_time();
}

std::u16string
ptimeToString(const boost::posix_time::ptime& time)
{
    if (time.is_special()) {
        return u"";
    } else {
        std::stringstream ss;
        boost::posix_time::time_facet* tf(
          new boost::posix_time::time_facet("%Y-%m-%d %H:%M"));
        ss.imbue(std::locale(ss.getloc(), tf));
        ss << time;
        std::string ss_str = ss.str();
        return std::u16string(ss_str.begin(), ss_str.end());
    }
}

boost::posix_time::ptime
localFromUtc(const boost::posix_time::ptime& utc)
{
    if (utc.is_special()) return utc;
    return utc + calculateTimezoneOffset();
}

}  // namespace simc

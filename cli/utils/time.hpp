// ========================================================================== //

// Copyright (C) 2023 Mikołaj Krzeszowiak

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef CLI_UTILS_TIME_HPP_
#define CLI_UTILS_TIME_HPP_

#include <boost/date_time/posix_time/ptime.hpp>

namespace simc
{

boost::posix_time::time_duration calculateTimezoneOffset();

std::u16string ptimeToString(const boost::posix_time::ptime& time);

boost::posix_time::ptime localFromUtc(const boost::posix_time::ptime& utc);

}  // namespace simc

#endif  // CLI_UTILS_TIME_HPP_

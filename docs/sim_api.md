# SIM API documentation

SIM provides convenient but not-well-documented application
programming interface under its `api` subdirectory. Here's a grasp of
useful functionalities.

## Obtaining session cookie

Each HTTP request requires two pieces of information for user
authentication: `session` id and `csrf_token`. Both can be obtained
from a cookie generated at user login:

```shell
curl -s -c session_cookie.txt 'https://sim.13lo.pl/api/sign_in' \
     --form "username=<username>"                               \
     --form "password=<password>"                               \
     --form 'remember_for_a_month=false'                        \
     -H 'x-csrf-token;'                                         \
     > /dev/null
```

Each next API call should attach the cookie and `csrf_token` in both
multipart data and http header:

```shell
curl -s 'https://sim.13lo.pl/api/<command>'        \
     -X 'POST'                                     \
     -b session_cookie.txt                         \
     -H "x-csrf-token: <csrf_token>"               \
     --data "csrf_token=<csrf_token>"
```

For example, to sign out, issue the following command:

```shell
curl -s 'https://sim.13lo.pl/api/sign_out'         \
     -X 'POST'                                     \
     -b session_cookie.txt                         \
     -H "x-csrf-token: <csrf_token>"               \
     --data "csrf_token=<csrf_token>"
```

## Fetching information

All API calls refer to the following logical objects:

- **contest** --- described by _contest ID_, set of rounds;
- **round** --- described by _round ID_, set of round tasks to be
  done in given time range; each round belongs to precisely one
  contest;
- **round task** --- described by _round task ID_, round entry to be
  done and judged according to the settings; associated with exactly
  one problem package and one round;
- **problem package** --- described by _problem ID_, archive with
  problem statement, official solutions, tests, checkers, etc. (sip
  object);
- **submission** --- described by _submission ID_, request to the
  judgement protocol to evaluate given code.

Here's a list of possible API commands:

### Contest details

| url                        | description                                    |
|----------------------------|------------------------------------------------|
| /api/contests              | List of all contests and their IDs             |
| /api/contest/[cID,rID,pID] | Information about contest, round, round tasks  |
| /api/contest\_files/cID    | List of file IDs associated with given contest |

### Submissions

| url                                | description                                              |
|------------------------------------|----------------------------------------------------------|
| /api/submissions/uID               | Submissions list of given user                           |
| /api/submissions/[cID,rID,PID]/uID | Submissions of user uID in contest, round, or round task |
| /api/submissions/=ID               | Judgement reports on given submission                    |

### Rankings

| url                                | description                                                                    |
|------------------------------------|--------------------------------------------------------------------------------|
| /api/contest/[cID,rID,pID]/ranking | Ranking of given contest, round, or round task: done tasks of each participant |

### Downloads

| url                                      | description                           |
|------------------------------------------|---------------------------------------|
| /api/download/contest_file/ID            | Download a contest file with given ID |
| /api/download/statement/contest/pID/name | Download round task's statement       |
| /api/download/submission/ID              | Download submitted code               |

## Submitting code

Submission API call requires additional data:

- problem package ID,
- round task ID,
- language (c11 ,cpp11, cpp14, cpp17, pascal),
- solution code (file path or direct stream).

```shell
curl -s 'https://sim.13lo.pl/api/submission/add/p<package_id>/cp<task_id>' \
     -X 'POST'                                                             \
     -b session_cookie.txt                                                 \
     -H "x-csrf-token: <csrf_token>"                                       \
     --form "csrf_token=<csrf_token>"                                      \
     --form "language=<language>"                                          \
     --form "solution=@<path>"
```

## The state of Fetcher's support

The sim-fetcher library is not only meant to be used with the simc interface.
The fetcher is a separate creation, an interface to the SIM platform so that
other tools can also easily exchange information with the SIM platform. For this
reason, the fetcher tries to achieve the greatest possible API compatibility.

However, it's sure that not all features will be implemented, because they miss
the point of the project. Often they are too administration-specific ---
sim-fetcher is not for cloning SIM instances but for small utilities created by
contestants.

The complete list of fetcher's features include:

- [x] SIM hierarchy:
  - [x] contests list
  - [x] info about a contest
  - [x] info about a round
  - [x] info about a round task
- [ ] submissions:
  - [x] list of the user's submissions
  - [ ] list of all submissions (e.g. if you are a teacher or admin) 
  - [x] info about a submission
  - [x] reports
  - [ ] solution codes
- [ ] rankings
- [ ] downloading files:
  - [ ] contest files
  - [ ] problem statements

Features that will be never implemented:

- global problems (they are actually useless and deprecated)
- changing passwords, account settings
- contest administration (creating contests, rounds, etc.)
- list of other's submissions
- packaging:
  - creating packages ([sip](https://github.com/varqox/sip) is already for that)
  - uploading sip packages
  - browsing sip packages

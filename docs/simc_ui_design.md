# The UI Design

This is a comprehensive documentation of the simc tool and its user interface.

The general purpose of this file is to collect all the ideas and store them in
one place. It's also a reference for the further development.

## Definitions

**Window** --- a rectangular buffer being the representation of the terminal's
printable area occupied by the interactive mode.

**Frame** --- a rectangular box being a logical piece of the _window_ (the
reader can associate frames with `<div>` in HTML). All the _frames_ form a tree
hierarchy, where all the children are fully contained within their parent.
_Window_ is the root of the tree.

**UI element** --- logical part of the UI, fully contained in an associated
_frame_, set to display information and interact with the user.

**UI page** --- predefined layout of UI elements that is displayed in a
specified case and allows to perform specified actions.

**Workspace** --- a folder containing all the files related to given problem
package, managed by simc. Each _workspace_ must contain a `.simc` configuration
file.

**Workspace store** --- a folder or a list of folders containing all the
_workspaces_ managed by simc. _Stores_ should be specified in the simc's
configuration file.

## Features list
### Creating workspaces

Simc should provide some way of fast workspace initialisation. It should
automatically:

- make directory,
- download problem statement,
- extract example test cases from the statement,
- copy and configure (format) templates specified by the user: main solution
  file, brut file, gen file, etc.,
- configure `.simc` file.

### Downloading problem statements

Download problem statement and optionally extract example test cases; it should
output files either to the current workspace or to the specified paths.

### Submitting solutions

Simc should try to submit a solution to the SIM platform according either to the
`.simc` file or the user's options.

In the interactive mode, it should also wait for the results and present them to
the user.

### Viewing submission reports

Once the user has submitted some solution, simc should give their a possibility
to print submission report and see the status, score, initial, and final report.

### Solutions versioning

The user should be able to download their code from earlier submissions and
store the locally in the workspace.

There also should be some small versioning system that let's the user to tag,
name, and save different solutions to the same problem.

### Browsing SIM's hierarchy

There should be a possibility to list contests, view contests, view rounds, and
view round tasks in a nice format.

### Viewing rankings

Ranking is sometimes the best form of motivation --- should be displayed as
nicely as it's possible!

### Statistics

There should be also some form of statistics and analytics helping contestants
to study harder:

- coding frequency (e.g. a diagram as on GitLab and GitHub),
- submissions accuracy,
- submissions count,
- tasks solved,
- and others.

## The interactive mode

This section describes the details of the interactive mode. However, the reader
should keep in mind that many of these things also apply to the non-interactive
mode.

### General principles
#### non-destructive

The UI should be as minimalist and simple as possible, not to disturb the
contestant from the real things that matter.

The window should not cover the whole terminal --- it should only occupy a small
rectangle full width wide and at most 24 chars tall.

The interactive mode should also tidy up everything after exiting to restore the
terminal to the state before starting.

It should be also possible for the user to specify the desired height --- or
maybe the full height for the fullscreen option.

#### good-aligned

Selected information should be displayed only there where that makes sense from
the view of a real use case. There should be no additional and unnecessary
features and animations that distracts only because "they are cool".

#### vertical

Content inside of the window should be scrollable but only in the vertical
direction. There should be no horizontal scrolling because it destructs the
natural flow of reading data in the terminal.

#### unified

There should be many details that make the UI unified; UI elements that are
always in place and the user expects to see them there; common themes that
appear throughout the whole interface.

#### aesthetic

The UI should be as elegant as it's possible to make the experience really
motivational and enjoyable.

### UI elements
#### Page title

At any time, there should be a one-row title describing the current page. For
example, if the user visits the statistics page, the first row of the window
should be:

```
─────────────────────────────────┐ Statistics ┌─────────────────────────────────
```

Anything below is considered to be a _body_.

#### Section separator

Each section should have a heading in the following form:

```
. . . . Initial report . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
```

All dots `.` are separated by exactly one space, none of the dots should be
adjacent to the header text, and the row should start with a dot, not a space.

It's also possible for the separator to appear without any header text.

#### Fuzzy search

This UI element is the recommended way of navigating through the UI. It can be
opened any time at pages: statistics, contest, round, round task, to:

- fuzzy match through the current entries,
- go up in the hierarchy,
- select an entry and enter it,
- select an entry without entering it, simply browse sub-entries instead.

Fuzzy search will always take the first rows of the body. So in the contest page
it will look this way:

```
────────────────────────────────┐ Contest name ┌────────────────────────────────
> Some search text
id1  Round1                                                               3 /  5
id2  Round2                                                               2 / 10
id3  Round3                                                              13 / 13
id4  Round4                                                               0 /  3
id5  Round5                                                               2 /  5
id6  Round6                                                               1 / 17
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
```

#### Contest details

General contest dashboard that appears after selecting a contest. It includes:

- rounds list:
  - name
  - id
  - total score
  - solved tasks
  - total tasks
  - time schedule

En example page showing `XXX OI Przygotowania' contest details:

```
────────────────────────────┐ XXX OI Przygotowania ┌────────────────────────────

          │ Contest:                     XXX OI Przygotowania (210)
          │ Total problems:                                       2
          │ Solved:                                          0 (88)
          │ Last submission:                   2023-01-27 15:01 (0)

. . . . Rounds . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

 1307 ─────── Runda 1                                                     0 / 2
  Time: 2023-01-27 09:00 -- 2023-01-27 15:15
  Solved: 0 (88)
  Last submission: 2023-01-27 15:01

 1309 ─────── Runda 2                                                     0 / 2
  Time: 2023-01-28 09:00 -- 2023-01-28 14:00
  Solved: 0 (0)


```

The contest details page shows only information about the rounds and hides the
information about individual tasks and their status, not to make the UI
overloaded with the data.

To conveniently browse problems without entering a round, the user should be
able to open a problems listing page.

#### Round details

The round details page shows information about the given round, it's:

- name (and id)
- time schedule
- full results since
- total number of problems
- total scored points
- number of problems solved
- last submission: date and score

It also included a problems listing below.

```
────────────────────────────┐ XXX OI Przygotowania ┌────────────────────────────

          │ Round:                                   Runda 1 (1307)
          │ Time:              2023-01-27 09:00 -- 2023-01-27 15:15
          │ Full results:                          2023-01-27 19:00
          │ Total problems:                                       2
          │ Solved:                                          0 (88)
          │ Last submission:                   2023-01-27 15:01 (0)

```

#### Problems (tasks) listing

Problems listing is made up of three sections: _In progress_, _To be done_, and
_Done_. Each round task is assigned to exactly one section:

1. if the user submitted nothing to it, then it belongs to the _To be done_
   section;
2. if the user submitted something, and the final submission status is OK and
   score 100, then it belongs to the _Done_ section.
3. otherwise, it belongs to the _In progress_ section.

Each section contains tasks sorted by the last submission's date, so that the
most active and recent problems come up first.

For each problem (round task), that information is displayed:

- round name (only for contest problems listing; in parenthesis, lowercase)
- round task name
- last submission date
- final submission score and status

Example:

```
────────────────────────────┐ XXX OI Przygotowania ┌────────────────────────────

. . . . In progress (2) . . . . . . . . . . . . . . . . . . . . . . . . . . . .

┃ 2058 twierdzenie Sprague-Grundy'ego (matma)         2023-03-30 19:00  TLE (50)
┃ 2071 funkcja Phi, CTR (matma)                       2023-03-19 08:00   RLE (0)

. . . . To be done (3) . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

┃ 2007 liniówki: algorytm Manachera (algorytmy tekstowe)
┃ 2012 hashowanie: paradoks urodzin (algorytmy tekstowe)
┃ 2063 Pierwiastki: algorytm Mo (struktury danych)

. . . . Done (2) . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

┃ 1998 Grafy: SCC (grafy)                                       2022-12-24 17:36
┃ 2061 kolejka monotoniczna (struktury danych)                  2022-12-30 15:03

```

### Usage flow

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

/**
 * \file      fetcher/api_urls.cpp
 * \brief     List of command urls of SIM API
 * \author    Adam Maleszka <adam_maleszka@AOL.com>
 *
 * Advanced commands contain placeholders for \c boost::format library. For more
 * information on commands meanings, refer to \ref README.md "README.md".
 */

#include <string>

namespace simc
{
namespace url
{

// Session management
const std::string signIn  = "/api/sign_in";
const std::string signOut = "/api/sign_out";

// Contest details
const std::string contestsList     = "/api/contests";
const std::string contestInfo      = "/api/contest/c%1%";
const std::string roundInfo        = "/api/contest/r%1%";
const std::string roundTaskInfo    = "/api/contest/p%1%";
const std::string contestFilesList = "/api/contest_files/c%1%";

// Submissions
const std::string userSubmits      = "/api/submissions/u%1%";
const std::string userSubmitsLower = "/api/submissions/u%1%/<%2%";
const std::string submitReports    = "/api/submissions/=%1%";

// Rankings
const std::string contestRanking   = "/api/contest/c%1%/ranking";
const std::string roundRanking     = "/api/contest/r%1%/ranking";
const std::string roundTaskRanking = "/api/contest/p%1%/ranking";

// Downloads
const std::string downloadContestFile = "/api/download/contest_file/%1%";
const std::string downloadRoundTaskStatement
  = "/api/download/statement/contest/p%1%/%2%";
const std::string downloadSubmitCode = "/api/download/submission/%1%";

// Submit code: %1% is package id and %2% is round task id
const std::string submitCode = "/api/submission/add/p%1%/cp%2%";

}  // namespace url
}  // namespace simc

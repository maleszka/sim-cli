// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

/**
 * \file      fetcher/curl_utils.cpp
 * \brief     Utilities strongly related to libcurl, separated for clearness
 * \author    Adam Maleszka <adam_maleszka@aol.com>
 *
 */

#include <boost/algorithm/string.hpp>
#include <curl/curl.h>
#include <stdexcept>
#include <vector>

namespace simc
{

//! Obtain cookie value from list produced by \c CURLINFO_COOKIELIST
std::string
getCookie(struct curl_slist* cookies, std::string name)
{
    struct curl_slist* entry = cookies;
    while (entry) {
        std::string cookie_line = entry->data;
        std::vector<std::string> line_words;
        boost::split(line_words,
                     cookie_line,
                     boost::is_any_of("\t"),
                     boost::token_compress_on);
        entry = entry->next;
        if (line_words.size() != 7)
            throw std::runtime_error("cookie file corrupted");
        if (line_words[5] == name) return line_words[6];
    }
    return "";
}

//! Function for \c CURLOPT_WRITEFUNCTION to output data to \c std::string
size_t
stringWriteCallback(char* body,        //!< [in] libcurl output data
                    size_t base_size,  //!< [in] size of \c char
                    size_t body_size,  //!< [in] number of elements in \p body
                    std::string* out   //!< [out] string to write data to
)
{
    out->append(body, base_size * body_size);
    return base_size * body_size;
}

}  // namespace simc

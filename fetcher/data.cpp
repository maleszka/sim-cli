// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <algorithm>
#include <boost/algorithm/string/replace.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/format.hpp>
#include <boost/json.hpp>
#include <fetcher/data.hpp>
#include <stdexcept>
#include <utf8.h>

namespace simc
{

/*******************************************************************************
 *  Utilities
 ******************************************************************************/

std::vector<std::vector<std::string>>
extractGroupsFromTable(std::string table_html)
{
    // Extract the table's body
    size_t body_start = table_html.find("<tbody>") + 7;
    size_t body_end   = table_html.find("</tbody>", body_start);
    if (body_end == std::string::npos)
        throw std::runtime_error(
          "failed parsing report html: there is no table");
    std::string body = table_html.substr(body_start, body_end - body_start);

    // Extract each row
    std::vector<std::string> rows;
    size_t row_start, row_end = 0;
    do {
        row_start = body.find("<tr>", row_end);
        row_end   = body.find("</tr>", row_start);
        if (row_end == std::string::npos) break;
        row_end += 5;
        rows.emplace_back(body.substr(row_start + 4, row_end - row_start - 9));
    } while (true);

    // Divide rows into groups
    std::vector<std::vector<std::string>> groups;
    for (const auto& row : rows) {
        if (row.find("groupscore") != std::string::npos) groups.emplace_back();
        groups.back().emplace_back(row);
    }
    return groups;
}

SubmissionStatus
statusFromString(std::string text)
{
    if (text == "") {
        return SubmissionStatus::empty;
    } else if (text == "pending") {
        return SubmissionStatus::pending;
    } else if (text == "OK") {
        return SubmissionStatus::ok;
    } else if (text == "Time limit exceeded") {
        return SubmissionStatus::timeLimitExceeded;
    } else if (text == "Memory limit exceeded") {
        return SubmissionStatus::memoryLimitExceeded;
    } else if (text == "Wrong answer") {
        return SubmissionStatus::wrongAnswer;
    } else if (text == "Compilation failed") {
        return SubmissionStatus::compilationFailed;
    } else if (text == "Runtime error") {
        return SubmissionStatus::runtimeError;
    } else if (text == "Judge error" || text == "Checker error")
    {  // SIM has two different names for this
        return SubmissionStatus::checkerError;
    } else {
        throw std::runtime_error("unknown submission status text '" + text
                                 + "'");
    }
}

boost::posix_time::ptime
ptimeFromString(const std::string& date)
{
    if (date == "+inf") return boost::posix_time::pos_infin;
    if (date == "-inf") return boost::posix_time::neg_infin;
    return boost::posix_time::time_from_string(date);
}


/*******************************************************************************
 *  FetcherData
 ******************************************************************************/
FetcherData::FetcherData(const boost::json::array& desc,
                         const boost::json::array& data)
{
    for (size_t col = 0; col < desc.size(); ++col) {
        std::string name(desc[col].as_string());
        if (name == "id")
            id_ = data[col].as_int64();
        else if (name == "item")
            item_ = data[col].as_int64();
        else if (name == "name") {
            std::u32string ucs4_name = utf8::utf8to32(data[col].as_string());
            us_name_.resize(ucs4_name.size());
            for (size_t i = 0; i < ucs4_name.size(); ++i) {
                us_name_[i]
                  = (ucs4_name[i] > std::numeric_limits<char16_t>::max()
                       ? u'?'
                       : (char16_t)ucs4_name[i]);
            }
        }
    }
}

FetcherData::~FetcherData() {}

void
FetcherData::sortChildren()
{
    std::sort(children_.begin(),
              children_.end(),
              [](const std::shared_ptr<FetcherData>& a,
                 const std::shared_ptr<FetcherData>& b)
              { return a->item_ < b->item_; });
}

void
FetcherData::addChild(std::shared_ptr<FetcherData> child)
{
    children_.emplace_back(child);
    child->parent_ = this;
}

std::shared_ptr<FetcherData>
FetcherData::getChild(std::u16string us_name) const
{
    for (const auto& child : children_)
        if (child->usGetName() == us_name) return child;
    return nullptr;
}

int
FetcherData::getTotalTasks() const
{
    int total_tasks = 0;
    for (auto child : children_) total_tasks += child->getTotalTasks();
    return total_tasks;
}

// clang-format off
int FetcherData::getId() const { return id_; }
std::u16string FetcherData::usGetName() const { return us_name_; }
int FetcherData::getTotalScore() const { return total_score_; }
int FetcherData::getTotalSolvedTasks() const { return total_solved_tasks_; }
FetcherData* FetcherData::getParent() const { return parent_; }
const std::vector<std::shared_ptr<FetcherData>>& FetcherData::getChildren() const
{ return children_; }
// clang-format on


/*******************************************************************************
 *  FetcherDataContest
 ******************************************************************************/
FetcherDataContest::FetcherDataContest(const boost::json::array& desc,
                                       const boost::json::array& contest_info)
  : FetcherData(desc, contest_info)
{
    for (size_t col = 0; col < desc.size(); ++col) {
        std::string name(desc[col].as_string());
        if (name == "is_public") {
            is_public_ = contest_info[col].as_bool();
        } else if (name != "id" && name != "item" && name != "name"
                   && name != "actions")
        {
            throw std::runtime_error(
              (boost::format("failed parsing contest: invalid column '%1%'")
               % name)
                .str());
        }
    }
}

// clang-format off
bool FetcherDataContest::getIsPublic() const { return is_public_; }
// clang-format on


/*******************************************************************************
 *  FetcherDataRound
 ******************************************************************************/
FetcherDataRound::FetcherDataRound(const boost::json::array& desc,
                                   const boost::json::array& round_info)
  : FetcherData(desc, round_info)
{
    for (size_t col = 0; col < desc.size(); ++col) {
        std::string name(desc[col].as_string());
        if (name == "ranking_exposure") {
            ranking_date_
              = ptimeFromString(std::string(round_info[col].as_string()));
        } else if (name == "begins") {
            start_date_
              = ptimeFromString(std::string(round_info[col].as_string()));
        } else if (name == "full_results") {
            results_date_
              = ptimeFromString(std::string(round_info[col].as_string()));
        } else if (name == "ends") {
            end_date_
              = ptimeFromString(std::string(round_info[col].as_string()));
        } else if (name != "id" && name != "item" && name != "name") {
            throw std::runtime_error(
              (boost::format("failed parsing round: invalid column '%1%'")
               % name)
                .str());
        }
    }
}

// clang-format off
int FetcherDataRound::getTotalTasks() const { return children_.size(); }
boost::posix_time::ptime FetcherDataRound::getStartDate() const { return start_date_; }
boost::posix_time::ptime FetcherDataRound::getEndDate() const { return end_date_; }
boost::posix_time::ptime FetcherDataRound::getRankingDate() const { return ranking_date_; }
boost::posix_time::ptime FetcherDataRound::getResultsDate() const { return results_date_; }
// clang-format on


/*******************************************************************************
 *  FetcherDataRoundTask
 ******************************************************************************/
FetcherDataRoundTask::FetcherDataRoundTask(
  const boost::json::array& desc,
  const boost::json::array& round_task_info)
  : FetcherData(desc, round_task_info)
{
    for (size_t col = 0; col < desc.size(); ++col) {
        std::string name(desc[col].as_string());
        auto& value = round_task_info[col];
        if (name == "round_id") {
            round_id_ = value.as_int64();
        } else if (name == "problem_id") {
            problem_id_ = value.as_int64();
        } else if (name == "can_view_problem") {
            // _can_view_problem = value.as_bool();  // it's SIM
            // front-end-specific
        } else if (name == "problem_label") {
            problem_label_ = value.as_string();
        } else if (name == "method_of_choosing_final_submission") {
            std::string str(value.as_string());
            if (str == "highest_score")
                final_submit_rule_ = FinalSubmitRule::highestScore;
            else
                final_submit_rule_ = FinalSubmitRule::latestCompiling;
        } else if (name == "score_revealing") {
            // score_revealing_rule_ = value.as_string();  // useless
        } else if (name != "id" && name != "item" && name != "name"
                   && name != "color_class")
        {
            throw std::runtime_error(
              (boost::format("failed parsing round task: invalid column '%1%'")
               % name)
                .str());
        }
    }
}

// clang-format off
int FetcherDataRoundTask::getTotalTasks() const { return 1; }
int FetcherDataRoundTask::getRoundId() const { return round_id_; }
int FetcherDataRoundTask::getProblemId() const { return problem_id_; }
std::string FetcherDataRoundTask::getProblemLabel() const
{ return problem_label_; }
FinalSubmitRule FetcherDataRoundTask::getFinalSubmitRule() const
{ return final_submit_rule_; }
// clang-format on


/*******************************************************************************
 *  SubmissionReport
 ******************************************************************************/

SubmissionReport::SubmissionReport(const boost::json::array& desc,
                                   const boost::json::array& info)
{
    std::string ns_initial = "";
    std::string ns_final   = "";
    for (size_t col = 0; col < desc.size(); ++col) {
        if (!desc[col].if_string()) continue;
        std::string name(desc[col].as_string());
        if (name == "initial_report")
            ns_initial = info[col].as_string();
        else if (name == "final_report")
            ns_final = info[col].as_string();
    }

    // Parse initial report
    if (ns_initial == "") return;
    {
        const auto& groups = extractGroupsFromTable(ns_initial);
        for (const auto& group : groups)
            initial_report_.emplace_back(ReportGroup(group));
    }

    // Parse final report
    if (ns_final == "") return;
    {
        const auto& groups = extractGroupsFromTable(ns_final);
        for (const auto& group : groups)
            final_report_.emplace_back(ReportGroup(group));
    }

    // Parse tests comments
    {
        // Extract comments list from ns_final
        size_t comments_start = ns_final.find("<ul class=\"tests-comments\">");
        size_t comments_end   = ns_final.find("</ul>", comments_start);
        if (comments_start == std::string::npos) return;
        comments_start += 27;
        std::string comments
          = ns_final.substr(comments_start, comments_end - comments_start);

        // Extract individual comments
        size_t comment_start, comment_end = 0;
        do {
            comment_start = comments.find("<li>", comment_end);
            comment_end   = comments.find("</li>", comment_start);
            if (comment_start == std::string::npos) break;
            comment_start += 4;
            comments_.emplace_back(TestComment(
              comments.substr(comment_start, comment_end - comment_start)));
        } while (true);
    }
}

SubmissionReport::ReportGroup::ReportGroup(
  const std::vector<std::string>& rows_html)
{
    // Obtain group score
    {
        std::string row = rows_html.front();
        size_t gs_start = row.find(">", row.find("<td class=\"groupscore\""));
        size_t gs_end   = row.find("</td>", gs_start);
        if (gs_end == std::string::npos)
            throw std::runtime_error(
              "failed parsing report html: il-formed group");
        std::string gs_str    = row.substr(gs_start + 1, gs_end - gs_start - 1);
        std::string score_str = gs_str.substr(0, gs_str.find(' '));
        std::string score_max_str(gs_str.begin() + gs_str.rfind(' ') + 1,
                                  gs_str.end());
        score     = std::stoi(score_str);
        score_max = std::stoi(score_max_str);
    }

    // Add the first row
    tests.emplace_back(ReportRow(
      rows_html[0].substr(0, rows_html[0].find("<td class=\"groupscore\""))));

    // Add remaining rows
    for (size_t i = 1; i < rows_html.size(); ++i)
        tests.emplace_back(ReportRow(rows_html[i]));
}

SubmissionReport::ReportRow::ReportRow(const std::string& data_html)
{
    // Obtain test name
    size_t col_start = data_html.find("<td>") + 4;
    size_t col_end   = data_html.find("</td>", col_start);
    test_name        = data_html.substr(col_start, col_end - col_start);

    // Obtain status
    col_start
      = data_html.find("\">", data_html.find("<td class=\"status", col_end));
    col_end                 = data_html.find("</td>", col_start);
    std::string status_text = data_html.substr(col_start, col_end - col_start);
    status                  = statusFromString(status_text);

    // Obtain times
    col_start            = data_html.find("<td>", col_end) + 4;
    col_end              = data_html.find("</td>", col_start);
    std::string time_str = data_html.substr(col_start, col_end - col_start);
    std::string time_consumed_str = time_str.substr(0, time_str.find(' '));
    std::string time_limit_str(time_str.begin() + time_str.rfind(' ') + 1,
                               time_str.end());
    time_consumed = std::stof(time_consumed_str);
    time_limit    = std::stof(time_limit_str);

    // Obtain memory
    col_start              = data_html.find("<td>", col_end) + 4;
    col_end                = data_html.find("</td>", col_start);
    std::string memory_str = data_html.substr(col_start, col_end - col_start);
    std::string memory_consumed_str
      = memory_str.substr(0, memory_str.find(' '));
    std::string memory_limit_str(memory_str.begin() + memory_str.rfind(' ') + 1,
                                 memory_str.end());
    memory_consumed = std::stoi(memory_consumed_str);
    memory_limit    = std::stoi(memory_limit_str);
}

SubmissionReport::TestComment::TestComment(const std::string& data_html)
{
    // Extract individual parts from data_html
    size_t name_start = data_html.find("<span class=\"test-id\">") + 22;
    size_t name_end   = data_html.find("</span>", name_start);
    test_name         = data_html.substr(name_start, name_end - name_start);
    message           = data_html.substr(name_end + 7);

    // Replace html stuff in message
    boost::replace_all(message, "&apos;", "'");
}

// clang-format off
const std::vector<SubmissionReport::ReportGroup>&
SubmissionReport::getInitialReport() const
{ return initial_report_; }
const std::vector<SubmissionReport::ReportGroup>&
SubmissionReport::getFinalReport() const
{ return final_report_; }
const std::vector<SubmissionReport::TestComment>&
SubmissionReport::getComments() const
{ return comments_; }
// clang-format on


/*******************************************************************************
 *  Submission
 ******************************************************************************/

Submission::Submission(const boost::json::array& desc,
                       const boost::json::array& submit_info)
{
    for (size_t col = 0; col < desc.size(); ++col) {
        auto& value = submit_info[col];
        if (desc[col].if_string()) {
            std::string name(desc[col].as_string());
            if (name == "id")
                id_ = value.as_int64();
            else if (name == "type")
                is_final_ = (value.as_string() == "Final");
            else if (name == "language") {
                std::string lang(value.as_string());
                if (lang == "C++17")
                    language_ = SolutionLanguage::cpp17;
                else if (lang == "C++14")
                    language_ = SolutionLanguage::cpp14;
                else if (lang == "C++11")
                    language_ = SolutionLanguage::cpp11;
                else if (lang
                         == "Pascal")  // TODO: ensure that this name exists
                    language_ = SolutionLanguage::pascal;
                else if (lang == "C11")  // TODO: ensure that this name exists
                    language_ = SolutionLanguage::c11;
            } else if (name == "problem_id")
                package_id_ = value.as_int64();
            else if (name == "contest_problem_id")
                round_task_id_ = (value.is_null() ? 0 : value.as_int64());
            else if (name == "contest_round_id")
                round_id_ = (value.is_null() ? 0 : value.as_int64());
            else if (name == "contest_id")
                contest_id_ = (value.is_null() ? publicProblemsContestId
                                               : value.as_int64());
            else if (name == "submit_time") {
                date_ = ptimeFromString(std::string(value.as_string()));
            } else if (name == "score")
                score_ = (value.is_null() ? -1 : value.as_int64());
        } else if (desc[col].if_object()
                   && desc[col].as_object().at("name") == "status")
        {
            boost::json::array sdesc
              = desc[col].as_object().at("fields").as_array();
            boost::json::array sinfo = value.as_array();
            for (size_t scol = 0; scol < sdesc.size(); ++scol)
                if (sdesc[scol].as_string() == "text") {
                    std::string color_text(sinfo[scol].as_string());
                    status_ = statusFromString(color_text);
                }
        }
    }
}

void
Submission::loadReport(const boost::json::array& desc,
                       const boost::json::array& info)
{
    report_.reset(new SubmissionReport(desc, info));
}

// clang-format off
int Submission::getId() const { return id_; }
int Submission::getPackageId() const { return package_id_; }
int Submission::getRoundTaskId() const { return round_task_id_; }
int Submission::getRoundId() const { return round_id_; }
int Submission::getContestId() const { return contest_id_; }
boost::posix_time::ptime Submission::getDate() const { return date_; }
bool Submission::getIsFinal() const { return is_final_; }
SolutionLanguage Submission::getLanguage() const { return language_; }
SubmissionStatus Submission::getStatus() const { return status_; }
int Submission::getScore() const { return score_; }
std::shared_ptr<SubmissionReport> Submission::getReport() const { return report_; }
// clang-format on


/*******************************************************************************
 *  SubmissionsList
 ******************************************************************************/

SubmissionsList::SubmissionsList(const boost::json::array& desc,
                                 const std::vector<boost::json::array>& submits)
  : submits_(submits.size())
{
    for (size_t submit = 0; submit < submits.size(); ++submit)
        submits_[submit].reset(new Submission(desc, submits[submit]));
}

void
SubmissionsList::addSubmits(const boost::json::array& desc,
                            const std::vector<boost::json::array>& submits)
{
    for (auto rit = submits.rbegin(); rit < submits.rend(); ++rit)
        submits_.emplace_front(new Submission(desc, *rit));
}

void
SubmissionsList::removeRecent(boost::posix_time::ptime date_from)
{
    auto s_end = std::upper_bound(submits_.begin(),
                                  submits_.end(),
                                  date_from,
                                  [](boost::posix_time::ptime date_from,
                                     const std::shared_ptr<Submission>& submit)
                                  { return submit->getDate() < date_from; });
    submits_.erase(submits_.begin(), s_end);
}

std::vector<std::shared_ptr<Submission>>
SubmissionsList::queryImpl(int id_from,
                           int id_to,
                           std::vector<int> package_ids,
                           std::vector<int> round_task_ids,
                           std::vector<int> round_ids,
                           std::vector<int> contest_ids,
                           boost::posix_time::ptime date_from,
                           boost::posix_time::ptime date_to,
                           bool only_final,
                           bool no_final,
                           std::vector<SolutionLanguage> languages,
                           std::vector<SubmissionStatus> statuses,
                           int score_from,
                           int score_to) const
{
    // Narrow submits_ ranges basing on id_from, date_from, date_to, and id_to
    auto s_begin = std::lower_bound(
      submits_.begin(),
      submits_.end(),
      id_to,
      [&date_to](const std::shared_ptr<Submission>& submit, int id_to)
      { return submit->getId() > id_to || submit->getDate() > date_to; });
    auto s_end = std::upper_bound(
      submits_.begin(),
      submits_.end(),
      id_from,
      [&date_from](int id_from, const std::shared_ptr<Submission>& submit)
      { return submit->getId() < id_from || submit->getDate() < date_from; });

    // Iterate through candidates obtain results
    std::vector<std::shared_ptr<Submission>> results;
    for (auto it = s_begin; it < s_end; ++it) {
        auto curr = *it;

        // Filters: package_ids
        if (!package_ids.empty()
            && std::find(
                 package_ids.begin(), package_ids.end(), curr->getPackageId())
                 == package_ids.end())
            continue;

        // Filters: round_task_ids
        if (!round_task_ids.empty()
            && std::find(round_task_ids.begin(),
                         round_task_ids.end(),
                         curr->getRoundTaskId())
                 == round_task_ids.end())
            continue;

        // Filters: round_ids
        if (!round_ids.empty()
            && std::find(round_ids.begin(), round_ids.end(), curr->getRoundId())
                 == round_ids.end())
            continue;

        // Filters: contest_ids
        if (!contest_ids.empty()
            && std::find(
                 contest_ids.begin(), contest_ids.end(), curr->getContestId())
                 == contest_ids.end())
            continue;

        // Filters: only_final and no_final
        if (only_final && !curr->getIsFinal()) continue;
        if (no_final && curr->getIsFinal()) continue;

        // Filters: languages
        if (!languages.empty()
            && std::find(
                 languages.begin(), languages.end(), curr->getLanguage())
                 == languages.end())
            continue;

        // Filters: statuses
        if (!statuses.empty()
            && std::find(statuses.begin(), statuses.end(), curr->getStatus())
                 == statuses.end())
            continue;

        // Filters: score_from and score_to
        if (curr->getScore() != -1
            && (curr->getScore() > score_to || curr->getScore() < score_from))
            continue;

        // Success!
        results.emplace_back(curr);
    }

    return results;
}

}  // namespace simc

// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

/**
 * \file      fetcher/data.hpp
 * \brief     Abstract SIM API data objects
 * \author    Adam Maleszka <adam_maleszka@aol.com>
 *
 * This file provides set of abstract classes for representation of data
 * obtained from SIM API.
 */

#ifndef FETCHER_DATA_HPP_
#define FETCHER_DATA_HPP_

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/parameter/name.hpp>
#include <boost/parameter/preprocessor.hpp>
#include <deque>
#include <memory>
#include <string>
#include <vector>

namespace boost::json
{
class array;
}

namespace simc
{


/*******************************************************************************
 * \class     FetcherData
 * \brief     Main abstract class that all fetcher's objects inherit from
 ******************************************************************************/
class FetcherData
{
   protected:
    // Identifiers
    int id_;
    int item_ = 0;
    std::u16string us_name_;

    // Statistics
    int total_score_        = 0;
    int total_solved_tasks_ = 0;

    // Tree hierarchy
    std::vector<std::shared_ptr<FetcherData>> children_;
    FetcherData* parent_;

   public:
    virtual ~FetcherData();

    //! Get object ID assigned by the SIM API
    int getId() const;

    //! Get object human-friendly name
    std::u16string usGetName() const;

    //! Get sum over all scores in associated round tasks
    int getTotalScore() const;

    //! Get number of associated round tasks
    virtual int getTotalTasks() const;

    //! Get number of associated and solved round tasks
    int getTotalSolvedTasks() const;

    //! Get parent object (e.g. contest of round)
    FetcherData* getParent() const;

    //! Get children objects (e.g. rounds of contest)
    const std::vector<std::shared_ptr<FetcherData>>& getChildren() const;

    //! Get child object by name
    std::shared_ptr<FetcherData> getChild(std::u16string us_name) const;

   protected:
    //! Construct object basing on JSON data, results of an API call
    FetcherData(const boost::json::array& desc, const boost::json::array& data);

    //! Sort \c _children by \c _item to keep the order as on the SIM platform
    void sortChildren();

    //! Add \p child to \c _children and update child's parent
    void addChild(std::shared_ptr<FetcherData> child);

    friend class Fetcher;
};


/*******************************************************************************
 * \class     FetcherDataContest
 * \brief     Fetcher data class representing contest
 ******************************************************************************/
class FetcherDataContest : public FetcherData
{
   protected:
    bool is_public_ = true;

   public:
    //! Whether this contest is publicly available
    bool getIsPublic() const;

   protected:
    //! Construct contest object basing on JSON data, results of an API call
    FetcherDataContest(const boost::json::array& desc,
                       const boost::json::array& contest_info);

    friend class Fetcher;
};


/*******************************************************************************
 * \class     FetcherDataRound
 * \brief     Fetcher data class representing round
 ******************************************************************************/
class FetcherDataRound : public FetcherData
{
   protected:
    boost::posix_time::ptime start_date_;
    boost::posix_time::ptime end_date_;
    boost::posix_time::ptime ranking_date_;
    boost::posix_time::ptime results_date_;

   public:
    //! Get number of associated round tasks
    int getTotalTasks() const;

    //! Get the date of round start in human-readable format
    boost::posix_time::ptime getStartDate() const;

    //! Get the date of round end in human-readable format
    boost::posix_time::ptime getEndDate() const;

    //! Get the date since when the ranking will be available
    boost::posix_time::ptime getRankingDate() const;

    //! Get the date since when the round results will be available
    boost::posix_time::ptime getResultsDate() const;

   protected:
    //! Construct round object basing on JSON data, results of an API call
    FetcherDataRound(const boost::json::array& desc,
                     const boost::json::array& round_info);

    friend class Fetcher;
};


/*******************************************************************************
 * \class     FetcherDataRoundTask
 * \brief     Fetcher data class representing round entry (task)
 ******************************************************************************/
enum class SubmissionStatus;
enum class FinalSubmitRule;

class FetcherDataRoundTask : public FetcherData
{
   protected:
    int round_id_;
    int problem_id_;
    std::string problem_label_;
    FinalSubmitRule final_submit_rule_;

   public:
    //! Get number of associated round tasks
    int getTotalTasks() const;

    //! Get id of round that this task belongs to
    int getRoundId() const;

    //! Get id of problem package associated with this round task
    int getProblemId() const;

    //! Get label (short, lowercase code) of associated problem
    std::string getProblemLabel() const;

    //! Get the rule of choosing which submission will be considered as final
    FinalSubmitRule getFinalSubmitRule() const;

   protected:
    //! Construct round task object basing on JSON data, results of an API call
    FetcherDataRoundTask(const boost::json::array& desc,
                         const boost::json::array& round_task_info);

    friend class Fetcher;
};

enum class FinalSubmitRule
{
    highestScore,
    latestCompiling,
};


/*******************************************************************************
 * \enum      SubmissionStatus
 * \brief     Judgement report summary (submission status)
 ******************************************************************************/
enum class SubmissionStatus
{
    empty,
    pending,
    ok,
    timeLimitExceeded,
    memoryLimitExceeded,
    wrongAnswer,
    compilationFailed,
    runtimeError,
    checkerError,
};
SubmissionStatus statusFromString(std::string text);


/*******************************************************************************
 * \enum      SolutionLanguage
 * \brief     Programming language of the user's solution
 ******************************************************************************/
enum class SolutionLanguage
{
    c11,
    cpp11,
    cpp14,
    cpp17,
    pascal,
};


/*******************************************************************************
 * \class     SubmissionReport
 * \brief     Full judgement report on the user's submission
 ******************************************************************************/

class SubmissionReport
{
   public:
    class ReportGroup;
    class ReportRow
    {
       public:
        std::string test_name;
        SubmissionStatus status;
        float time_consumed, time_limit;
        int memory_consumed, memory_limit;

       protected:
        ReportRow(const std::string& data_html);
        friend class ReportGroup;
    };

    class ReportGroup
    {
       public:
        std::vector<ReportRow> tests;
        int score, score_max;

       protected:
        ReportGroup(const std::vector<std::string>& rows_html);
        friend class SubmissionReport;
    };

    class TestComment
    {
       public:
        std::string test_name;
        std::string message;

       protected:
        TestComment(const std::string& data_html);
        friend class SubmissionReport;
    };

   protected:
    std::vector<ReportGroup> initial_report_;
    std::vector<ReportGroup> final_report_;
    std::vector<TestComment> comments_;

   public:
    //! Get judgement initial report in the form of nice C++ objects
    const std::vector<ReportGroup>& getInitialReport() const;

    //! Get judgement final report in the form of nice C++ objects
    const std::vector<ReportGroup>& getFinalReport() const;

    //! Get judgement comments (e.g. explanations of errors)
    const std::vector<TestComment>& getComments() const;

   protected:
    //! Construct SubmissionReport object basing on JSON data, results of an API
    //! call
    SubmissionReport(const boost::json::array& desc,
                     const boost::json::array& info);

    friend class Submission;
};


/*******************************************************************************
 * \class     Submission
 * \brief     Data on the user's submission
 ******************************************************************************/
class Submission
{
   protected:
    int id_;
    int package_id_;
    int round_task_id_;
    int round_id_;
    int contest_id_;

    boost::posix_time::ptime date_;
    bool is_final_;
    SolutionLanguage language_;

    SubmissionStatus status_;
    int score_;
    std::shared_ptr<SubmissionReport> report_;

   public:
    //! Get id of this submission
    int getId() const;

    //! Get id of the problem package that this submission solves
    int getPackageId() const;

    //! Get id of the round task associated with this submission
    int getRoundTaskId() const;

    //! Get id of round the associated round task is in
    int getRoundId() const;

    //! Get id of contest the associated round task is in
    int getContestId() const;

    //! Get submission date
    boost::posix_time::ptime getDate() const;

    //! Get whether this submission is the final solution for the round task
    bool getIsFinal() const;

    //! Get the solution's programming language
    SolutionLanguage getLanguage() const;

    //! Get a judgement report summary
    SubmissionStatus getStatus() const;

    //! Get the score of this solution, -1 if not judged
    int getScore() const;

    //! Get the submission judgement report
    std::shared_ptr<SubmissionReport> getReport() const;

   protected:
    //! Construct Submission object basing on JSON data, results of an API call
    Submission(const boost::json::array& desc,
               const boost::json::array& submit_info);

    //! Construct underlying SubmissionReport object, basing on API JSON data
    void loadReport(const boost::json::array& desc,
                    const boost::json::array& info);

    friend class SubmissionsList;
    friend class Fetcher;
};

//! Contest id being set when submission belongs to the 'Problems' page
const int publicProblemsContestId = std::numeric_limits<int>::max();


/*******************************************************************************
 * \class     SubmissionsList
 * \brief     Submissions list management
 ******************************************************************************/

namespace keywords
{

BOOST_PARAMETER_NAME(id_from)
BOOST_PARAMETER_NAME(id_to)
BOOST_PARAMETER_NAME(package_ids)
BOOST_PARAMETER_NAME(round_task_ids)
BOOST_PARAMETER_NAME(round_ids)
BOOST_PARAMETER_NAME(contest_ids)
BOOST_PARAMETER_NAME(date_from)
BOOST_PARAMETER_NAME(date_to)
BOOST_PARAMETER_NAME(only_final)
BOOST_PARAMETER_NAME(no_final)
BOOST_PARAMETER_NAME(languages)
BOOST_PARAMETER_NAME(statuses)
BOOST_PARAMETER_NAME(score_from)
BOOST_PARAMETER_NAME(score_to)

}  // namespace keywords

class SubmissionsList
{
   protected:
    std::deque<std::shared_ptr<Submission>> submits_;

    //! Construct SubmissionsList basing on JSON data, results from an API call
    SubmissionsList(const boost::json::array& desc,
                    const std::vector<boost::json::array>& submits);

    //! Add new submissions to the beginning of the list
    void addSubmits(const boost::json::array& desc,
                    const std::vector<boost::json::array>& submits);

    //! Remove all submissions not older than \p date_from
    void removeRecent(boost::posix_time::ptime date_from);

    std::vector<std::shared_ptr<Submission>> queryImpl(
      int id_from,
      int id_to,
      std::vector<int> package_ids,
      std::vector<int> round_task_ids,
      std::vector<int> round_ids,
      std::vector<int> contest_ids,
      boost::posix_time::ptime date_from,
      boost::posix_time::ptime date_to,
      bool only_final,
      bool no_final,
      std::vector<SolutionLanguage> languages,
      std::vector<SubmissionStatus> statuses,
      int score_from,
      int score_to) const;

   public:
    /**
     * \brief Query the submissions database
     *
     * This function method looks scary but isn't; it's just Boost.Parameter
     * that makes the signature ugly.
     *
     * Run `query` with keyword-based parameters, e.g.
     * `query(keywords::_only_final = true)`.
     *
     * Possible keywords are:
     *
     * - specify ranges:
     *   - \p id_from or \p id_to --- submissions ID range
     *   - \p date_from or \p date_to --- date range (in
     * `boost::posix_time::ptime`)
     *   - \p score_from or \p score_to --- solution scores range
     *
     * - specify accepted IDs:
     *   - \p package_ids
     *   - \p round_task_ids
     *   - \p round_ids
     *   - \p contest_ids
     *
     * - whether final:
     *   - \p only_final --- exclude all non-final submissions
     *   - \p no_final --- exclude all final submissions
     *
     * - \p languages --- list of accepted programming languages
     *
     * - \p statuses --- list of accepted `SubmissionStatus`es
     *
     */
    BOOST_PARAMETER_CONST_MEMBER_FUNCTION(
      (std::vector<std::shared_ptr<Submission>>),
      query,
      keywords::tag,
      (optional                                                               //
       (id_from, (int), 0)                                                    //
       (id_to, (int), std::numeric_limits<int>::max())                        //
       (package_ids, (std::vector<int>), std::vector<int>())                  //
       (round_task_ids, (std::vector<int>), std::vector<int>())               //
       (round_ids, (std::vector<int>), std::vector<int>())                    //
       (contest_ids, (std::vector<int>), std::vector<int>())                  //
       (date_from, (boost::posix_time::ptime), boost::posix_time::neg_infin)  //
       (date_to, (boost::posix_time::ptime), boost::posix_time::pos_infin)    //
       (only_final, (bool), false)                                            //
       (no_final, (bool), false)                                              //
       (languages,
        (std::vector<SolutionLanguage>),
        std::vector<SolutionLanguage>())  //
       (statuses,
        (std::vector<SubmissionStatus>),
        std::vector<SubmissionStatus>())  //
       (score_from, (int), 0)             //
       (score_to, (int), 100)             //
       ))
    {
        return queryImpl(id_from,
                         id_to,
                         package_ids,
                         round_task_ids,
                         round_ids,
                         contest_ids,
                         date_from,
                         date_to,
                         only_final,
                         no_final,
                         languages,
                         statuses,
                         score_from,
                         score_to);
    }

   protected:
    friend class Fetcher;
};

}  // namespace simc

#endif  // FETCHER_DATA_HPP_

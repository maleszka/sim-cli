// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <fetcher/fetcher.hpp>

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/format.hpp>
#include <boost/json.hpp>
#include <fetcher/api_urls.cpp>
#include <fetcher/session.hpp>
#include <stdexcept>


namespace simc
{

Fetcher::Fetcher(std::string sim_url,
                 std::string username,
                 std::string password,
                 FILE* logger)
  : session_(new FetcherSession(sim_url, username, password, logger))
{
    forceUpdate();
}

Fetcher::~Fetcher() {}

void
Fetcher::forceUpdate()
{
    // Perform API call
    std::string raw_output = session_->performApiCall(url::contestsList);
    boost::json::error_code js_ec;
    boost::json::value js_output = boost::json::parse(raw_output, js_ec);
    if (js_ec)
        throw std::runtime_error("failed parsing contests list: "
                                 + js_ec.message());

    // Purge old data
    purgeOldData();

    // Construct data root
    root_.reset(new FetcherData({"id", "name", "item"}, {0, "root", 0}));

    // Obtain fields descriptor
    boost::json::array desc
      = js_output.as_array()[0].as_object()["columns"].as_array();

    // Allocate each contest
    for (auto it = js_output.as_array().begin() + 1;
         it != js_output.as_array().end();
         ++it)
    {
        std::shared_ptr<FetcherDataContest> contest(
          new FetcherDataContest(desc, it->as_array()));
        root_->addChild(contest);
        contest_loaded_[contest->getId()] = false;
        contests_[contest->getId()]       = std::move(contest);
    }
}

void
Fetcher::smallSubmissionsUpdate()
{
    if (submits_ == nullptr)
        loadAllSubmissions();
    else {
        // Request SubmissionsList to remove recent submissions
        submits_->removeRecent(boost::posix_time::second_clock::universal_time()
                               - boost::gregorian::days(1));

        // Maybe we have to download all submissions
        if (submits_->submits_.empty()) {
            loadAllSubmissions();
            return;
        }

        // Download submissions
        const auto& submits
          = fetchSubmissions(submits_->submits_.front()->getId() + 1);
        submits_->addSubmits(submits.first, submits.second);

        // Update submission statistics
        updateDataSubmitsStats();
    }
}

bool
Fetcher::isContestAvailable(int id) const
{
    return contests_.count(id);
}

std::shared_ptr<FetcherDataContest>
Fetcher::getContest(int id, bool load)
{
    auto contest = contests_.find(id);
    if (contest == contests_.end()) return nullptr;
    if (load && !contest_loaded_[id]) lazyLoadContest(contest->second.get());
    return contest->second;
}

std::shared_ptr<FetcherDataContest>
Fetcher::getContest(std::u16string us_name, bool load)
{
    return getContest(root_->getChild(us_name)->getId(), load);
}


std::shared_ptr<FetcherDataRound>
Fetcher::getRound(int id)
{
    auto round = rounds_.find(id);
    if (round != rounds_.end()) return round->second;

    // Perform API call to get contest ID of round
    std::string raw_output
      = session_->performApiCall((boost::format(url::roundInfo) % id).str());
    boost::json::error_code js_ec;
    boost::json::value js_output = boost::json::parse(raw_output, js_ec);
    if (js_ec)
        throw std::runtime_error(
          (boost::format("failed parsing round %1% details: %2%") % id
           % js_ec.message())
            .str());

    // Load appropriate contest
    int contest_id = getContestIdFromJson(js_output);
    getContest(contest_id, true);

    // Return round
    round = rounds_.find(id);
    if (round == rounds_.end())
        throw std::runtime_error(
          (boost::format(
             "cannot get round %1%: not available even though contest "
             "%2% is loaded")
           % id % contest_id)
            .str());
    return round->second;
}


std::shared_ptr<FetcherDataRoundTask>
Fetcher::getRoundTask(int id)
{
    auto round_task = round_tasks_.find(id);
    if (round_task != round_tasks_.end()) return round_task->second;

    // Perform API call to get contest ID of round
    std::string raw_output = session_->performApiCall(
      (boost::format(url::roundTaskInfo) % id).str());
    boost::json::error_code js_ec;
    boost::json::value js_output = boost::json::parse(raw_output, js_ec);
    if (js_ec)
        throw std::runtime_error(
          (boost::format("failed parsing round task %1% details: %2%") % id
           % js_ec.message())
            .str());

    // Load appropriate contest
    int contest_id = getContestIdFromJson(js_output);
    getContest(contest_id, true);

    // Return round
    round_task = round_tasks_.find(id);
    if (round_task == round_tasks_.end())
        throw std::runtime_error(
          (boost::format(
             "cannot get round task %1%: not available even though contest "
             "%2% is loaded")
           % id % contest_id)
            .str());
    return round_task->second;
}


std::vector<std::shared_ptr<FetcherDataContest>>
Fetcher::getContestsList() const
{
    auto& orig = root_->getChildren();
    std::vector<std::shared_ptr<FetcherDataContest>> rt(orig.size());
    for (size_t i = 0; i < orig.size(); i++)
        rt[i] = std::reinterpret_pointer_cast<FetcherDataContest>(orig[i]);
    return rt;
}

std::shared_ptr<FetcherData>
Fetcher::getRoot() const
{
    return root_;
}

std::shared_ptr<SubmissionsList>
Fetcher::getSubmissions()
{
    if (submits_ == nullptr) loadAllSubmissions();
    return submits_;
}

std::shared_ptr<SubmissionReport>
Fetcher::getSubmissionReport(Submission* submit)
{
    // Perform the API call
    std::string raw_output = session_->performApiCall(
      (boost::format(url::submitReports) % submit->getId()).str());
    boost::json::error_code js_ec;
    boost::json::value js_output = boost::json::parse(raw_output, js_ec);
    if (js_ec)
        throw std::runtime_error("failed parsing submission report "
                                 + std::to_string(submit->getId()) + ": "
                                 + js_ec.message());

    // Extract required data
    boost::json::array desc
      = js_output.as_array()[0].as_object()["columns"].as_array();
    boost::json::array info = js_output.as_array()[1].as_array();

    // Pass data to the submit
    submit->loadReport(desc, info);
    return submit->getReport();
}

std::shared_ptr<SubmissionReport>
Fetcher::getSubmissionReport(int id)
{
    getSubmissions();
    const auto& vec
      = submits_->query(keywords::_id_from = id, keywords::_id_to = id);
    if (vec.empty())
        throw std::runtime_error(
          "cannot fetch submission report: cannot find submission of id "
          + std::to_string(id));
    return getSubmissionReport(vec.back().get());
}

// clang-format off
int Fetcher::getUserId() const { return session_->getUserId(); }
std::string Fetcher::getUsername() const { return session_->getUsername(); }
// clang-format on


void
Fetcher::lazyLoadContest(FetcherDataContest* contest)
{
    // Perform an API call
    std::string raw_output = session_->performApiCall(
      (boost::format(url::contestInfo) % contest->getId()).str());
    boost::json::error_code js_ec;
    boost::json::value js_output = boost::json::parse(raw_output, js_ec);
    if (js_ec)
        throw std::runtime_error(
          (boost::format("failed parsing %1% contest details: %2%")
           % contest->getId() % js_ec.message())
            .str());

    // Split data into 2 components: rounds and round_tasks arrays
    boost::json::array global_desc
      = js_output.as_array()[0].as_object()["fields"].as_array();
    boost::json::array round_desc;
    boost::json::array rounds;
    boost::json::array round_task_desc;
    boost::json::array round_tasks;
    for (size_t part = 0; part < global_desc.size(); ++part) {
        std::string name
          = global_desc[part].as_object()["name"].as_string().c_str();
        if (name == "rounds") {
            round_desc = global_desc[part].as_object()["columns"].as_array();
            rounds     = js_output.as_array()[part + 1].as_array();
        } else if (name == "problems") {
            round_task_desc
              = global_desc[part].as_object()["columns"].as_array();
            round_tasks = js_output.as_array()[part + 1].as_array();
        } else if (name != "contest") {
            throw std::runtime_error(
              (boost::format("failed parsing contest info: invalid part '%1%'")
               % name)
                .str());
        }
    }

    // Initialize rounds
    for (auto round_info : rounds) {
        std::shared_ptr<FetcherDataRound> round(
          new FetcherDataRound(round_desc, round_info.as_array()));
        contest->addChild(round);
        rounds_[round->getId()] = std::move(round);
    }
    contest->sortChildren();

    // Initialize round tasks
    for (auto round_task_info : round_tasks) {
        std::shared_ptr<FetcherDataRoundTask> round_task(
          new FetcherDataRoundTask(round_task_desc,
                                   round_task_info.as_array()));
        rounds_[round_task->getRoundId()]->addChild(round_task);
        round_tasks_[round_task->getId()] = std::move(round_task);
    }
    for (auto round : contest->getChildren()) round->sortChildren();

    // Set contest as loaded
    contest_loaded_[contest->getId()] = true;

    // Update submission stats if submissions are downloaded
    if (submits_ != nullptr) updateDataSubmitsStats();
}

void
Fetcher::loadAllSubmissions()
{
    const auto& submits = fetchSubmissions();
    submits_.reset(new SubmissionsList(submits.first, submits.second));
    updateDataSubmitsStats();
}

std::pair<boost::json::array, std::vector<boost::json::array>>
Fetcher::fetchSubmissions(int id_from)
{
    // We will store all data here
    boost::json::array desc;
    std::vector<boost::json::array> submits;

    // Download the very first set of data
    int user_id = session_->getUserId();
    {
        // Perform the API call
        std::string raw_output = session_->performApiCall(
          (boost::format(url::userSubmits) % user_id).str());
        boost::json::error_code js_ec;
        boost::json::value js_output = boost::json::parse(raw_output, js_ec);
        if (js_ec)
            throw std::runtime_error("failed parsing the user's submissions: "
                                     + js_ec.message());

        // Get description
        desc = js_output.as_array()[0].as_object()["columns"].as_array();

        // Store submissions
        submits.reserve(js_output.as_array().size() - 1);
        for (size_t submit = 1; submit < js_output.as_array().size(); ++submit)
            submits.emplace_back(js_output.as_array()[submit].as_array());
    }

    // Download remaining parts of data
    while (!submits.empty()) {
        // Get id of the last submission
        int last_id = Submission(desc, submits.back()).getId();

        // Exit if already reached id_from
        if (last_id <= id_from) break;

        // Perform the API call
        std::string raw_output = session_->performApiCall(
          (boost::format(url::userSubmitsLower) % user_id % last_id).str());
        boost::json::error_code js_ec;
        boost::json::value js_output = boost::json::parse(raw_output, js_ec);
        if (js_ec)
            throw std::runtime_error("failed parsing the user's submissions: "
                                     + js_ec.message());

        // Exit as got nothing new
        if (js_output.as_array().size() <= 1) break;

        // Store submissions
        submits.reserve(submits.size() + js_output.as_array().size() - 1);
        for (size_t submit = 1; submit < js_output.as_array().size(); ++submit)
            submits.emplace_back(js_output.as_array()[submit].as_array());
    }

    return std::pair(
      desc,
      std::vector<boost::json::array>(
        submits.begin(),
        std::upper_bound(submits.begin(),
                         submits.end(),
                         id_from,
                         [&desc](int id_from, boost::json::array sub_js) {
                             return Submission(desc, sub_js).getId() < id_from;
                         })));
}

void
Fetcher::updateDataSubmitsStats()
{
    assert(submits_ != nullptr);

    // Reset data set so far
    for (auto contest : contests_) {
        contest.second->total_score_        = 0;
        contest.second->total_solved_tasks_ = 0;
    }
    for (auto task : round_tasks_) {
        task.second->total_score_        = 0;
        task.second->total_solved_tasks_ = 0;
    }

    // Retrieve submissions
    const auto& submits = submits_->query(keywords::_only_final = true);

    // Process submissions
    for (auto submit : submits) {
        if (contest_loaded_[submit->getContestId()]) {
            auto& task         = round_tasks_[submit->getRoundTaskId()];
            task->total_score_ = submit->getScore();
            task->total_solved_tasks_
              = (submit->getStatus() == SubmissionStatus::ok ? 1 : 0);
        } else if (isContestAvailable(submit->getContestId())) {
            auto& contest = contests_[submit->getContestId()];
            contest->total_score_ += submit->getScore();
            if (submit->getStatus() == SubmissionStatus::ok)
                contest->total_solved_tasks_++;
        }
    }

    // Update round's statistics
    for (auto round : rounds_) {
        round.second->total_score_        = 0;
        round.second->total_solved_tasks_ = 0;
        for (auto child : round.second->getChildren()) {
            round.second->total_score_ += child->total_score_;
            round.second->total_solved_tasks_ += child->total_solved_tasks_;
        }
    }

    // Update contest's statistics
    for (auto contest : contests_) {
        if (contest_loaded_[contest.first]) {
            contest.second->total_score_        = 0;
            contest.second->total_solved_tasks_ = 0;
            for (auto child : contest.second->getChildren()) {
                contest.second->total_score_ += child->total_score_;
                contest.second->total_solved_tasks_
                  += child->total_solved_tasks_;
            }
        }
    }
}

int
Fetcher::getContestIdFromJson(boost::json::value js_output)
{
    boost::json::array global_desc
      = js_output.as_array()[0].as_object()["fields"].as_array();
    for (size_t row = 0; row < global_desc.size(); ++row) {
        if (global_desc[row].as_object()["name"].as_string() != "contest")
            continue;
        boost::json::array desc
          = global_desc[row].as_object()["fields"].as_array();
        boost::json::array info = js_output.as_array()[row + 1].as_array();
        for (size_t col = 0; col < desc.size(); ++col)
            if (desc[col].as_string() == "id") return info[col].as_int64();
    }
    return 0;
}

void
Fetcher::purgeOldData()
{
    rounds_.clear();
    round_tasks_.clear();
    contest_loaded_.clear();
    contests_.clear();
    root_.reset();
    submits_.reset();
}

}  // namespace simc

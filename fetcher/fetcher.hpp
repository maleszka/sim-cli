// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

/**
 * \file      fetcher/fetcher.hpp
 * \brief     The main interface to the sim-fetcher library
 * \author    Adam Maleszka <adam_maleszka@aol.com>
 *
 * This library builds a layer on top of SIM API that handles network
 * communication and translates acquired data into C++ objects.
 */

#ifndef FETCHER_FETCHER_HPP_
#define FETCHER_FETCHER_HPP_

#include <fetcher/data.hpp>
#include <map>
#include <memory>

namespace boost::json
{
class value;
}

namespace simc
{
class FetcherSession;
}

namespace simc
{


/*******************************************************************************
 * \class     Fetcher
 * \brief     Sim-fetcher instance class
 *
 * This class is meant to be the main interface to the sim-fetcher library and
 * all SIM API operations.
 *
 * It utilizes simc::FetcherSession as the session handler and translates JSON
 * objects retrieved from the API into abstract C++ objects.
 *
 * \see       simc::FetcherData
 ******************************************************************************/
class Fetcher
{
   protected:
    //////////////
    // SIM Data //
    //////////////

    //!< abstract object connecting all contests
    std::shared_ptr<FetcherData> root_;

    //! contest objects by ID
    std::map<int, std::shared_ptr<FetcherDataContest>> contests_;

    //! round objects by ID
    std::map<int, std::shared_ptr<FetcherDataRound>> rounds_;

    //! task objects by ID
    std::map<int, std::shared_ptr<FetcherDataRoundTask>> round_tasks_;

    //!< if contest by ID has been loaded
    std::map<int, bool> contest_loaded_;

    //!< submissions manager
    std::shared_ptr<SubmissionsList> submits_;

    ////////////////
    // Connection //
    ////////////////

    //!< SIM API session
    std::unique_ptr<FetcherSession> session_;

   public:
    /**
     * \brief Log in to the SIM API and fetch basic data (e.g. contests list)
     */
    Fetcher(std::string sim_url,   //!< [in] SIM instance url
            std::string username,  //!< [in] login username
            std::string password,  //!< [in] login password
            FILE* logger
            = nullptr  //!< [out] file that libcurl should write verbose
                       //!< log to, set to \c nullptr to disable log
    );
    ~Fetcher();

    /**
     * \brief Invalidate old data and start new cache with updated data
     */
    void forceUpdate();

    /**
     * \brief Update submissions partially
     *
     * When the user watches their submissions, the fetcher is supposed to offer
     * information that is up-to-date. Continuous full updates (see
     * forceUpdate()) may prove too slow and redundant, given that they purge
     * all contest data which changes rather rarely. This method downloads and
     * updates only those submissions that have been sent within the last 24
     * hours, so in the case of small submission updates, this method should be
     * used instead.
     */
    void smallSubmissionsUpdate();

    /**
     * \brief Check whether contest of given ID exists and is visible to the
     * user
     */
    bool isContestAvailable(int id) const;

    /**
     * \brief Get contest object of given \p id
     *
     * If \p load is set to true, it makes sure that given contest is loaded
     * (it's all rounds belonging to the contest and their tasks are
     * initialized). Loading, performed by lazyLoadContest(), takes only one API
     * call. Then, it returns complete contest object.
     */
    std::shared_ptr<FetcherDataContest> getContest(
      int id,           //!< ID of the contest
      bool load = true  //!< whether to load the contest
    );

    /**
     * \brief Get contest object of given \p us_name
     *
     * It first finds contest id and performs getContest(int).
     */
    std::shared_ptr<FetcherDataContest> getContest(
      std::u16string us_name,  //!< UCS2 name of the contest
      bool load = true         //!< whether to load the contest
    );

    /**
     * \brief Get round object of given \p id
     *
     * It makes sure that contest the round belongs to is loaded and then
     * returns cached results.
     */
    std::shared_ptr<FetcherDataRound> getRound(int id);

    /**
     * \brief Get round task object of given \p id
     *
     * It makes sure that contest the round task belongs to is loaded and then
     * returns cached results.
     */
    std::shared_ptr<FetcherDataRoundTask> getRoundTask(int id);

    /**
     * \brief Returns list to all registered contest objects
     *
     * Note that some contests may not be loaded, so make sure to run
     * getContest() before using their children (rounds, tasks, etc).
     */
    std::vector<std::shared_ptr<FetcherDataContest>> getContestsList() const;

    std::shared_ptr<FetcherData> getRoot() const;

    /**
     * \brief Returns a pointer to the submissions database
     *
     * Submissions are only downloaded during the first call of this function
     * member, later calls take almost no time.
     *
     * To update all the submissions, see forceUpdate(), and
     * smallSubmissionsUpdate() for a partial update.
     */
    std::shared_ptr<SubmissionsList> getSubmissions();

    /**
     * \brief Download submission report of given \p submit
     *
     * For each Submission object, associated report is downloaded only once;
     * later cached reports are used. To reset the cache, invoke
     * smallSubmissionsUpdate() or forceUpdate().
     */
    std::shared_ptr<SubmissionReport> getSubmissionReport(Submission* submit);

    /**
     * \brief Download submission report for submit of given \p id
     *
     * It first ensures that given id exists, obtains Submission object and then
     * calls getSubmissionReport(Submission*).
     */
    std::shared_ptr<SubmissionReport> getSubmissionReport(int id);

    //! Return user id from session
    int getUserId() const;

    //! Return username from session
    std::string getUsername() const;

   protected:
    //! Invalidates cache and frees memory
    void purgeOldData();

    //! Loads unloaded (!) contest
    void lazyLoadContest(FetcherDataContest* contest);

    //! Loads all submissions and creates a new submissions database
    void loadAllSubmissions();

    //! Downloads data of submissions with id equal or greater \p id_from
    std::pair<boost::json::array, std::vector<boost::json::array>>
    fetchSubmissions(int id_from = 0);

    //! Update FetcherData statistics related to submissions
    void updateDataSubmitsStats();

    //! Obtains contest ID from API call response
    int getContestIdFromJson(boost::json::value js_output);
};

}  // namespace simc

#endif  // FETCHER_FETCHER_HPP_

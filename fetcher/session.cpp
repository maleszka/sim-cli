// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#include <boost/format.hpp>
#include <boost/json.hpp>
#include <curl/curl.h>
#include <fetcher/api_urls.cpp>
#include <fetcher/curl_utils.cpp>
#include <fetcher/session.hpp>
#include <stdexcept>

namespace simc
{

FetcherSession::FetcherSession(std::string sim_url,
                               std::string username,
                               std::string password,
                               FILE* logger)
  : username_(username)
  , sim_url_(sim_url)
  , curl_handle_(curl_easy_init(), curl_easy_cleanup)
  , logger_(logger)
{
    // Make sure that curl_handle_ has been initialized
    if (curl_handle_ == nullptr)
        throw std::runtime_error("libcurl initialization failed");

    // Main libcurl settings
    std::string body;
    std::string call_url = sim_url + url::signIn;
    curl_easy_setopt(curl_handle_.get(), CURLOPT_URL, call_url.c_str());
    curl_easy_setopt(curl_handle_.get(), CURLOPT_COOKIEFILE, "");
    curl_easy_setopt(
      curl_handle_.get(), CURLOPT_WRITEFUNCTION, stringWriteCallback);
    curl_easy_setopt(curl_handle_.get(), CURLOPT_WRITEDATA, &body);
    setupLogging();

    // Construct HTTP header
    std::unique_ptr<curl_slist, void (*)(curl_slist*)> http_header(
      nullptr, curl_slist_free_all);
    http_header.reset(curl_slist_append(http_header.get(), "x-csrf-token;"));
    curl_easy_setopt(curl_handle_.get(), CURLOPT_HTTPHEADER, http_header.get());

    // Create form data
    std::unique_ptr<curl_mime, void (*)(curl_mime*)> multipart(
      curl_mime_init(curl_handle_.get()), curl_mime_free);
    curl_mimepart* form_username = curl_mime_addpart(multipart.get());
    curl_mime_name(form_username, "username");
    curl_mime_data(form_username, username.c_str(), CURL_ZERO_TERMINATED);
    curl_mimepart* form_password = curl_mime_addpart(multipart.get());
    curl_mime_name(form_password, "password");
    curl_mime_data(form_password, password.c_str(), CURL_ZERO_TERMINATED);
    curl_mimepart* form_remember = curl_mime_addpart(multipart.get());
    curl_mime_name(form_remember, "remember_for_a_month");
    curl_mime_data(form_remember, "false", CURL_ZERO_TERMINATED);
    curl_easy_setopt(curl_handle_.get(), CURLOPT_MIMEPOST, multipart.get());

    // Perform the call!
    CURLcode res_code = curl_easy_perform(curl_handle_.get());
    if (res_code != CURLE_OK)
        throw std::runtime_error(
          (boost::format("failed to perform API call: %1% (%2%)")
           % curl_easy_strerror(res_code) % res_code)
            .str());

    // Obtain cookies
    curl_slist* cookies_p = nullptr;
    curl_easy_getinfo(curl_handle_.get(), CURLINFO_COOKIELIST, &cookies_p);
    std::unique_ptr<curl_slist, void (*)(curl_slist*)> cookies(
      cookies_p, curl_slist_free_all);

    // Reset connection session
    curl_easy_reset(curl_handle_.get());

    // Obtain the csrf_token
    csrf_token_ = getCookie(cookies.get(), "csrf_token");
    if (csrf_token_.empty())
        throw std::runtime_error(
          "could not get session cookies (bad credentials?)");

    // Obtain user_id
    boost::json::error_code js_ec;
    boost::json::value js_body = boost::json::parse(body, js_ec);
    if (js_ec)
        throw std::runtime_error("failed to obtain user_id: "
                                 + js_ec.message());
    user_id_ = js_body.as_object()["session"].as_object()["user_id"].as_int64();
}

FetcherSession::~FetcherSession()
{
    performApiCall(url::signOut);
}

std::string
FetcherSession::performApiCall(std::string command)
{
    // Main libcurl settings
    std::string body;
    std::string call_url = sim_url_ + command;
    curl_easy_setopt(curl_handle_.get(), CURLOPT_URL, call_url.c_str());
    curl_easy_setopt(curl_handle_.get(), CURLOPT_COOKIEFILE, "");
    curl_easy_setopt(
      curl_handle_.get(), CURLOPT_WRITEFUNCTION, stringWriteCallback);
    curl_easy_setopt(curl_handle_.get(), CURLOPT_WRITEDATA, &body);
    setupLogging();

    // Add csrf_token to the header
    std::unique_ptr<curl_slist, void (*)(curl_slist*)> http_header(
      nullptr, curl_slist_free_all);
    http_header.reset(curl_slist_append(
      http_header.get(), ("x-csrf-token: " + csrf_token_).c_str()));
    curl_easy_setopt(curl_handle_.get(), CURLOPT_HTTPHEADER, http_header.get());

    // Add csrf_token as mimepart
    std::unique_ptr<curl_mime, void (*)(curl_mime*)> multipart(
      curl_mime_init(curl_handle_.get()), curl_mime_free);
    curl_mimepart* form_csrf_token = curl_mime_addpart(multipart.get());
    curl_mime_name(form_csrf_token, "csrf_token");
    curl_mime_data(form_csrf_token, csrf_token_.c_str(), CURL_ZERO_TERMINATED);
    curl_easy_setopt(curl_handle_.get(), CURLOPT_MIMEPOST, multipart.get());

    // Perform the call
    CURLcode res_code = curl_easy_perform(curl_handle_.get());
    if (res_code != CURLE_OK)
        throw std::runtime_error(
          (boost::format("failed to perform API call: %1% (%2%)")
           % curl_easy_strerror(res_code) % res_code)
            .str());

    // Reset connection session
    curl_easy_reset(curl_handle_.get());
    return body;
}

void
FetcherSession::setupLogging()
{
    if (logger_ == nullptr) return;
    curl_easy_setopt(curl_handle_.get(), CURLOPT_VERBOSE, 1);
    curl_easy_setopt(curl_handle_.get(), CURLOPT_STDERR, logger_);
}

// clang-format off
int FetcherSession::getUserId() const { return user_id_; }
std::string FetcherSession::getUsername() const { return username_; }
// clang-format on

}  // namespace simc

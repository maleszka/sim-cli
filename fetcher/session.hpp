// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

/**
 * \file      fetcher/session.hpp
 * \brief     SIM API session management interface
 * \author    Adam Maleszka <adam_maleszka@aol.com>
 *
 * This file provides SIM API session management abilities being an interface to
 * all network operations.
 */

#ifndef FETCHER_SESSION_HPP_
#define FETCHER_SESSION_HPP_

#include <memory>
#include <string>

// Forward declarations
typedef void CURL;

namespace simc
{

/*******************************************************************************
 * \class     FetcherSession
 * \brief     Main fetcher class for SIM API session management
 *
 * It obtains SIM API session cookie and uses it to authorise all API calls.
 *
 * All data obtained from the API is returned in an unprocessed \c std::string
 * representing JSON object or JSON array. This class is meant to be used by
 * simc::Fetcher which also provides parsing and caching functionalities.
 *
 * \warning Although this is possible to instance multiple FetcherSession
 *          objects and maintain multiple sessions for different user accounts,
 *          it should be kept in mind that API calls in itself are not
 *          thread-safe.
 ******************************************************************************/
class FetcherSession
{
   protected:
    // User information
    int user_id_;           //!< session user id
    std::string username_;  //!< session user username

    // Connection info
    std::string sim_url_;     //!< base url of SIM instance
    std::string csrf_token_;  //!< csrf token for authentication
    std::unique_ptr<CURL, void (*)(CURL*)>
      curl_handle_;  //!< handle for libcurl communication

    // Logging
    FILE* logger_;  //!< file that libcurl should write verbose log to

   public:
    /**
     * \brief Log in to SIM API and initiate session that will persist through
     * the whole lifetime of the class
     */
    FetcherSession(std::string sim_url,   //!< [in] SIM instance url
                   std::string username,  //!< [in] login username
                   std::string password,  //!< [in] login password
                   FILE* logger
                   = nullptr  //!< [out] file that libcurl should write verbose
                              //!< log to, set to \c nullptr to disable log
    );
    ~FetcherSession();

    /**
     * \brief Perform SIM API call with \p command and return body in raw format
     */
    std::string performApiCall(std::string command);

    //! Return user id obtain during login
    int getUserId() const;

    //! Return username used during login
    std::string getUsername() const;

   protected:
    //! Enable libcurl verbose logging to #_logger
    void setupLogging();
};


}  // namespace simc

#endif  // FETCHER_SESSION_HPP_

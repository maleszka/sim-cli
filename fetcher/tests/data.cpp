#include <boost/json.hpp>
#include <cassert>
#include <chrono>
#include <fetcher/data.hpp>
#include <random>

class FetcherDataPublic : public simc::FetcherData
{
   public:
    FetcherDataPublic(const boost::json::array& desc,
                      const boost::json::array& data)
      : FetcherData(desc, data)
    {
    }

    int getItem() const { return item_; }

    void addChildrenPublic(std::shared_ptr<FetcherData> child)
    {
        addChild(child);
    }

    void sortChildrenPublic() { sortChildren(); }
};

int
data(int argc, char* argv[])
{
    // Create input JSON data
    boost::json::array desc = {"81h", "_id", "id", "name", "item", "`\\"};
    boost::json::array data = {"laksd", 348, 398, "test_data", 8, nullptr};

    // Create data object
    std::unique_ptr<FetcherDataPublic> obj(new FetcherDataPublic(desc, data));

    // Make sure that all values are appropriately set
    assert(obj->getId() == 398);
    assert(obj->usGetName() == u"test_data");
    assert(obj->getItem() == 8);
    assert(obj->getChildren().empty());

    // Create some objects
    std::mt19937 rng(
      std::chrono::high_resolution_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<int> dist(0, 34987);
    for (int i = 0; i < 100000; ++i) {
        int id = dist(rng);
        desc   = {"name", "item"};
        data   = {"some_" + std::to_string(id), id};
        std::shared_ptr<FetcherDataPublic> some_object(
          new FetcherDataPublic(desc, data));
        obj->addChildrenPublic(some_object);
    }
    obj->sortChildrenPublic();

    // Check children list
    auto& children = obj->getChildren();
    assert(std::is_sorted(
      children.begin(),
      children.end(),
      [](std::shared_ptr<simc::FetcherData> a,
         std::shared_ptr<simc::FetcherData> b)
      {
          return std::reinterpret_pointer_cast<FetcherDataPublic>(a)->getItem()
                 < std::reinterpret_pointer_cast<FetcherDataPublic>(b)
                     ->getItem();
      }));
    for (const auto& child : children) assert(child->getParent() == obj.get());
    return 0;
}

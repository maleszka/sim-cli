#include <boost/json.hpp>
#include <cassert>
#include <fetcher/data.hpp>

class FetcherDataRoundTaskPublic : public simc::FetcherDataRoundTask
{
   public:
    FetcherDataRoundTaskPublic(const boost::json::array& desc,
                               const boost::json::array& data)
      : FetcherDataRoundTask(desc, data)
    {
    }

    int getItem() const { return item_; }
};

int
data_round_task(int argc, char* argv[])
{
    // Create input JSON data
    boost::json::array desc = {"id",
                               "round_id",
                               "problem_id",
                               "can_view_problem",
                               "problem_label",
                               "name",
                               "item",
                               "method_of_choosing_final_submission",
                               "score_revealing",
                               "color_class"};
    boost::json::array data = {348,
                               398,
                               493,
                               false,
                               "mon",
                               "Drzewa przedział-przedział",
                               0,
                               "highest_score",
                               "none",
                               "intense-red"};

    // Create data object
    FetcherDataRoundTaskPublic* obj
      = new FetcherDataRoundTaskPublic(desc, data);

    // Make sure that all data has been loaded
    assert(obj->getId() == 348);
    assert(obj->usGetName() == u"Drzewa przedział-przedział");
    assert(obj->getRoundId() == 398);
    assert(obj->getProblemId() == 493);
    assert(obj->getProblemLabel() == "mon");
    assert(obj->getFinalSubmitRule() == simc::FinalSubmitRule::highestScore);
    assert(obj->getStatus() == simc::SubmissionStatus::runtimeError);

    // Purge data
    delete obj;
    return 0;
}

#include <cassert>
#include <fetcher/fetcher.hpp>

int
fetcher_get_contest(int argc, char* argv[])
{
    std::unique_ptr<simc::Fetcher> fetcher(
      new simc::Fetcher("https://sim.13lo.pl", "test_user", "1234"));

    // List contests and make sure that "Dojazd Nowaka" exists
    auto contests_list = fetcher->getContestsList();
    int contest_id     = 0;
    std::shared_ptr<simc::FetcherDataContest> contest;
    for (auto c : contests_list) {
        if (c->usGetName() == u"Dojazd Nowaka" && contest_id)
            return 1;  // contest appeared twice
        if (c->usGetName() == u"Dojazd Nowaka") {
            contest_id = c->getId();
            contest    = c;
        }
    }

    // Make sure that this contest is obtainable
    assert(contest_id != 0);
    assert(fetcher->getContest(contest_id, false) == contest);
    assert(fetcher->getContest(contest_id) == contest);
    assert(fetcher->getContest(contest_id)->usGetName() == u"Dojazd Nowaka");

    // Make sure that running getContest(name) gives the same results
    assert(fetcher->getContest(u"Dojazd Nowaka") == contest);
    return 0;
}

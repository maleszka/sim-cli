#include <cassert>
#include <fetcher/fetcher.hpp>
#include <iostream>

int
fetcher_lazy_load(int argc, char* argv[])
{
    using namespace simc;

    //////////////////// First session
    std::unique_ptr<Fetcher> fetcher(
      new Fetcher("https://sim.13lo.pl", "test_user", "1234"));

    std::shared_ptr<FetcherDataRoundTask> task_1
      = std::reinterpret_pointer_cast<FetcherDataRoundTask>(
        fetcher->getContest(u"Dojazd Nowaka")
          ->getChild(u"Łatwe")
          ->getChild(u"Trójkąt lub kwadrat"));


    //////////////////// Second session
    fetcher.reset(new Fetcher("https://sim.13lo.pl", "test_user", "1234"));

    std::shared_ptr<FetcherDataRoundTask> task_2
      = fetcher->getRoundTask(task_1->getId());

    // Make sure that results are the same
    assert(task_1->getId() == task_2->getId());
    assert(task_1->usGetName() == task_2->usGetName());
    assert(task_1->getRoundId() == task_2->getRoundId());
    assert(task_1->getProblemLabel() == task_2->getProblemLabel());
    return 0;
}

#include <cassert>
#include <fetcher/fetcher.hpp>

int
fetcher_login(int argc, char* argv[])
{
    simc::Fetcher* fetcher
      = new simc::Fetcher("https://sim.13lo.pl", "test_user", "1234");

    // Make sure that username and user_id matches
    assert(fetcher->getUsername() == "test_user");
    assert(fetcher->getUserId() == 1256);

    delete fetcher;
    return 0;
}

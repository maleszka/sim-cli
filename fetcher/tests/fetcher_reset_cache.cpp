#include <cassert>
#include <fetcher/fetcher.hpp>

int
fetcher_reset_cache(int argc, char* argv[])
{
    using namespace simc;
    std::unique_ptr<Fetcher> fetcher(
      new Fetcher("https://sim.13lo.pl", "test_user", "1234"));

    auto task_1 = std::reinterpret_pointer_cast<FetcherDataRoundTask>(
      fetcher->getContest(u"Dojazd Nowaka")
        ->getChild(u"Łatwe")
        ->getChild(u"Trójkąt lub kwadrat"));

    // Reset cache
    fetcher->forceUpdate();

    auto task_2 = fetcher->getRoundTask(task_1->getId());

    // Make sure that results are the same
    assert(task_1->getId() == task_2->getId());
    assert(task_1->usGetName() == task_2->usGetName());
    assert(task_1->getRoundId() == task_2->getRoundId());
    assert(task_1->getProblemLabel() == task_2->getProblemLabel());
    return 0;
}

#include <cassert>
#include <fetcher/fetcher.hpp>
#include <iostream>

int
fetcher_rounds_and_tasks(int argc, char* argv[])
{
    using namespace simc;
    std::unique_ptr<Fetcher> fetcher(
      new Fetcher("https://sim.13lo.pl", "test_user", "1234"));

    const auto& contest = fetcher->getContest(u"Dojazd Nowaka");
    assert(fetcher->getContest(contest->getId()) == contest);

    // Select round "Łatwe"
    auto round = std::reinterpret_pointer_cast<FetcherDataRound>(
      contest->getChild(u"Łatwe"));
    assert(round->getParent() == contest.get());
    assert(round->usGetName() == u"Łatwe");
    assert(fetcher->getRound(round->getId()) == round);

    // Select task "Trójkąt lub kwadrat"
    auto task = std::reinterpret_pointer_cast<FetcherDataRoundTask>(
      round->getChild(u"Trójkąt lub kwadrat"));
    assert(task->getParent() == round.get());
    assert(task->usGetName() == u"Trójkąt lub kwadrat");
    assert(task->getStatus() == SubmissionStatus::ok);
    assert(fetcher->getRoundTask(task->getId()) == task);

    return 0;
}

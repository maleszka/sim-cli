#include <cassert>
#include <fetcher/session.hpp>

int
session_login(int argc, char* argv[])
{
    simc::FetcherSession* session
      = new simc::FetcherSession("https://sim.13lo.pl", "test_user", "1234");

    // Make sure that username and user_id matches
    assert(session->getUsername() == "test_user");
    assert(session->getUserId() == 1256);

    delete session;
    return 0;
}

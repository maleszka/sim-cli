// ========================================================================== //

// Copyright (C) 2024 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef FETCHER_UTILS_LOGGER_HPP_
#define FETCHER_UTILS_LOGGER_HPP_

#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/severity_logger.hpp>

namespace simc
{
namespace log
{

/**
 * \enum  SeverityLevel
 * \brief Levels used by the logger to determine importance of given log record
 */
enum class SeverityLevel
{
    debug    = 0,
    info     = 1,
    warning  = 2,
    error    = 3,
    critical = 4,
};

/**
 * \typedef logger_t
 * \brief   Preferred logger type to be used in the project
 */
typedef boost::log::sources::severity_logger<SeverityLevel> logger_t;

/**
 * \brief Force all pending log records to be flushed
 *
 * This module keeps \c auto_flush boost log keyword off in order to improve
 * performance. It's necessary then to perform log flush before throwing an
 * error.
 */
void flush();

}  // namespace log
}  // namespace simc

/**
 * \def   LOG(lg, severity)
 * \brief Convenient macro for logging into \c logger_t
 */
#define LOG(lg, severity) BOOST_LOG_SEV(lg, simc::log::SeverityLevel::severity)

#endif  // FETCHER_UTILS_LOGGER_HPP_

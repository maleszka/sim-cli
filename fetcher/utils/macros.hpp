// ========================================================================== //

// Copyright (C) 2023 Adam Maleszka

// This file is part of sim-cli. sim-cli is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.

// sim-cli is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// sim-cli. If not, see <https://www.gnu.org/licenses/>.

// ========================================================================== //

#ifndef FETCHER_UTILS_MACROS_HPP_
#define FETCHER_UTILS_MACROS_HPP_

#ifdef NDEBUG

// Touch the expression without evaluating it, only to silence `unused' warnings
#define SIMC_ASSERT(x)                                                         \
    do {                                                                       \
        (void)sizeof(x);                                                       \
    } while (0)

#else

#include <cassert>
#define SIMC_ASSERT(x) assert(x)

#endif

// Safe memory deallocation
#define SIMC_DELETE(x)                                                         \
    do {                                                                       \
        if ((x) != nullptr) {                                                  \
            delete (x);                                                        \
            (x) = nullptr;                                                     \
        }                                                                      \
    } while (0)

#endif

;; GNU Guix package manager manifest

(specifications->manifest
 (list
  ;; for shell usage
  "coreutils"

  ;; build system
  "meson"
  "ninja"
  "cmake"
  "gcc-toolchain@10"
  "clang@15.0.7"  ;; for clang-format
  "pkg-config"

  ;; dependencies
  "boost"
  "curl"
  "doxygen"
  "graphviz"
  ))
